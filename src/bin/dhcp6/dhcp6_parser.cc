// A Bison parser, made by GNU Bison 3.0.4.

// Skeleton implementation for Bison LALR(1) parsers in C++

// Copyright (C) 2002-2015 Free Software Foundation, Inc.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// As a special exception, you may create a larger work that contains
// part or all of the Bison parser skeleton and distribute that work
// under terms of your choice, so long as that work isn't itself a
// parser generator using the skeleton or a modified version thereof
// as a parser skeleton.  Alternatively, if you modify or redistribute
// the parser skeleton itself, you may (at your option) remove this
// special exception, which will cause the skeleton and the resulting
// Bison output files to be licensed under the GNU General Public
// License without this special exception.

// This special exception was added by the Free Software Foundation in
// version 2.2 of Bison.

// Take the name prefix into account.
#define yylex   parser6_lex

// First part of user declarations.

#line 39 "dhcp6_parser.cc" // lalr1.cc:404

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

#include "dhcp6_parser.h"

// User implementation prologue.

#line 53 "dhcp6_parser.cc" // lalr1.cc:412
// Unqualified %code blocks.
#line 34 "dhcp6_parser.yy" // lalr1.cc:413

#include <dhcp6/parser_context.h>

#line 59 "dhcp6_parser.cc" // lalr1.cc:413


#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> // FIXME: INFRINGES ON USER NAME SPACE.
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K].location)
/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

# ifndef YYLLOC_DEFAULT
#  define YYLLOC_DEFAULT(Current, Rhs, N)                               \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).begin  = YYRHSLOC (Rhs, 1).begin;                   \
          (Current).end    = YYRHSLOC (Rhs, N).end;                     \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).begin = (Current).end = YYRHSLOC (Rhs, 0).end;      \
        }                                                               \
    while (/*CONSTCOND*/ false)
# endif


// Suppress unused-variable warnings by "using" E.
#define YYUSE(E) ((void) (E))

// Enable debugging if requested.
#if PARSER6_DEBUG

// A pseudo ostream that takes yydebug_ into account.
# define YYCDEBUG if (yydebug_) (*yycdebug_)

# define YY_SYMBOL_PRINT(Title, Symbol)         \
  do {                                          \
    if (yydebug_)                               \
    {                                           \
      *yycdebug_ << Title << ' ';               \
      yy_print_ (*yycdebug_, Symbol);           \
      *yycdebug_ << std::endl;                  \
    }                                           \
  } while (false)

# define YY_REDUCE_PRINT(Rule)          \
  do {                                  \
    if (yydebug_)                       \
      yy_reduce_print_ (Rule);          \
  } while (false)

# define YY_STACK_PRINT()               \
  do {                                  \
    if (yydebug_)                       \
      yystack_print_ ();                \
  } while (false)

#else // !PARSER6_DEBUG

# define YYCDEBUG if (false) std::cerr
# define YY_SYMBOL_PRINT(Title, Symbol)  YYUSE(Symbol)
# define YY_REDUCE_PRINT(Rule)           static_cast<void>(0)
# define YY_STACK_PRINT()                static_cast<void>(0)

#endif // !PARSER6_DEBUG

#define yyerrok         (yyerrstatus_ = 0)
#define yyclearin       (yyla.clear ())

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYRECOVERING()  (!!yyerrstatus_)

#line 14 "dhcp6_parser.yy" // lalr1.cc:479
namespace isc { namespace dhcp {
#line 145 "dhcp6_parser.cc" // lalr1.cc:479

  /* Return YYSTR after stripping away unnecessary quotes and
     backslashes, so that it's suitable for yyerror.  The heuristic is
     that double-quoting is unnecessary unless the string contains an
     apostrophe, a comma, or backslash (other than backslash-backslash).
     YYSTR is taken from yytname.  */
  std::string
  Dhcp6Parser::yytnamerr_ (const char *yystr)
  {
    if (*yystr == '"')
      {
        std::string yyr = "";
        char const *yyp = yystr;

        for (;;)
          switch (*++yyp)
            {
            case '\'':
            case ',':
              goto do_not_strip_quotes;

            case '\\':
              if (*++yyp != '\\')
                goto do_not_strip_quotes;
              // Fall through.
            default:
              yyr += *yyp;
              break;

            case '"':
              return yyr;
            }
      do_not_strip_quotes: ;
      }

    return yystr;
  }


  /// Build a parser object.
  Dhcp6Parser::Dhcp6Parser (isc::dhcp::Parser6Context& ctx_yyarg)
    :
#if PARSER6_DEBUG
      yydebug_ (false),
      yycdebug_ (&std::cerr),
#endif
      ctx (ctx_yyarg)
  {}

  Dhcp6Parser::~Dhcp6Parser ()
  {}


  /*---------------.
  | Symbol types.  |
  `---------------*/



  // by_state.
  inline
  Dhcp6Parser::by_state::by_state ()
    : state (empty_state)
  {}

  inline
  Dhcp6Parser::by_state::by_state (const by_state& other)
    : state (other.state)
  {}

  inline
  void
  Dhcp6Parser::by_state::clear ()
  {
    state = empty_state;
  }

  inline
  void
  Dhcp6Parser::by_state::move (by_state& that)
  {
    state = that.state;
    that.clear ();
  }

  inline
  Dhcp6Parser::by_state::by_state (state_type s)
    : state (s)
  {}

  inline
  Dhcp6Parser::symbol_number_type
  Dhcp6Parser::by_state::type_get () const
  {
    if (state == empty_state)
      return empty_symbol;
    else
      return yystos_[state];
  }

  inline
  Dhcp6Parser::stack_symbol_type::stack_symbol_type ()
  {}


  inline
  Dhcp6Parser::stack_symbol_type::stack_symbol_type (state_type s, symbol_type& that)
    : super_type (s, that.location)
  {
      switch (that.type_get ())
    {
      case 171: // value
      case 175: // map_value
      case 219: // db_type
      case 295: // hr_mode
      case 426: // duid_type
      case 459: // ncr_protocol_value
      case 467: // replace_client_name_value
        value.move< ElementPtr > (that.value);
        break;

      case 155: // "boolean"
        value.move< bool > (that.value);
        break;

      case 154: // "floating point"
        value.move< double > (that.value);
        break;

      case 153: // "integer"
        value.move< int64_t > (that.value);
        break;

      case 152: // "constant string"
        value.move< std::string > (that.value);
        break;

      default:
        break;
    }

    // that is emptied.
    that.type = empty_symbol;
  }

  inline
  Dhcp6Parser::stack_symbol_type&
  Dhcp6Parser::stack_symbol_type::operator= (const stack_symbol_type& that)
  {
    state = that.state;
      switch (that.type_get ())
    {
      case 171: // value
      case 175: // map_value
      case 219: // db_type
      case 295: // hr_mode
      case 426: // duid_type
      case 459: // ncr_protocol_value
      case 467: // replace_client_name_value
        value.copy< ElementPtr > (that.value);
        break;

      case 155: // "boolean"
        value.copy< bool > (that.value);
        break;

      case 154: // "floating point"
        value.copy< double > (that.value);
        break;

      case 153: // "integer"
        value.copy< int64_t > (that.value);
        break;

      case 152: // "constant string"
        value.copy< std::string > (that.value);
        break;

      default:
        break;
    }

    location = that.location;
    return *this;
  }


  template <typename Base>
  inline
  void
  Dhcp6Parser::yy_destroy_ (const char* yymsg, basic_symbol<Base>& yysym) const
  {
    if (yymsg)
      YY_SYMBOL_PRINT (yymsg, yysym);
  }

#if PARSER6_DEBUG
  template <typename Base>
  void
  Dhcp6Parser::yy_print_ (std::ostream& yyo,
                                     const basic_symbol<Base>& yysym) const
  {
    std::ostream& yyoutput = yyo;
    YYUSE (yyoutput);
    symbol_number_type yytype = yysym.type_get ();
    // Avoid a (spurious) G++ 4.8 warning about "array subscript is
    // below array bounds".
    if (yysym.empty ())
      std::abort ();
    yyo << (yytype < yyntokens_ ? "token" : "nterm")
        << ' ' << yytname_[yytype] << " ("
        << yysym.location << ": ";
    switch (yytype)
    {
            case 152: // "constant string"

#line 228 "dhcp6_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< std::string > (); }
#line 364 "dhcp6_parser.cc" // lalr1.cc:636
        break;

      case 153: // "integer"

#line 228 "dhcp6_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< int64_t > (); }
#line 371 "dhcp6_parser.cc" // lalr1.cc:636
        break;

      case 154: // "floating point"

#line 228 "dhcp6_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< double > (); }
#line 378 "dhcp6_parser.cc" // lalr1.cc:636
        break;

      case 155: // "boolean"

#line 228 "dhcp6_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< bool > (); }
#line 385 "dhcp6_parser.cc" // lalr1.cc:636
        break;

      case 171: // value

#line 228 "dhcp6_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< ElementPtr > (); }
#line 392 "dhcp6_parser.cc" // lalr1.cc:636
        break;

      case 175: // map_value

#line 228 "dhcp6_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< ElementPtr > (); }
#line 399 "dhcp6_parser.cc" // lalr1.cc:636
        break;

      case 219: // db_type

#line 228 "dhcp6_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< ElementPtr > (); }
#line 406 "dhcp6_parser.cc" // lalr1.cc:636
        break;

      case 295: // hr_mode

#line 228 "dhcp6_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< ElementPtr > (); }
#line 413 "dhcp6_parser.cc" // lalr1.cc:636
        break;

      case 426: // duid_type

#line 228 "dhcp6_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< ElementPtr > (); }
#line 420 "dhcp6_parser.cc" // lalr1.cc:636
        break;

      case 459: // ncr_protocol_value

#line 228 "dhcp6_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< ElementPtr > (); }
#line 427 "dhcp6_parser.cc" // lalr1.cc:636
        break;

      case 467: // replace_client_name_value

#line 228 "dhcp6_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< ElementPtr > (); }
#line 434 "dhcp6_parser.cc" // lalr1.cc:636
        break;


      default:
        break;
    }
    yyo << ')';
  }
#endif

  inline
  void
  Dhcp6Parser::yypush_ (const char* m, state_type s, symbol_type& sym)
  {
    stack_symbol_type t (s, sym);
    yypush_ (m, t);
  }

  inline
  void
  Dhcp6Parser::yypush_ (const char* m, stack_symbol_type& s)
  {
    if (m)
      YY_SYMBOL_PRINT (m, s);
    yystack_.push (s);
  }

  inline
  void
  Dhcp6Parser::yypop_ (unsigned int n)
  {
    yystack_.pop (n);
  }

#if PARSER6_DEBUG
  std::ostream&
  Dhcp6Parser::debug_stream () const
  {
    return *yycdebug_;
  }

  void
  Dhcp6Parser::set_debug_stream (std::ostream& o)
  {
    yycdebug_ = &o;
  }


  Dhcp6Parser::debug_level_type
  Dhcp6Parser::debug_level () const
  {
    return yydebug_;
  }

  void
  Dhcp6Parser::set_debug_level (debug_level_type l)
  {
    yydebug_ = l;
  }
#endif // PARSER6_DEBUG

  inline Dhcp6Parser::state_type
  Dhcp6Parser::yy_lr_goto_state_ (state_type yystate, int yysym)
  {
    int yyr = yypgoto_[yysym - yyntokens_] + yystate;
    if (0 <= yyr && yyr <= yylast_ && yycheck_[yyr] == yystate)
      return yytable_[yyr];
    else
      return yydefgoto_[yysym - yyntokens_];
  }

  inline bool
  Dhcp6Parser::yy_pact_value_is_default_ (int yyvalue)
  {
    return yyvalue == yypact_ninf_;
  }

  inline bool
  Dhcp6Parser::yy_table_value_is_error_ (int yyvalue)
  {
    return yyvalue == yytable_ninf_;
  }

  int
  Dhcp6Parser::parse ()
  {
    // State.
    int yyn;
    /// Length of the RHS of the rule being reduced.
    int yylen = 0;

    // Error handling.
    int yynerrs_ = 0;
    int yyerrstatus_ = 0;

    /// The lookahead symbol.
    symbol_type yyla;

    /// The locations where the error started and ended.
    stack_symbol_type yyerror_range[3];

    /// The return value of parse ().
    int yyresult;

    // FIXME: This shoud be completely indented.  It is not yet to
    // avoid gratuitous conflicts when merging into the master branch.
    try
      {
    YYCDEBUG << "Starting parse" << std::endl;


    /* Initialize the stack.  The initial state will be set in
       yynewstate, since the latter expects the semantical and the
       location values to have been already stored, initialize these
       stacks with a primary value.  */
    yystack_.clear ();
    yypush_ (YY_NULLPTR, 0, yyla);

    // A new symbol was pushed on the stack.
  yynewstate:
    YYCDEBUG << "Entering state " << yystack_[0].state << std::endl;

    // Accept?
    if (yystack_[0].state == yyfinal_)
      goto yyacceptlab;

    goto yybackup;

    // Backup.
  yybackup:

    // Try to take a decision without lookahead.
    yyn = yypact_[yystack_[0].state];
    if (yy_pact_value_is_default_ (yyn))
      goto yydefault;

    // Read a lookahead token.
    if (yyla.empty ())
      {
        YYCDEBUG << "Reading a token: ";
        try
          {
            symbol_type yylookahead (yylex (ctx));
            yyla.move (yylookahead);
          }
        catch (const syntax_error& yyexc)
          {
            error (yyexc);
            goto yyerrlab1;
          }
      }
    YY_SYMBOL_PRINT ("Next token is", yyla);

    /* If the proper action on seeing token YYLA.TYPE is to reduce or
       to detect an error, take that action.  */
    yyn += yyla.type_get ();
    if (yyn < 0 || yylast_ < yyn || yycheck_[yyn] != yyla.type_get ())
      goto yydefault;

    // Reduce or error.
    yyn = yytable_[yyn];
    if (yyn <= 0)
      {
        if (yy_table_value_is_error_ (yyn))
          goto yyerrlab;
        yyn = -yyn;
        goto yyreduce;
      }

    // Count tokens shifted since error; after three, turn off error status.
    if (yyerrstatus_)
      --yyerrstatus_;

    // Shift the lookahead token.
    yypush_ ("Shifting", yyn, yyla);
    goto yynewstate;

  /*-----------------------------------------------------------.
  | yydefault -- do the default action for the current state.  |
  `-----------------------------------------------------------*/
  yydefault:
    yyn = yydefact_[yystack_[0].state];
    if (yyn == 0)
      goto yyerrlab;
    goto yyreduce;

  /*-----------------------------.
  | yyreduce -- Do a reduction.  |
  `-----------------------------*/
  yyreduce:
    yylen = yyr2_[yyn];
    {
      stack_symbol_type yylhs;
      yylhs.state = yy_lr_goto_state_(yystack_[yylen].state, yyr1_[yyn]);
      /* Variants are always initialized to an empty instance of the
         correct type. The default '$$ = $1' action is NOT applied
         when using variants.  */
        switch (yyr1_[yyn])
    {
      case 171: // value
      case 175: // map_value
      case 219: // db_type
      case 295: // hr_mode
      case 426: // duid_type
      case 459: // ncr_protocol_value
      case 467: // replace_client_name_value
        yylhs.value.build< ElementPtr > ();
        break;

      case 155: // "boolean"
        yylhs.value.build< bool > ();
        break;

      case 154: // "floating point"
        yylhs.value.build< double > ();
        break;

      case 153: // "integer"
        yylhs.value.build< int64_t > ();
        break;

      case 152: // "constant string"
        yylhs.value.build< std::string > ();
        break;

      default:
        break;
    }


      // Compute the default @$.
      {
        slice<stack_symbol_type, stack_type> slice (yystack_, yylen);
        YYLLOC_DEFAULT (yylhs.location, slice, yylen);
      }

      // Perform the reduction.
      YY_REDUCE_PRINT (yyn);
      try
        {
          switch (yyn)
            {
  case 2:
#line 237 "dhcp6_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.NO_KEYWORD; }
#line 680 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 4:
#line 238 "dhcp6_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.CONFIG; }
#line 686 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 6:
#line 239 "dhcp6_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.DHCP6; }
#line 692 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 8:
#line 240 "dhcp6_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.INTERFACES_CONFIG; }
#line 698 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 10:
#line 241 "dhcp6_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.SUBNET6; }
#line 704 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 12:
#line 242 "dhcp6_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.POOLS; }
#line 710 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 14:
#line 243 "dhcp6_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.PD_POOLS; }
#line 716 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 16:
#line 244 "dhcp6_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.RESERVATIONS; }
#line 722 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 18:
#line 245 "dhcp6_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.DHCP6; }
#line 728 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 20:
#line 246 "dhcp6_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.OPTION_DEF; }
#line 734 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 22:
#line 247 "dhcp6_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.OPTION_DATA; }
#line 740 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 24:
#line 248 "dhcp6_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.HOOKS_LIBRARIES; }
#line 746 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 26:
#line 249 "dhcp6_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.DHCP_DDNS; }
#line 752 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 28:
#line 257 "dhcp6_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location))); }
#line 758 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 29:
#line 258 "dhcp6_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new DoubleElement(yystack_[0].value.as< double > (), ctx.loc2pos(yystack_[0].location))); }
#line 764 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 30:
#line 259 "dhcp6_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location))); }
#line 770 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 31:
#line 260 "dhcp6_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location))); }
#line 776 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 32:
#line 261 "dhcp6_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new NullElement(ctx.loc2pos(yystack_[0].location))); }
#line 782 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 33:
#line 262 "dhcp6_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ctx.stack_.back(); ctx.stack_.pop_back(); }
#line 788 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 34:
#line 263 "dhcp6_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ctx.stack_.back(); ctx.stack_.pop_back(); }
#line 794 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 35:
#line 266 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // Push back the JSON value on the stack
    ctx.stack_.push_back(yystack_[0].value.as< ElementPtr > ());
}
#line 803 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 36:
#line 271 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // This code is executed when we're about to start parsing
    // the content of the map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 814 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 37:
#line 276 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // map parsing completed. If we ever want to do any wrap up
    // (maybe some sanity checking), this would be the best place
    // for it.
}
#line 824 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 38:
#line 282 "dhcp6_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ctx.stack_.back(); ctx.stack_.pop_back(); }
#line 830 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 41:
#line 289 "dhcp6_parser.yy" // lalr1.cc:859
    {
                  // map containing a single entry
                  ctx.stack_.back()->set(yystack_[2].value.as< std::string > (), yystack_[0].value.as< ElementPtr > ());
                  }
#line 839 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 42:
#line 293 "dhcp6_parser.yy" // lalr1.cc:859
    {
                  // map consisting of a shorter map followed by
                  // comma and string:value
                  ctx.stack_.back()->set(yystack_[2].value.as< std::string > (), yystack_[0].value.as< ElementPtr > ());
                  }
#line 849 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 43:
#line 300 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(l);
}
#line 858 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 44:
#line 303 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // list parsing complete. Put any sanity checking here
}
#line 866 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 47:
#line 311 "dhcp6_parser.yy" // lalr1.cc:859
    {
                  // List consisting of a single element.
                  ctx.stack_.back()->add(yystack_[0].value.as< ElementPtr > ());
                  }
#line 875 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 48:
#line 315 "dhcp6_parser.yy" // lalr1.cc:859
    {
                  // List ending with , and a value.
                  ctx.stack_.back()->add(yystack_[0].value.as< ElementPtr > ());
                  }
#line 884 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 49:
#line 322 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // List parsing about to start
}
#line 892 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 50:
#line 324 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // list parsing complete. Put any sanity checking here
    //ctx.stack_.pop_back();
}
#line 901 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 53:
#line 333 "dhcp6_parser.yy" // lalr1.cc:859
    {
                          ElementPtr s(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
                          ctx.stack_.back()->add(s);
                          }
#line 910 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 54:
#line 337 "dhcp6_parser.yy" // lalr1.cc:859
    {
                          ElementPtr s(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
                          ctx.stack_.back()->add(s);
                          }
#line 919 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 55:
#line 348 "dhcp6_parser.yy" // lalr1.cc:859
    {
    const std::string& where = ctx.contextName();
    const std::string& keyword = yystack_[1].value.as< std::string > ();
    error(yystack_[1].location,
          "got unexpected keyword \"" + keyword + "\" in " + where + " map.");
}
#line 930 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 56:
#line 358 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // This code is executed when we're about to start parsing
    // the content of the map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 941 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 57:
#line 363 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // map parsing completed. If we ever want to do any wrap up
    // (maybe some sanity checking), this would be the best place
    // for it.

    // Dhcp6 is required
    ctx.require("Dhcp6", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
}
#line 954 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 66:
#line 386 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // This code is executed when we're about to start parsing
    // the content of the map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("Dhcp6", m);
    ctx.stack_.push_back(m);
    ctx.enter(ctx.DHCP6);
}
#line 967 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 67:
#line 393 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // No global parameter is required
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 977 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 68:
#line 401 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // Parse the Dhcp6 map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 987 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 69:
#line 405 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // No global parameter is required
    // parsing completed
}
#line 996 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 95:
#line 441 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr prf(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("preferred-lifetime", prf);
}
#line 1005 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 96:
#line 446 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr prf(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("valid-lifetime", prf);
}
#line 1014 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 97:
#line 451 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr prf(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("renew-timer", prf);
}
#line 1023 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 98:
#line 456 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr prf(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("rebind-timer", prf);
}
#line 1032 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 99:
#line 461 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr dpp(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("decline-probation-period", dpp);
}
#line 1041 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 100:
#line 466 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr i(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("interfaces-config", i);
    ctx.stack_.push_back(i);
    ctx.enter(ctx.INTERFACES_CONFIG);
}
#line 1052 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 101:
#line 471 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // No interfaces config param is required
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 1062 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 102:
#line 477 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // Parse the interfaces-config map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 1072 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 103:
#line 481 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // No interfaces config param is required
    // parsing completed
}
#line 1081 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 108:
#line 494 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("interfaces", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1092 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 109:
#line 499 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 1101 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 110:
#line 504 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr b(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("re-detect", b);
}
#line 1110 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 111:
#line 510 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr i(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("lease-database", i);
    ctx.stack_.push_back(i);
    ctx.enter(ctx.LEASE_DATABASE);
}
#line 1121 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 112:
#line 515 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // The type parameter is required
    ctx.require("type", ctx.loc2pos(yystack_[2].location), ctx.loc2pos(yystack_[0].location));
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 1132 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 113:
#line 522 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr i(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("hosts-database", i);
    ctx.stack_.push_back(i);
    ctx.enter(ctx.HOSTS_DATABASE);
}
#line 1143 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 114:
#line 527 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // The type parameter is required
    ctx.require("type", ctx.loc2pos(yystack_[2].location), ctx.loc2pos(yystack_[0].location));
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 1154 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 130:
#line 553 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.DATABASE_TYPE);
}
#line 1162 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 131:
#line 555 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.back()->set("type", yystack_[0].value.as< ElementPtr > ());
    ctx.leave();
}
#line 1171 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 132:
#line 560 "dhcp6_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("memfile", ctx.loc2pos(yystack_[0].location))); }
#line 1177 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 133:
#line 561 "dhcp6_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("mysql", ctx.loc2pos(yystack_[0].location))); }
#line 1183 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 134:
#line 562 "dhcp6_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("postgresql", ctx.loc2pos(yystack_[0].location))); }
#line 1189 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 135:
#line 563 "dhcp6_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("cql", ctx.loc2pos(yystack_[0].location))); }
#line 1195 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 136:
#line 564 "dhcp6_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("radius", ctx.loc2pos(yystack_[0].location))); }
#line 1201 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 137:
#line 567 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1209 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 138:
#line 569 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr user(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("user", user);
    ctx.leave();
}
#line 1219 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 139:
#line 575 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1227 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 140:
#line 577 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr pwd(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("password", pwd);
    ctx.leave();
}
#line 1237 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 141:
#line 583 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1245 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 142:
#line 585 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr h(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("host", h);
    ctx.leave();
}
#line 1255 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 143:
#line 591 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr p(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("port", p);
}
#line 1264 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 144:
#line 596 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1272 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 145:
#line 598 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr name(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("name", name);
    ctx.leave();
}
#line 1282 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 146:
#line 604 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr n(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("persist", n);
}
#line 1291 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 147:
#line 609 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr n(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("lfc-interval", n);
}
#line 1300 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 148:
#line 614 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr n(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("readonly", n);
}
#line 1309 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 149:
#line 619 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr n(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("connect-timeout", n);
}
#line 1318 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 150:
#line 624 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1326 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 151:
#line 626 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr cp(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("contact-points", cp);
    ctx.leave();
}
#line 1336 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 152:
#line 632 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1344 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 153:
#line 634 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr ks(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("keyspace", ks);
    ctx.leave();
}
#line 1354 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 154:
#line 641 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("mac-sources", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.MAC_SOURCES);
}
#line 1365 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 155:
#line 646 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 1374 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 160:
#line 659 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr duid(new StringElement("duid", ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(duid);
}
#line 1383 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 161:
#line 664 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr duid(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(duid);
}
#line 1392 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 162:
#line 669 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("host-reservation-identifiers", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.HOST_RESERVATION_IDENTIFIERS);
}
#line 1403 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 163:
#line 674 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 1412 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 169:
#line 688 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr hwaddr(new StringElement("hw-address", ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(hwaddr);
}
#line 1421 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 170:
#line 693 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr flex_id(new StringElement("flex-id", ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(flex_id);
}
#line 1430 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 171:
#line 700 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("relay-supplied-options", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1441 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 172:
#line 705 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 1450 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 173:
#line 710 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("hooks-libraries", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.HOOKS_LIBRARIES);
}
#line 1461 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 174:
#line 715 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 1470 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 179:
#line 728 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(m);
    ctx.stack_.push_back(m);
}
#line 1480 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 180:
#line 732 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // The library hooks parameter is required
    ctx.require("library", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.stack_.pop_back();
}
#line 1490 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 181:
#line 738 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // Parse the hooks-libraries list entry map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 1500 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 182:
#line 742 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // The library hooks parameter is required
    ctx.require("library", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    // parsing completed
}
#line 1510 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 188:
#line 757 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1518 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 189:
#line 759 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr lib(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("library", lib);
    ctx.leave();
}
#line 1528 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 190:
#line 765 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1536 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 191:
#line 767 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.back()->set("parameters", yystack_[0].value.as< ElementPtr > ());
    ctx.leave();
}
#line 1545 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 192:
#line 773 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("expired-leases-processing", m);
    ctx.stack_.push_back(m);
    ctx.enter(ctx.EXPIRED_LEASES_PROCESSING);
}
#line 1556 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 193:
#line 778 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // No expired lease parameter is required
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 1566 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 202:
#line 796 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr value(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("reclaim-timer-wait-time", value);
}
#line 1575 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 203:
#line 801 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr value(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("flush-reclaimed-timer-wait-time", value);
}
#line 1584 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 204:
#line 806 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr value(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("hold-reclaimed-time", value);
}
#line 1593 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 205:
#line 811 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr value(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("max-reclaim-leases", value);
}
#line 1602 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 206:
#line 816 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr value(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("max-reclaim-time", value);
}
#line 1611 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 207:
#line 821 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr value(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("unwarned-reclaim-cycles", value);
}
#line 1620 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 208:
#line 829 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("subnet6", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.SUBNET6);
}
#line 1631 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 209:
#line 834 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 1640 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 214:
#line 854 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(m);
    ctx.stack_.push_back(m);
}
#line 1650 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 215:
#line 858 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // Once we reached this place, the subnet parsing is now complete.
    // If we want to, we can implement default values here.
    // In particular we can do things like this:
    // if (!ctx.stack_.back()->get("interface")) {
    //     ctx.stack_.back()->set("interface", StringElement("loopback"));
    // }
    //
    // We can also stack up one level (Dhcp6) and copy over whatever
    // global parameters we want to:
    // if (!ctx.stack_.back()->get("renew-timer")) {
    //     ElementPtr renew = ctx_stack_[...].get("renew-timer");
    //     if (renew) {
    //         ctx.stack_.back()->set("renew-timer", renew);
    //     }
    // }

    // The subnet subnet6 parameter is required
    ctx.require("subnet", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.stack_.pop_back();
}
#line 1676 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 216:
#line 880 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // Parse the subnet6 list entry map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 1686 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 217:
#line 884 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // The subnet subnet6 parameter is required
    ctx.require("subnet", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    // parsing completed
}
#line 1696 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 238:
#line 916 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1704 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 239:
#line 918 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr subnet(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("subnet", subnet);
    ctx.leave();
}
#line 1714 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 240:
#line 924 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1722 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 241:
#line 926 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr iface(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("interface", iface);
    ctx.leave();
}
#line 1732 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 242:
#line 932 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1740 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 243:
#line 934 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr iface(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("interface-id", iface);
    ctx.leave();
}
#line 1750 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 244:
#line 940 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.CLIENT_CLASS);
}
#line 1758 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 245:
#line 942 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr cls(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("client-class", cls);
    ctx.leave();
}
#line 1768 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 246:
#line 948 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.RESERVATION_MODE);
}
#line 1776 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 247:
#line 950 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.back()->set("reservation-mode", yystack_[0].value.as< ElementPtr > ());
    ctx.leave();
}
#line 1785 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 248:
#line 955 "dhcp6_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("disabled", ctx.loc2pos(yystack_[0].location))); }
#line 1791 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 249:
#line 956 "dhcp6_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("out-of-pool", ctx.loc2pos(yystack_[0].location))); }
#line 1797 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 250:
#line 957 "dhcp6_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("all", ctx.loc2pos(yystack_[0].location))); }
#line 1803 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 251:
#line 960 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr id(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("id", id);
}
#line 1812 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 252:
#line 965 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr rc(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("rapid-commit", rc);
}
#line 1821 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 253:
#line 973 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("shared-networks", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.SHARED_NETWORK);
}
#line 1832 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 254:
#line 978 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 1841 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 259:
#line 993 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(m);
    ctx.stack_.push_back(m);
}
#line 1851 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 260:
#line 997 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
}
#line 1859 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 277:
#line 1025 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("option-def", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.OPTION_DEF);
}
#line 1870 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 278:
#line 1030 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 1879 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 279:
#line 1038 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 1888 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 280:
#line 1041 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // parsing completed
}
#line 1896 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 285:
#line 1057 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(m);
    ctx.stack_.push_back(m);
}
#line 1906 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 286:
#line 1061 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // The name, code and type option def parameters are required.
    ctx.require("name", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.require("code", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.require("type", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.stack_.pop_back();
}
#line 1918 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 287:
#line 1072 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // Parse the option-def list entry map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 1928 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 288:
#line 1076 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // The name, code and type option def parameters are required.
    ctx.require("name", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.require("code", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.require("type", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    // parsing completed
}
#line 1940 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 302:
#line 1106 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr code(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("code", code);
}
#line 1949 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 304:
#line 1113 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1957 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 305:
#line 1115 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr prf(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("type", prf);
    ctx.leave();
}
#line 1967 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 306:
#line 1121 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1975 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 307:
#line 1123 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr rtypes(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("record-types", rtypes);
    ctx.leave();
}
#line 1985 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 308:
#line 1129 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1993 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 309:
#line 1131 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr space(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("space", space);
    ctx.leave();
}
#line 2003 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 311:
#line 1139 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2011 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 312:
#line 1141 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr encap(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("encapsulate", encap);
    ctx.leave();
}
#line 2021 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 313:
#line 1147 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr array(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("array", array);
}
#line 2030 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 314:
#line 1156 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("option-data", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.OPTION_DATA);
}
#line 2041 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 315:
#line 1161 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 2050 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 320:
#line 1180 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(m);
    ctx.stack_.push_back(m);
}
#line 2060 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 321:
#line 1184 "dhcp6_parser.yy" // lalr1.cc:859
    {
    /// @todo: the code or name parameters are required.
    ctx.stack_.pop_back();
}
#line 2069 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 322:
#line 1192 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // Parse the option-data list entry map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 2079 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 323:
#line 1196 "dhcp6_parser.yy" // lalr1.cc:859
    {
    /// @todo: the code or name parameters are required.
    // parsing completed
}
#line 2088 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 336:
#line 1227 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2096 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 337:
#line 1229 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr data(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("data", data);
    ctx.leave();
}
#line 2106 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 340:
#line 1239 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr space(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("csv-format", space);
}
#line 2115 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 341:
#line 1244 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr persist(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("always-send", persist);
}
#line 2124 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 342:
#line 1252 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("pools", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.POOLS);
}
#line 2135 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 343:
#line 1257 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 2144 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 348:
#line 1272 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(m);
    ctx.stack_.push_back(m);
}
#line 2154 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 349:
#line 1276 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // The pool parameter is required.
    ctx.require("pool", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.stack_.pop_back();
}
#line 2164 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 350:
#line 1282 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // Parse the pool list entry map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 2174 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 351:
#line 1286 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // The pool parameter is required.
    ctx.require("pool", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
}
#line 2183 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 358:
#line 1301 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2191 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 359:
#line 1303 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr pool(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("pool", pool);
    ctx.leave();
}
#line 2201 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 360:
#line 1309 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2209 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 361:
#line 1311 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.back()->set("user-context", yystack_[0].value.as< ElementPtr > ());
    ctx.leave();
}
#line 2218 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 362:
#line 1319 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("pd-pools", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.PD_POOLS);
}
#line 2229 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 363:
#line 1324 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 2238 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 368:
#line 1339 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(m);
    ctx.stack_.push_back(m);
}
#line 2248 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 369:
#line 1343 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // The prefix, prefix len and delegated len parameters are required.
    ctx.require("prefix", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.require("prefix-len", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.require("delegated-len", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.stack_.pop_back();
}
#line 2260 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 370:
#line 1351 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // Parse the pd-pool list entry map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 2270 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 371:
#line 1355 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // The prefix, prefix len and delegated len parameters are required.
    ctx.require("prefix", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.require("prefix-len", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.require("delegated-len", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    // parsing completed
}
#line 2282 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 382:
#line 1377 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2290 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 383:
#line 1379 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr prf(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("prefix", prf);
    ctx.leave();
}
#line 2300 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 384:
#line 1385 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr prf(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("prefix-len", prf);
}
#line 2309 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 385:
#line 1390 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2317 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 386:
#line 1392 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr prf(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("excluded-prefix", prf);
    ctx.leave();
}
#line 2327 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 387:
#line 1398 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr prf(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("excluded-prefix-len", prf);
}
#line 2336 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 388:
#line 1403 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr deleg(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("delegated-len", deleg);
}
#line 2345 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 389:
#line 1411 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("reservations", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.RESERVATIONS);
}
#line 2356 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 390:
#line 1416 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 2365 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 395:
#line 1429 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(m);
    ctx.stack_.push_back(m);
}
#line 2375 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 396:
#line 1433 "dhcp6_parser.yy" // lalr1.cc:859
    {
    /// @todo: an identifier parameter is required.
    ctx.stack_.pop_back();
}
#line 2384 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 397:
#line 1438 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // Parse the reservations list entry map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 2394 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 398:
#line 1442 "dhcp6_parser.yy" // lalr1.cc:859
    {
    /// @todo: an identifier parameter is required.
    // parsing completed
}
#line 2403 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 412:
#line 1467 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("ip-addresses", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2414 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 413:
#line 1472 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 2423 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 414:
#line 1477 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("prefixes", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2434 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 415:
#line 1482 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 2443 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 416:
#line 1487 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2451 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 417:
#line 1489 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr d(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("duid", d);
    ctx.leave();
}
#line 2461 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 418:
#line 1495 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2469 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 419:
#line 1497 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr hw(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("hw-address", hw);
    ctx.leave();
}
#line 2479 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 420:
#line 1503 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2487 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 421:
#line 1505 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr host(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("hostname", host);
    ctx.leave();
}
#line 2497 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 422:
#line 1511 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2505 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 423:
#line 1513 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr hw(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("flex-id", hw);
    ctx.leave();
}
#line 2515 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 424:
#line 1519 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr c(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("client-classes", c);
    ctx.stack_.push_back(c);
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2526 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 425:
#line 1524 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 2535 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 426:
#line 1532 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("relay", m);
    ctx.stack_.push_back(m);
    ctx.enter(ctx.RELAY);
}
#line 2546 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 427:
#line 1537 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 2555 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 428:
#line 1542 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2563 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 429:
#line 1544 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr ip(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("ip-address", ip);
    ctx.leave();
}
#line 2573 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 430:
#line 1553 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("client-classes", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.CLIENT_CLASSES);
}
#line 2584 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 431:
#line 1558 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 2593 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 434:
#line 1567 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(m);
    ctx.stack_.push_back(m);
}
#line 2603 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 435:
#line 1571 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // The name client class parameter is required.
    ctx.require("name", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.stack_.pop_back();
}
#line 2613 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 445:
#line 1593 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2621 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 446:
#line 1595 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr test(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("test", test);
    ctx.leave();
}
#line 2631 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 447:
#line 1604 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("server-id", m);
    ctx.stack_.push_back(m);
    ctx.enter(ctx.SERVER_ID);
}
#line 2642 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 448:
#line 1609 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // The type parameter is required.
    ctx.require("type", ctx.loc2pos(yystack_[2].location), ctx.loc2pos(yystack_[0].location));
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 2653 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 458:
#line 1629 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.DUID_TYPE);
}
#line 2661 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 459:
#line 1631 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.back()->set("type", yystack_[0].value.as< ElementPtr > ());
    ctx.leave();
}
#line 2670 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 460:
#line 1636 "dhcp6_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("LLT", ctx.loc2pos(yystack_[0].location))); }
#line 2676 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 461:
#line 1637 "dhcp6_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("EN", ctx.loc2pos(yystack_[0].location))); }
#line 2682 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 462:
#line 1638 "dhcp6_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("LL", ctx.loc2pos(yystack_[0].location))); }
#line 2688 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 463:
#line 1641 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr htype(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("htype", htype);
}
#line 2697 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 464:
#line 1646 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2705 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 465:
#line 1648 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr id(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("identifier", id);
    ctx.leave();
}
#line 2715 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 466:
#line 1654 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr time(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("time", time);
}
#line 2724 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 467:
#line 1659 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr time(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("enterprise-id", time);
}
#line 2733 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 468:
#line 1666 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr time(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("dhcp4o6-port", time);
}
#line 2742 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 469:
#line 1673 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("control-socket", m);
    ctx.stack_.push_back(m);
    ctx.enter(ctx.CONTROL_SOCKET);
}
#line 2753 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 470:
#line 1678 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 2762 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 475:
#line 1691 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2770 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 476:
#line 1693 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr stype(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("socket-type", stype);
    ctx.leave();
}
#line 2780 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 477:
#line 1699 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2788 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 478:
#line 1701 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr name(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("socket-name", name);
    ctx.leave();
}
#line 2798 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 479:
#line 1709 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("dhcp-ddns", m);
    ctx.stack_.push_back(m);
    ctx.enter(ctx.DHCP_DDNS);
}
#line 2809 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 480:
#line 1714 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // The enable updates DHCP DDNS parameter is required.
    ctx.require("enable-updates", ctx.loc2pos(yystack_[2].location), ctx.loc2pos(yystack_[0].location));
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 2820 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 481:
#line 1721 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // Parse the dhcp-ddns map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 2830 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 482:
#line 1725 "dhcp6_parser.yy" // lalr1.cc:859
    {
    // The enable updates DHCP DDNS parameter is required.
    ctx.require("enable-updates", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    // parsing completed
}
#line 2840 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 500:
#line 1752 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr b(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("enable-updates", b);
}
#line 2849 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 501:
#line 1757 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2857 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 502:
#line 1759 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr s(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("qualifying-suffix", s);
    ctx.leave();
}
#line 2867 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 503:
#line 1765 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2875 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 504:
#line 1767 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr s(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("server-ip", s);
    ctx.leave();
}
#line 2885 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 505:
#line 1773 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr i(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("server-port", i);
}
#line 2894 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 506:
#line 1778 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2902 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 507:
#line 1780 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr s(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("sender-ip", s);
    ctx.leave();
}
#line 2912 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 508:
#line 1786 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr i(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("sender-port", i);
}
#line 2921 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 509:
#line 1791 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr i(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("max-queue-size", i);
}
#line 2930 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 510:
#line 1796 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NCR_PROTOCOL);
}
#line 2938 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 511:
#line 1798 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.back()->set("ncr-protocol", yystack_[0].value.as< ElementPtr > ());
    ctx.leave();
}
#line 2947 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 512:
#line 1804 "dhcp6_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("UDP", ctx.loc2pos(yystack_[0].location))); }
#line 2953 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 513:
#line 1805 "dhcp6_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("TCP", ctx.loc2pos(yystack_[0].location))); }
#line 2959 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 514:
#line 1808 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NCR_FORMAT);
}
#line 2967 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 515:
#line 1810 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr json(new StringElement("JSON", ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("ncr-format", json);
    ctx.leave();
}
#line 2977 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 516:
#line 1816 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr b(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("always-include-fqdn", b);
}
#line 2986 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 517:
#line 1821 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr b(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("override-no-update", b);
}
#line 2995 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 518:
#line 1826 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr b(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("override-client-update", b);
}
#line 3004 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 519:
#line 1831 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.REPLACE_CLIENT_NAME);
}
#line 3012 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 520:
#line 1833 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.back()->set("replace-client-name", yystack_[0].value.as< ElementPtr > ());
    ctx.leave();
}
#line 3021 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 521:
#line 1839 "dhcp6_parser.yy" // lalr1.cc:859
    {
      yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("when-present", ctx.loc2pos(yystack_[0].location))); 
      }
#line 3029 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 522:
#line 1842 "dhcp6_parser.yy" // lalr1.cc:859
    {
      yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("never", ctx.loc2pos(yystack_[0].location)));
      }
#line 3037 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 523:
#line 1845 "dhcp6_parser.yy" // lalr1.cc:859
    {
      yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("always", ctx.loc2pos(yystack_[0].location)));
      }
#line 3045 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 524:
#line 1848 "dhcp6_parser.yy" // lalr1.cc:859
    {
      yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("when-not-present", ctx.loc2pos(yystack_[0].location)));
      }
#line 3053 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 525:
#line 1851 "dhcp6_parser.yy" // lalr1.cc:859
    {
      error(yystack_[0].location, "boolean values for the replace-client-name are "
                "no longer supported");
      }
#line 3062 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 526:
#line 1857 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 3070 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 527:
#line 1859 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr s(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("generated-prefix", s);
    ctx.leave();
}
#line 3080 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 528:
#line 1867 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 3088 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 529:
#line 1869 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.back()->set("Dhcp4", yystack_[0].value.as< ElementPtr > ());
    ctx.leave();
}
#line 3097 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 530:
#line 1874 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 3105 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 531:
#line 1876 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.back()->set("DhcpDdns", yystack_[0].value.as< ElementPtr > ());
    ctx.leave();
}
#line 3114 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 532:
#line 1881 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 3122 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 533:
#line 1883 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.back()->set("Control-agent", yystack_[0].value.as< ElementPtr > ());
    ctx.leave();
}
#line 3131 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 534:
#line 1894 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("Logging", m);
    ctx.stack_.push_back(m);
    ctx.enter(ctx.LOGGING);
}
#line 3142 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 535:
#line 1899 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 3151 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 539:
#line 1916 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("loggers", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.LOGGERS);
}
#line 3162 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 540:
#line 1921 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 3171 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 543:
#line 1933 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(l);
    ctx.stack_.push_back(l);
}
#line 3181 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 544:
#line 1937 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
}
#line 3189 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 552:
#line 1952 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr dl(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("debuglevel", dl);
}
#line 3198 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 553:
#line 1957 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 3206 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 554:
#line 1959 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr sev(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("severity", sev);
    ctx.leave();
}
#line 3216 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 555:
#line 1965 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("output_options", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.OUTPUT_OPTIONS);
}
#line 3227 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 556:
#line 1970 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 3236 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 559:
#line 1979 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(m);
    ctx.stack_.push_back(m);
}
#line 3246 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 560:
#line 1983 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
}
#line 3254 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 567:
#line 1997 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 3262 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 568:
#line 1999 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr sev(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("output", sev);
    ctx.leave();
}
#line 3272 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 569:
#line 2005 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr flush(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("flush", flush);
}
#line 3281 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 570:
#line 2010 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr maxsize(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("maxsize", maxsize);
}
#line 3290 "dhcp6_parser.cc" // lalr1.cc:859
    break;

  case 571:
#line 2015 "dhcp6_parser.yy" // lalr1.cc:859
    {
    ElementPtr maxver(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("maxver", maxver);
}
#line 3299 "dhcp6_parser.cc" // lalr1.cc:859
    break;


#line 3303 "dhcp6_parser.cc" // lalr1.cc:859
            default:
              break;
            }
        }
      catch (const syntax_error& yyexc)
        {
          error (yyexc);
          YYERROR;
        }
      YY_SYMBOL_PRINT ("-> $$ =", yylhs);
      yypop_ (yylen);
      yylen = 0;
      YY_STACK_PRINT ();

      // Shift the result of the reduction.
      yypush_ (YY_NULLPTR, yylhs);
    }
    goto yynewstate;

  /*--------------------------------------.
  | yyerrlab -- here on detecting error.  |
  `--------------------------------------*/
  yyerrlab:
    // If not already recovering from an error, report this error.
    if (!yyerrstatus_)
      {
        ++yynerrs_;
        error (yyla.location, yysyntax_error_ (yystack_[0].state, yyla));
      }


    yyerror_range[1].location = yyla.location;
    if (yyerrstatus_ == 3)
      {
        /* If just tried and failed to reuse lookahead token after an
           error, discard it.  */

        // Return failure if at end of input.
        if (yyla.type_get () == yyeof_)
          YYABORT;
        else if (!yyla.empty ())
          {
            yy_destroy_ ("Error: discarding", yyla);
            yyla.clear ();
          }
      }

    // Else will try to reuse lookahead token after shifting the error token.
    goto yyerrlab1;


  /*---------------------------------------------------.
  | yyerrorlab -- error raised explicitly by YYERROR.  |
  `---------------------------------------------------*/
  yyerrorlab:

    /* Pacify compilers like GCC when the user code never invokes
       YYERROR and the label yyerrorlab therefore never appears in user
       code.  */
    if (false)
      goto yyerrorlab;
    yyerror_range[1].location = yystack_[yylen - 1].location;
    /* Do not reclaim the symbols of the rule whose action triggered
       this YYERROR.  */
    yypop_ (yylen);
    yylen = 0;
    goto yyerrlab1;

  /*-------------------------------------------------------------.
  | yyerrlab1 -- common code for both syntax error and YYERROR.  |
  `-------------------------------------------------------------*/
  yyerrlab1:
    yyerrstatus_ = 3;   // Each real token shifted decrements this.
    {
      stack_symbol_type error_token;
      for (;;)
        {
          yyn = yypact_[yystack_[0].state];
          if (!yy_pact_value_is_default_ (yyn))
            {
              yyn += yyterror_;
              if (0 <= yyn && yyn <= yylast_ && yycheck_[yyn] == yyterror_)
                {
                  yyn = yytable_[yyn];
                  if (0 < yyn)
                    break;
                }
            }

          // Pop the current state because it cannot handle the error token.
          if (yystack_.size () == 1)
            YYABORT;

          yyerror_range[1].location = yystack_[0].location;
          yy_destroy_ ("Error: popping", yystack_[0]);
          yypop_ ();
          YY_STACK_PRINT ();
        }

      yyerror_range[2].location = yyla.location;
      YYLLOC_DEFAULT (error_token.location, yyerror_range, 2);

      // Shift the error token.
      error_token.state = yyn;
      yypush_ ("Shifting", error_token);
    }
    goto yynewstate;

    // Accept.
  yyacceptlab:
    yyresult = 0;
    goto yyreturn;

    // Abort.
  yyabortlab:
    yyresult = 1;
    goto yyreturn;

  yyreturn:
    if (!yyla.empty ())
      yy_destroy_ ("Cleanup: discarding lookahead", yyla);

    /* Do not reclaim the symbols of the rule whose action triggered
       this YYABORT or YYACCEPT.  */
    yypop_ (yylen);
    while (1 < yystack_.size ())
      {
        yy_destroy_ ("Cleanup: popping", yystack_[0]);
        yypop_ ();
      }

    return yyresult;
  }
    catch (...)
      {
        YYCDEBUG << "Exception caught: cleaning lookahead and stack"
                 << std::endl;
        // Do not try to display the values of the reclaimed symbols,
        // as their printer might throw an exception.
        if (!yyla.empty ())
          yy_destroy_ (YY_NULLPTR, yyla);

        while (1 < yystack_.size ())
          {
            yy_destroy_ (YY_NULLPTR, yystack_[0]);
            yypop_ ();
          }
        throw;
      }
  }

  void
  Dhcp6Parser::error (const syntax_error& yyexc)
  {
    error (yyexc.location, yyexc.what());
  }

  // Generate an error message.
  std::string
  Dhcp6Parser::yysyntax_error_ (state_type yystate, const symbol_type& yyla) const
  {
    // Number of reported tokens (one for the "unexpected", one per
    // "expected").
    size_t yycount = 0;
    // Its maximum.
    enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
    // Arguments of yyformat.
    char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];

    /* There are many possibilities here to consider:
       - If this state is a consistent state with a default action, then
         the only way this function was invoked is if the default action
         is an error action.  In that case, don't check for expected
         tokens because there are none.
       - The only way there can be no lookahead present (in yyla) is
         if this state is a consistent state with a default action.
         Thus, detecting the absence of a lookahead is sufficient to
         determine that there is no unexpected or expected token to
         report.  In that case, just report a simple "syntax error".
       - Don't assume there isn't a lookahead just because this state is
         a consistent state with a default action.  There might have
         been a previous inconsistent state, consistent state with a
         non-default action, or user semantic action that manipulated
         yyla.  (However, yyla is currently not documented for users.)
       - Of course, the expected token list depends on states to have
         correct lookahead information, and it depends on the parser not
         to perform extra reductions after fetching a lookahead from the
         scanner and before detecting a syntax error.  Thus, state
         merging (from LALR or IELR) and default reductions corrupt the
         expected token list.  However, the list is correct for
         canonical LR with one exception: it will still contain any
         token that will not be accepted due to an error action in a
         later state.
    */
    if (!yyla.empty ())
      {
        int yytoken = yyla.type_get ();
        yyarg[yycount++] = yytname_[yytoken];
        int yyn = yypact_[yystate];
        if (!yy_pact_value_is_default_ (yyn))
          {
            /* Start YYX at -YYN if negative to avoid negative indexes in
               YYCHECK.  In other words, skip the first -YYN actions for
               this state because they are default actions.  */
            int yyxbegin = yyn < 0 ? -yyn : 0;
            // Stay within bounds of both yycheck and yytname.
            int yychecklim = yylast_ - yyn + 1;
            int yyxend = yychecklim < yyntokens_ ? yychecklim : yyntokens_;
            for (int yyx = yyxbegin; yyx < yyxend; ++yyx)
              if (yycheck_[yyx + yyn] == yyx && yyx != yyterror_
                  && !yy_table_value_is_error_ (yytable_[yyx + yyn]))
                {
                  if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                    {
                      yycount = 1;
                      break;
                    }
                  else
                    yyarg[yycount++] = yytname_[yyx];
                }
          }
      }

    char const* yyformat = YY_NULLPTR;
    switch (yycount)
      {
#define YYCASE_(N, S)                         \
        case N:                               \
          yyformat = S;                       \
        break
        YYCASE_(0, YY_("syntax error"));
        YYCASE_(1, YY_("syntax error, unexpected %s"));
        YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
        YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
        YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
        YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
#undef YYCASE_
      }

    std::string yyres;
    // Argument number.
    size_t yyi = 0;
    for (char const* yyp = yyformat; *yyp; ++yyp)
      if (yyp[0] == '%' && yyp[1] == 's' && yyi < yycount)
        {
          yyres += yytnamerr_ (yyarg[yyi++]);
          ++yyp;
        }
      else
        yyres += *yyp;
    return yyres;
  }


  const short int Dhcp6Parser::yypact_ninf_ = -705;

  const signed char Dhcp6Parser::yytable_ninf_ = -1;

  const short int
  Dhcp6Parser::yypact_[] =
  {
     191,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,    80,    23,    24,    75,   109,   116,
     118,   132,   160,   166,   179,   189,   200,   215,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,    23,     1,    14,   123,    49,    16,
      85,    31,   173,   176,    65,   103,   -21,   244,  -705,   275,
     242,   276,   282,   267,  -705,  -705,  -705,  -705,  -705,   296,
    -705,    44,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,   298,   299,   304,   306,   308,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,   314,  -705,  -705,
    -705,    92,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,   318,   101,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,   319,   343,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,   107,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,   146,  -705,  -705,
    -705,  -705,   344,  -705,   363,   364,  -705,  -705,  -705,   171,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,   361,   367,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,   365,  -705,  -705,   368,  -705,
    -705,  -705,   370,  -705,  -705,   369,   372,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,   374,   375,
    -705,  -705,  -705,  -705,   373,   377,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,   177,  -705,  -705,  -705,
     378,  -705,  -705,   379,  -705,   380,   381,  -705,  -705,   382,
     383,   384,  -705,  -705,  -705,   197,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,    23,    23,  -705,   219,   385,   386,   387,   388,
     389,  -705,    14,  -705,   390,   391,   393,   223,   245,   246,
     247,   248,   398,   399,   400,   401,   402,   403,   404,   405,
     406,   407,   408,   260,   410,   411,   123,  -705,   412,   265,
      49,  -705,    29,   413,   414,   415,   417,   418,   419,   272,
     269,   422,   423,   427,   428,    16,  -705,   429,    85,  -705,
     430,   283,   431,   284,   285,    31,  -705,   435,   436,   437,
     438,   439,   440,   441,  -705,   173,  -705,   442,   445,   297,
     447,   448,   449,   302,  -705,    65,   450,   303,   305,  -705,
     103,   455,   457,   -13,  -705,   307,   459,   460,   312,   464,
     317,   320,   467,   468,   321,   322,   323,   471,   475,   244,
    -705,  -705,  -705,   476,   474,   477,    23,    23,    23,  -705,
     478,   480,   481,  -705,  -705,  -705,  -705,  -705,   484,   485,
     486,   487,   488,   489,   490,   491,   492,   493,   494,  -705,
     495,   496,  -705,   499,  -705,  -705,  -705,  -705,  -705,  -705,
     497,   479,  -705,  -705,  -705,   501,   502,   503,   331,   346,
     347,  -705,  -705,   262,   356,   506,   505,  -705,   362,  -705,
     366,  -705,   371,  -705,  -705,  -705,   499,   499,   499,   376,
     392,   394,   395,  -705,   396,   397,  -705,   409,   416,   420,
    -705,  -705,   421,  -705,  -705,  -705,   424,    23,  -705,  -705,
     425,   426,  -705,   432,  -705,  -705,   -18,   433,  -705,  -705,
    -705,    46,   434,  -705,    23,   123,   443,  -705,  -705,  -705,
      49,    13,    13,   508,   509,   510,   512,   -32,    23,   161,
      53,   513,   142,    30,   193,   244,  -705,  -705,   517,  -705,
      29,   515,   518,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,   520,   451,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,   205,  -705,
     207,  -705,  -705,   218,  -705,  -705,  -705,  -705,   525,   526,
     527,   528,   530,  -705,  -705,  -705,   232,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
     239,  -705,   507,   521,  -705,  -705,   529,   533,  -705,  -705,
     531,   535,  -705,  -705,   534,   536,  -705,  -705,  -705,   111,
    -705,  -705,  -705,   537,  -705,  -705,  -705,   130,  -705,  -705,
    -705,  -705,   217,  -705,   539,   538,  -705,   546,   547,   548,
     549,   550,   552,   251,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,   553,   554,   555,  -705,  -705,   253,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,   254,  -705,  -705,
    -705,   255,   444,   446,  -705,  -705,   556,   557,  -705,  -705,
     558,   560,  -705,  -705,   559,   563,  -705,  -705,   561,  -705,
     566,   443,  -705,  -705,   570,   571,   575,   576,   452,   453,
     454,   456,   461,   577,   578,    13,  -705,  -705,    16,  -705,
     508,    65,  -705,   509,   103,  -705,   510,    59,  -705,   512,
     -32,  -705,  -705,   161,  -705,    53,  -705,   -21,  -705,   513,
     462,   463,   465,   466,   469,   470,   142,  -705,   579,   581,
     472,   473,   482,    30,  -705,   583,   584,   193,  -705,  -705,
    -705,   585,   564,  -705,    85,  -705,   515,    31,  -705,   518,
     173,  -705,   520,   586,  -705,   587,  -705,   266,   458,   498,
     500,  -705,  -705,  -705,  -705,  -705,   504,   524,  -705,   257,
    -705,   589,  -705,   591,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,   261,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,   263,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,   250,   532,  -705,  -705,  -705,  -705,
     540,   541,  -705,  -705,   542,   264,  -705,   270,  -705,   592,
    -705,   543,   582,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,    59,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,   288,  -705,  -705,    66,   582,  -705,  -705,
     590,  -705,  -705,  -705,   271,  -705,  -705,  -705,  -705,  -705,
     597,   483,   598,    66,  -705,   588,  -705,   544,  -705,   596,
    -705,  -705,   292,  -705,    69,   596,  -705,  -705,   600,   602,
     605,   274,  -705,  -705,  -705,  -705,  -705,  -705,   608,   514,
     511,   519,    69,  -705,   551,  -705,  -705,  -705,  -705,  -705
  };

  const unsigned short int
  Dhcp6Parser::yydefact_[] =
  {
       0,     2,     4,     6,     8,    10,    12,    14,    16,    18,
      20,    22,    24,    26,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     1,    43,
      36,    32,    31,    28,    29,    30,    35,     3,    33,    34,
      56,     5,    68,     7,   102,     9,   216,    11,   350,    13,
     370,    15,   397,    17,   279,    19,   287,    21,   322,    23,
     181,    25,   481,    27,    45,    39,     0,     0,     0,     0,
       0,     0,   399,     0,   289,   324,     0,     0,    47,     0,
      46,     0,     0,    40,    66,   534,   528,   530,   532,     0,
      65,     0,    58,    60,    62,    63,    64,    61,   100,   111,
     113,     0,     0,     0,     0,     0,   208,   277,   314,   253,
     154,   171,   162,   430,   173,   192,   447,     0,   469,   479,
      94,     0,    70,    72,    73,    74,    75,    76,    79,    80,
      81,    82,    84,    83,    88,    89,    77,    78,    86,    87,
      85,    90,    91,    92,    93,   108,     0,     0,   104,   106,
     107,   434,   342,   362,   360,   238,   240,   242,     0,     0,
     246,   244,   389,   426,   237,   220,   221,   222,   223,     0,
     218,   227,   228,   229,   232,   234,   230,   231,   224,   225,
     236,   226,   233,   235,   358,   357,   355,     0,   352,   354,
     356,   382,     0,   385,     0,     0,   381,   377,   380,     0,
     372,   374,   375,   378,   379,   376,   424,   412,   414,   416,
     418,   420,   422,   411,   410,     0,   400,   401,   405,   406,
     403,   407,   408,   409,   404,     0,   304,   144,     0,   308,
     306,   311,     0,   300,   301,     0,   290,   291,   293,   303,
     294,   295,   296,   310,   297,   298,   299,   336,     0,     0,
     334,   335,   338,   339,     0,   325,   326,   328,   329,   330,
     331,   332,   333,   188,   190,   185,     0,   183,   186,   187,
       0,   501,   503,     0,   506,     0,     0,   510,   514,     0,
       0,     0,   519,   526,   499,     0,   483,   485,   486,   487,
     488,   489,   490,   491,   492,   493,   494,   495,   496,   497,
     498,    44,     0,     0,    37,     0,     0,     0,     0,     0,
       0,    55,     0,    57,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    69,     0,     0,
       0,   103,   436,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   217,     0,     0,   351,
       0,     0,     0,     0,     0,     0,   371,     0,     0,     0,
       0,     0,     0,     0,   398,     0,   280,     0,     0,     0,
       0,     0,     0,     0,   288,     0,     0,     0,     0,   323,
       0,     0,     0,     0,   182,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     482,    48,    41,     0,     0,     0,     0,     0,     0,    59,
       0,     0,     0,    95,    96,    97,    98,    99,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   468,
       0,     0,    71,     0,   110,   105,   445,   443,   444,   442,
       0,   437,   438,   440,   441,     0,     0,     0,     0,     0,
       0,   251,   252,     0,     0,     0,     0,   219,     0,   353,
       0,   384,     0,   387,   388,   373,     0,     0,     0,     0,
       0,     0,     0,   402,     0,     0,   302,     0,     0,     0,
     313,   292,     0,   340,   341,   327,     0,     0,   184,   500,
       0,     0,   505,     0,   508,   509,     0,     0,   516,   517,
     518,     0,     0,   484,     0,     0,     0,   529,   531,   533,
       0,     0,     0,   210,   281,   316,   255,     0,    45,     0,
       0,   175,     0,     0,     0,     0,    49,   109,     0,   435,
       0,   344,   364,    38,   361,   239,   241,   243,   248,   249,
     250,   247,   245,   391,     0,   359,   383,   386,   425,   413,
     415,   417,   419,   421,   423,   305,   145,   309,   307,   312,
     337,   189,   191,   502,   504,   507,   512,   513,   511,   515,
     521,   522,   523,   524,   525,   520,   527,    42,     0,   539,
       0,   536,   538,     0,   130,   137,   139,   141,     0,     0,
       0,     0,     0,   150,   152,   129,     0,   115,   117,   118,
     119,   120,   121,   122,   123,   124,   125,   126,   127,   128,
       0,   214,     0,   211,   212,   285,     0,   282,   283,   320,
       0,   317,   318,   259,     0,   256,   257,   160,   161,     0,
     156,   158,   159,     0,   169,   170,   166,     0,   164,   167,
     168,   432,     0,   179,     0,   176,   177,     0,     0,     0,
       0,     0,     0,     0,   194,   196,   197,   198,   199,   200,
     201,   458,   464,     0,     0,     0,   457,   456,     0,   449,
     451,   454,   452,   453,   455,   475,   477,     0,   471,   473,
     474,     0,    51,     0,   439,   348,     0,   345,   346,   368,
       0,   365,   366,   395,     0,   392,   393,   428,     0,    67,
       0,     0,   535,   101,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   112,   114,     0,   209,
       0,   289,   278,     0,   324,   315,     0,     0,   254,     0,
       0,   155,   172,     0,   163,     0,   431,     0,   174,     0,
       0,     0,     0,     0,     0,     0,     0,   193,     0,     0,
       0,     0,     0,     0,   448,     0,     0,     0,   470,   480,
      53,     0,    52,   446,     0,   343,     0,     0,   363,     0,
     399,   390,     0,     0,   427,     0,   537,     0,     0,     0,
       0,   143,   146,   147,   148,   149,     0,     0,   116,     0,
     213,     0,   284,     0,   319,   276,   273,   275,   267,   268,
     263,   264,   265,   266,   272,   271,   274,     0,   261,   269,
     270,   258,   157,   165,   433,     0,   178,   202,   203,   204,
     205,   206,   207,   195,     0,     0,   463,   466,   467,   450,
       0,     0,   472,    50,     0,     0,   347,     0,   367,     0,
     394,     0,     0,   132,   133,   134,   135,   136,   131,   138,
     140,   142,   151,   153,   215,   286,   321,     0,   260,   180,
     460,   461,   462,   459,   465,   476,   478,    54,   349,   369,
     396,   429,   543,     0,   541,   262,     0,     0,   540,   555,
       0,   553,   551,   547,     0,   545,   549,   550,   548,   542,
       0,     0,     0,     0,   544,     0,   552,     0,   546,     0,
     554,   559,     0,   557,     0,     0,   556,   567,     0,     0,
       0,     0,   561,   563,   564,   565,   566,   558,     0,     0,
       0,     0,     0,   560,     0,   569,   570,   571,   562,   568
  };

  const short int
  Dhcp6Parser::yypgoto_[] =
  {
    -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,    12,  -705,   138,  -705,  -705,
    -705,  -705,  -705,  -705,    89,  -705,  -112,  -705,  -705,  -705,
     -66,  -705,  -705,  -705,   230,  -705,  -705,  -705,  -705,    98,
     291,   -62,   -50,   -49,   -48,  -705,  -705,  -705,  -705,  -705,
     100,   281,  -705,  -705,  -705,  -705,  -705,  -705,  -705,   102,
     -97,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,   -74,  -705,  -520,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -111,  -507,  -705,  -705,  -705,  -705,
    -113,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -118,
    -705,  -705,  -705,  -115,   240,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,  -122,  -705,  -705,  -705,  -705,  -705,  -705,  -704,
    -705,  -705,  -705,   -93,  -705,  -705,  -705,   -90,   286,  -705,
    -705,  -703,  -705,  -683,  -705,  -518,  -705,  -680,  -705,  -705,
    -705,  -679,  -705,  -705,  -705,  -705,  -100,  -705,  -705,  -227,
     569,  -705,  -705,  -705,  -705,  -705,   -89,  -705,  -705,  -705,
     -88,  -705,   268,  -705,   -73,  -705,  -705,  -705,  -705,  -705,
     -61,  -705,  -705,  -705,  -705,  -705,   -54,  -705,  -705,  -705,
     -91,  -705,  -705,  -705,   -87,  -705,   256,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -128,  -705,
    -705,  -705,  -125,   300,  -705,  -705,   -45,  -705,  -705,  -705,
    -705,  -705,  -124,  -705,  -705,  -705,  -123,   325,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -105,
    -705,  -705,  -705,  -110,  -705,   310,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
    -678,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
     117,  -705,  -705,  -705,  -705,  -705,  -705,   -96,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
     -85,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,   151,
     289,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,  -705,
    -705,  -705,  -705,   -20,  -705,  -705,  -705,  -188,  -705,  -705,
    -203,  -705,  -705,  -705,  -705,  -705,  -705,  -214,  -705,  -705,
    -230,  -705,  -705,  -705,  -705,  -705
  };

  const short int
  Dhcp6Parser::yydefgoto_[] =
  {
      -1,    14,    15,    16,    17,    18,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    78,    37,    38,    65,   544,
      82,    83,    39,    64,    79,    80,   537,   692,   771,   772,
     120,    41,    66,    91,    92,    93,   306,    43,    67,   121,
     122,   123,   124,   125,   126,   127,   128,   314,    45,    68,
     147,   148,   149,   338,   150,   129,   315,   130,   316,   606,
     607,   608,   714,   858,   609,   715,   610,   716,   611,   717,
     612,   234,   378,   614,   615,   616,   617,   618,   723,   619,
     724,   131,   326,   639,   640,   641,   642,   132,   328,   647,
     648,   649,   650,   133,   327,   134,   330,   654,   655,   656,
     747,    61,    76,   266,   267,   268,   391,   269,   392,   135,
     331,   663,   664,   665,   666,   667,   668,   669,   670,   136,
     322,   622,   623,   624,   728,    47,    69,   169,   170,   171,
     346,   172,   347,   173,   348,   174,   352,   175,   351,   551,
     176,   177,   137,   325,   634,   635,   636,   737,   817,   818,
     138,   323,    55,    73,   626,   627,   628,   731,    57,    74,
     235,   236,   237,   238,   239,   240,   241,   377,   242,   381,
     243,   380,   244,   245,   382,   246,   139,   324,   630,   631,
     632,   734,    59,    75,   254,   255,   256,   257,   258,   386,
     259,   260,   261,   262,   179,   343,   696,   697,   698,   774,
      49,    70,   187,   188,   189,   357,   180,   345,   181,   344,
     700,   701,   702,   777,    51,    71,   199,   200,   201,   360,
     202,   203,   362,   204,   205,   182,   353,   704,   705,   706,
     780,    53,    72,   215,   216,   217,   218,   368,   219,   369,
     220,   370,   221,   371,   222,   372,   223,   373,   224,   367,
     183,   354,   708,   783,   140,   329,   652,   342,   450,   451,
     452,   453,   454,   538,   141,   332,   678,   679,   680,   758,
     873,   681,   682,   759,   683,   684,   142,   143,   334,   687,
     688,   689,   765,   690,   766,   144,   335,    63,    77,   285,
     286,   287,   288,   396,   289,   397,   290,   291,   399,   292,
     293,   294,   402,   578,   295,   403,   296,   297,   298,   299,
     407,   585,   300,   408,    94,   308,    95,   309,    96,   310,
      97,   307,   590,   591,   592,   710,   883,   884,   886,   894,
     895,   896,   897,   902,   898,   900,   912,   913,   914,   921,
     922,   923,   928,   924,   925,   926
  };

  const unsigned short int
  Dhcp6Parser::yytable_[] =
  {
      90,   251,   252,   164,   185,   196,   213,   165,   233,   250,
     265,   284,   651,   677,   253,   178,   186,   197,   214,   166,
     167,   168,   646,   151,    84,   190,   198,    36,    29,   594,
      30,    40,    31,   811,   812,   595,   596,   597,   598,   599,
     600,   601,   602,   603,   604,   637,   671,   312,   101,   102,
     103,   104,   313,   227,   813,   108,   599,   815,   816,   820,
     151,   145,   146,   263,   264,   152,   151,   153,   108,   227,
     108,   263,   264,   154,   155,   156,   157,   158,   159,   160,
      28,   226,    42,   191,   192,   193,   194,   195,   154,   161,
     162,   101,   102,   103,   104,   336,   106,   163,   108,   227,
     337,   446,   576,   577,   340,   227,   227,   228,   229,   341,
     355,   230,   231,   232,   740,   356,    44,   741,   156,   157,
     638,   159,   160,    46,   108,    48,   161,   672,   673,   674,
     675,    89,   161,   743,    98,   184,   744,    99,   100,    50,
     163,    85,   154,   227,   247,   228,   229,   248,   249,   358,
      86,    87,    88,    81,   359,   101,   102,   103,   104,   105,
     106,   107,   108,   811,   812,    89,    89,    52,    89,   580,
     581,   582,   583,    54,   365,    32,    33,    34,    35,   366,
     393,    89,    89,    89,   813,   394,    56,   815,   816,   820,
     109,   110,   111,   112,   113,   889,    58,   890,   891,   917,
     409,   584,   918,   919,   920,   410,   114,    60,   336,   115,
     711,    89,   108,   709,   107,   712,   116,    89,    89,   814,
     745,   340,    62,   746,   117,   118,   713,   824,   119,   657,
     658,   659,   660,   661,   662,   725,   646,    89,   637,   644,
     726,   645,   725,   677,   206,   302,    90,   727,   207,   208,
     209,   210,   211,   212,   756,    89,   763,   767,   409,   757,
     355,   764,   768,   769,   867,   864,   393,   358,   448,   868,
     305,   869,   878,   365,   903,    89,   447,   932,   879,   904,
     303,   301,   933,   853,   854,   855,   856,   857,   449,   164,
     304,   887,   185,   165,   888,   915,   685,   686,   916,   196,
     311,   178,   317,   318,   186,   166,   167,   168,   319,   213,
     320,   197,   321,   190,   411,   412,   251,   252,   333,   233,
     198,   214,   339,   349,   250,    89,   548,   549,   550,   253,
       1,     2,     3,     4,     5,     6,     7,     8,     9,    10,
      11,    12,    13,   284,   870,   871,   872,   350,   361,   814,
     270,   271,   272,   273,   274,   275,   276,   277,   278,   279,
     280,   281,   282,   283,   558,   559,   560,   363,   364,   374,
     375,   413,   379,   376,   383,   385,   423,   384,   387,   388,
     390,   389,   395,   398,   400,   401,   404,   405,   406,   414,
     415,   416,   417,   418,   420,   421,    89,   422,   424,   425,
     426,   427,   428,   429,   430,   431,   432,   433,   434,   435,
     436,   437,   438,   439,   440,   441,   443,   455,   456,   457,
     444,   458,   459,   460,   462,   461,   463,   464,   517,   518,
     519,   465,   466,   468,   470,   472,   471,   473,   474,   476,
     477,   478,   479,   480,   481,   482,   484,   613,   613,   485,
     486,   487,   488,   489,   492,   605,   605,   490,   493,   496,
     494,   497,   499,   500,   501,   502,   448,   676,   503,   284,
     504,   506,   507,   505,   447,   511,   508,   509,   510,   512,
     514,   515,   540,   545,   516,   520,   449,   521,   522,   523,
     524,   525,   526,   527,   528,   529,   530,   531,   546,   547,
     532,   533,   534,   535,   536,   539,   541,   542,   552,   572,
      30,   553,   554,   729,   555,   621,   625,   629,   556,   633,
     653,   693,   695,   557,   730,   699,   587,   703,   561,   718,
     719,   720,   721,   707,   722,   732,   733,   735,   736,   739,
     738,   749,   419,   742,   562,   748,   563,   564,   565,   566,
     750,   751,   752,   753,   754,   579,   755,   760,   761,   762,
     776,   567,   775,   779,   778,   781,   782,   844,   568,   784,
     785,   589,   569,   570,   787,   788,   571,   573,   574,   789,
     790,   796,   797,   834,   575,   835,   586,   840,   841,   882,
     851,   843,   852,   909,   901,   543,   770,   865,   773,   866,
     880,   905,   907,   911,   929,   791,   930,   793,   792,   931,
     859,   794,   934,   588,   795,   827,   828,   643,   829,   830,
     593,   445,   831,   832,   620,   836,   837,   442,   798,   822,
     823,   826,   825,   498,   833,   838,   906,   800,   799,   821,
     885,   467,   225,   801,   802,   804,   495,   803,   846,   845,
     860,   613,   861,   491,   847,   848,   862,   694,   469,   605,
     251,   252,   164,   810,   936,   233,   165,   839,   250,   935,
     849,   805,   937,   253,   178,   806,   863,   850,   166,   167,
     168,   265,   842,   819,   874,   483,   691,   807,   808,   809,
     475,   786,   875,   876,   877,   881,   910,   676,   513,   899,
     908,   927,   938,   939,     0,     0,     0,     0,   185,     0,
       0,   196,     0,     0,   213,     0,     0,     0,     0,     0,
     186,     0,     0,   197,     0,     0,   214,     0,     0,   190,
       0,     0,   198,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   810,     0,     0,     0,     0,     0,     0,
       0,   805,     0,     0,     0,   806,     0,     0,     0,     0,
       0,     0,   893,   819,     0,     0,     0,   807,   808,   809,
     892,     0,     0,     0,     0,     0,     0,     0,     0,   893,
       0,     0,     0,     0,     0,     0,     0,   892
  };

  const short int
  Dhcp6Parser::yycheck_[] =
  {
      66,    75,    75,    69,    70,    71,    72,    69,    74,    75,
      76,    77,   530,   533,    75,    69,    70,    71,    72,    69,
      69,    69,   529,     7,    10,    70,    71,    15,     5,    16,
       7,     7,     9,   737,   737,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    77,    16,     3,    32,    33,
      34,    35,     8,    40,   737,    39,    26,   737,   737,   737,
       7,    12,    13,    84,    85,    49,     7,    51,    39,    40,
      39,    84,    85,    57,    58,    59,    60,    61,    62,    63,
       0,    16,     7,    52,    53,    54,    55,    56,    57,    73,
      74,    32,    33,    34,    35,     3,    37,    81,    39,    40,
       8,    72,   120,   121,     3,    40,    40,    42,    43,     8,
       3,    46,    47,    48,     3,     8,     7,     6,    59,    60,
     152,    62,    63,     7,    39,     7,    73,    97,    98,    99,
     100,   152,    73,     3,    11,    50,     6,    14,    15,     7,
      81,   127,    57,    40,    41,    42,    43,    44,    45,     3,
     136,   137,   138,   152,     8,    32,    33,    34,    35,    36,
      37,    38,    39,   867,   867,   152,   152,     7,   152,   123,
     124,   125,   126,     7,     3,   152,   153,   154,   155,     8,
       3,   152,   152,   152,   867,     8,     7,   867,   867,   867,
      67,    68,    69,    70,    71,   129,     7,   131,   132,   130,
       3,   155,   133,   134,   135,     8,    83,     7,     3,    86,
       3,   152,    39,     8,    38,     8,    93,   152,   152,   737,
       3,     3,     7,     6,   101,   102,     8,   745,   105,    87,
      88,    89,    90,    91,    92,     3,   743,   152,    77,    78,
       8,    80,     3,   763,    71,     3,   312,     8,    75,    76,
      77,    78,    79,    80,     3,   152,     3,     3,     3,     8,
       3,     8,     8,     8,     3,     8,     3,     3,   342,     8,
       3,     8,     8,     3,     3,   152,   342,     3,     8,     8,
       4,     6,     8,    17,    18,    19,    20,    21,   342,   355,
       8,     3,   358,   355,     6,     3,   103,   104,     6,   365,
       4,   355,     4,     4,   358,   355,   355,   355,     4,   375,
       4,   365,     4,   358,   302,   303,   390,   390,     4,   385,
     365,   375,     4,     4,   390,   152,    64,    65,    66,   390,
     139,   140,   141,   142,   143,   144,   145,   146,   147,   148,
     149,   150,   151,   409,    94,    95,    96,     4,     4,   867,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   476,   477,   478,     4,     4,     8,
       3,   152,     4,     8,     4,     3,   153,     8,     4,     4,
       3,     8,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,   152,     4,   153,   153,
     153,   153,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     4,   153,     4,     4,     4,     4,     4,     4,
     155,     4,     4,     4,   155,   153,     4,     4,   416,   417,
     418,     4,     4,     4,     4,     4,   153,   153,   153,     4,
       4,     4,     4,     4,     4,     4,     4,   521,   522,     4,
     153,     4,     4,     4,     4,   521,   522,   155,   155,     4,
     155,     4,   155,     4,     4,   153,   540,   533,     4,   535,
     153,     4,     4,   153,   540,     4,   155,   155,   155,     4,
       4,     7,     3,   152,     7,     7,   540,     7,     7,     5,
       5,     5,     5,     5,     5,     5,     5,     5,   152,   152,
       7,     7,     7,     7,     5,     8,     5,     5,   152,   497,
       7,     5,     7,     6,   152,     7,     7,     7,   152,     7,
       7,     4,     7,   152,     3,     7,   514,     7,   152,     4,
       4,     4,     4,    82,     4,     6,     3,     6,     3,     3,
       6,     3,   312,     6,   152,     6,   152,   152,   152,   152,
       4,     4,     4,     4,     4,   122,     4,     4,     4,     4,
       3,   152,     6,     3,     6,     6,     3,     3,   152,     8,
       4,   128,   152,   152,     4,     4,   152,   152,   152,     4,
       4,     4,     4,     4,   152,     4,   152,     4,     4,     7,
       4,     6,     5,     5,     4,   457,   152,     8,   152,     8,
       8,     4,     4,     7,     4,   153,     4,   153,   155,     4,
     152,   155,     4,   515,   153,   153,   153,   528,   153,   153,
     520,   340,   153,   153,   522,   153,   153,   336,   725,   740,
     743,   749,   747,   393,   756,   153,   153,   730,   728,   739,
     867,   355,    73,   731,   733,   736,   390,   734,   776,   774,
     152,   725,   152,   385,   777,   779,   152,   540,   358,   725,
     734,   734,   728,   737,   153,   731,   728,   763,   734,   155,
     780,   737,   153,   734,   728,   737,   152,   782,   728,   728,
     728,   747,   767,   737,   152,   375,   535,   737,   737,   737,
     365,   711,   152,   152,   152,   152,   152,   763,   409,   887,
     903,   915,   932,   152,    -1,    -1,    -1,    -1,   774,    -1,
      -1,   777,    -1,    -1,   780,    -1,    -1,    -1,    -1,    -1,
     774,    -1,    -1,   777,    -1,    -1,   780,    -1,    -1,   774,
      -1,    -1,   777,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   867,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   867,    -1,    -1,    -1,   867,    -1,    -1,    -1,    -1,
      -1,    -1,   886,   867,    -1,    -1,    -1,   867,   867,   867,
     886,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   903,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   903
  };

  const unsigned short int
  Dhcp6Parser::yystos_[] =
  {
       0,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   157,   158,   159,   160,   161,   162,
     163,   164,   165,   166,   167,   168,   169,   170,     0,     5,
       7,     9,   152,   153,   154,   155,   171,   172,   173,   178,
       7,   187,     7,   193,     7,   204,     7,   281,     7,   356,
       7,   370,     7,   387,     7,   308,     7,   314,     7,   338,
       7,   257,     7,   443,   179,   174,   188,   194,   205,   282,
     357,   371,   388,   309,   315,   339,   258,   444,   171,   180,
     181,   152,   176,   177,    10,   127,   136,   137,   138,   152,
     186,   189,   190,   191,   470,   472,   474,   476,    11,    14,
      15,    32,    33,    34,    35,    36,    37,    38,    39,    67,
      68,    69,    70,    71,    83,    86,    93,   101,   102,   105,
     186,   195,   196,   197,   198,   199,   200,   201,   202,   211,
     213,   237,   243,   249,   251,   265,   275,   298,   306,   332,
     410,   420,   432,   433,   441,    12,    13,   206,   207,   208,
     210,     7,    49,    51,    57,    58,    59,    60,    61,    62,
      63,    73,    74,    81,   186,   197,   198,   199,   200,   283,
     284,   285,   287,   289,   291,   293,   296,   297,   332,   350,
     362,   364,   381,   406,    50,   186,   332,   358,   359,   360,
     362,    52,    53,    54,    55,    56,   186,   332,   362,   372,
     373,   374,   376,   377,   379,   380,    71,    75,    76,    77,
      78,    79,    80,   186,   332,   389,   390,   391,   392,   394,
     396,   398,   400,   402,   404,   306,    16,    40,    42,    43,
      46,    47,    48,   186,   227,   316,   317,   318,   319,   320,
     321,   322,   324,   326,   328,   329,   331,    41,    44,    45,
     186,   227,   320,   326,   340,   341,   342,   343,   344,   346,
     347,   348,   349,    84,    85,   186,   259,   260,   261,   263,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   186,   445,   446,   447,   448,   450,
     452,   453,   455,   456,   457,   460,   462,   463,   464,   465,
     468,     6,     3,     4,     8,     3,   192,   477,   471,   473,
     475,     4,     3,     8,   203,   212,   214,     4,     4,     4,
       4,     4,   276,   307,   333,   299,   238,   250,   244,   411,
     252,   266,   421,     4,   434,   442,     3,     8,   209,     4,
       3,     8,   413,   351,   365,   363,   286,   288,   290,     4,
       4,   294,   292,   382,   407,     3,     8,   361,     3,     8,
     375,     4,   378,     4,     4,     3,     8,   405,   393,   395,
     397,   399,   401,   403,     8,     3,     8,   323,   228,     4,
     327,   325,   330,     4,     8,     3,   345,     4,     4,     8,
       3,   262,   264,     3,     8,     4,   449,   451,     4,   454,
       4,     4,   458,   461,     4,     4,     4,   466,   469,     3,
       8,   171,   171,   152,     4,     4,     4,     4,     4,   190,
       4,     4,     4,   153,   153,   153,   153,   153,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     4,   153,
       4,     4,   196,     4,   155,   207,    72,   186,   227,   332,
     414,   415,   416,   417,   418,     4,     4,     4,     4,     4,
       4,   153,   155,     4,     4,     4,     4,   284,     4,   359,
       4,   153,     4,   153,   153,   373,     4,     4,     4,     4,
       4,     4,     4,   391,     4,     4,   153,     4,     4,     4,
     155,   318,     4,   155,   155,   342,     4,     4,   260,   155,
       4,     4,   153,     4,   153,   153,     4,     4,   155,   155,
     155,     4,     4,   446,     4,     7,     7,   171,   171,   171,
       7,     7,     7,     5,     5,     5,     5,     5,     5,     5,
       5,     5,     7,     7,     7,     7,     5,   182,   419,     8,
       3,     5,     5,   173,   175,   152,   152,   152,    64,    65,
      66,   295,   152,     5,     7,   152,   152,   152,   182,   182,
     182,   152,   152,   152,   152,   152,   152,   152,   152,   152,
     152,   152,   171,   152,   152,   152,   120,   121,   459,   122,
     123,   124,   125,   126,   155,   467,   152,   171,   195,   128,
     478,   479,   480,   206,    16,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,   186,   215,   216,   217,   220,
     222,   224,   226,   227,   229,   230,   231,   232,   233,   235,
     215,     7,   277,   278,   279,     7,   310,   311,   312,     7,
     334,   335,   336,     7,   300,   301,   302,    77,   152,   239,
     240,   241,   242,   180,    78,    80,   241,   245,   246,   247,
     248,   291,   412,     7,   253,   254,   255,    87,    88,    89,
      90,    91,    92,   267,   268,   269,   270,   271,   272,   273,
     274,    16,    97,    98,    99,   100,   186,   229,   422,   423,
     424,   427,   428,   430,   431,   103,   104,   435,   436,   437,
     439,   445,   183,     4,   416,     7,   352,   353,   354,     7,
     366,   367,   368,     7,   383,   384,   385,    82,   408,     8,
     481,     3,     8,     8,   218,   221,   223,   225,     4,     4,
       4,     4,     4,   234,   236,     3,     8,     8,   280,     6,
       3,   313,     6,     3,   337,     6,     3,   303,     6,     3,
       3,     6,     6,     3,     6,     3,     6,   256,     6,     3,
       4,     4,     4,     4,     4,     4,     3,     8,   425,   429,
       4,     4,     4,     3,     8,   438,   440,     3,     8,     8,
     152,   184,   185,   152,   355,     6,     3,   369,     6,     3,
     386,     6,     3,   409,     8,     4,   479,     4,     4,     4,
       4,   153,   155,   153,   155,   153,     4,     4,   216,   283,
     279,   316,   312,   340,   336,   186,   197,   198,   199,   200,
     227,   275,   287,   289,   291,   293,   297,   304,   305,   332,
     406,   302,   240,   246,   291,   259,   255,   153,   153,   153,
     153,   153,   153,   268,     4,     4,   153,   153,   153,   423,
       4,     4,   436,     6,     3,   358,   354,   372,   368,   389,
     385,     4,     5,    17,    18,    19,    20,    21,   219,   152,
     152,   152,   152,   152,     8,     8,     8,     3,     8,     8,
      94,    95,    96,   426,   152,   152,   152,   152,     8,     8,
       8,   152,     7,   482,   483,   305,   484,     3,     6,   129,
     131,   132,   186,   227,   485,   486,   487,   488,   490,   483,
     491,     4,   489,     3,     8,     4,   153,     4,   486,     5,
     152,     7,   492,   493,   494,     3,     6,   130,   133,   134,
     135,   495,   496,   497,   499,   500,   501,   493,   498,     4,
       4,     4,     3,     8,     4,   155,   153,   153,   496,   152
  };

  const unsigned short int
  Dhcp6Parser::yyr1_[] =
  {
       0,   156,   158,   157,   159,   157,   160,   157,   161,   157,
     162,   157,   163,   157,   164,   157,   165,   157,   166,   157,
     167,   157,   168,   157,   169,   157,   170,   157,   171,   171,
     171,   171,   171,   171,   171,   172,   174,   173,   175,   176,
     176,   177,   177,   179,   178,   180,   180,   181,   181,   183,
     182,   184,   184,   185,   185,   186,   188,   187,   189,   189,
     190,   190,   190,   190,   190,   190,   192,   191,   194,   193,
     195,   195,   196,   196,   196,   196,   196,   196,   196,   196,
     196,   196,   196,   196,   196,   196,   196,   196,   196,   196,
     196,   196,   196,   196,   196,   197,   198,   199,   200,   201,
     203,   202,   205,   204,   206,   206,   207,   207,   209,   208,
     210,   212,   211,   214,   213,   215,   215,   216,   216,   216,
     216,   216,   216,   216,   216,   216,   216,   216,   216,   216,
     218,   217,   219,   219,   219,   219,   219,   221,   220,   223,
     222,   225,   224,   226,   228,   227,   229,   230,   231,   232,
     234,   233,   236,   235,   238,   237,   239,   239,   240,   240,
     241,   242,   244,   243,   245,   245,   246,   246,   246,   247,
     248,   250,   249,   252,   251,   253,   253,   254,   254,   256,
     255,   258,   257,   259,   259,   259,   260,   260,   262,   261,
     264,   263,   266,   265,   267,   267,   268,   268,   268,   268,
     268,   268,   269,   270,   271,   272,   273,   274,   276,   275,
     277,   277,   278,   278,   280,   279,   282,   281,   283,   283,
     284,   284,   284,   284,   284,   284,   284,   284,   284,   284,
     284,   284,   284,   284,   284,   284,   284,   284,   286,   285,
     288,   287,   290,   289,   292,   291,   294,   293,   295,   295,
     295,   296,   297,   299,   298,   300,   300,   301,   301,   303,
     302,   304,   304,   305,   305,   305,   305,   305,   305,   305,
     305,   305,   305,   305,   305,   305,   305,   307,   306,   309,
     308,   310,   310,   311,   311,   313,   312,   315,   314,   316,
     316,   317,   317,   318,   318,   318,   318,   318,   318,   318,
     318,   319,   320,   321,   323,   322,   325,   324,   327,   326,
     328,   330,   329,   331,   333,   332,   334,   334,   335,   335,
     337,   336,   339,   338,   340,   340,   341,   341,   342,   342,
     342,   342,   342,   342,   342,   343,   345,   344,   346,   347,
     348,   349,   351,   350,   352,   352,   353,   353,   355,   354,
     357,   356,   358,   358,   359,   359,   359,   359,   361,   360,
     363,   362,   365,   364,   366,   366,   367,   367,   369,   368,
     371,   370,   372,   372,   373,   373,   373,   373,   373,   373,
     373,   373,   375,   374,   376,   378,   377,   379,   380,   382,
     381,   383,   383,   384,   384,   386,   385,   388,   387,   389,
     389,   390,   390,   391,   391,   391,   391,   391,   391,   391,
     391,   391,   393,   392,   395,   394,   397,   396,   399,   398,
     401,   400,   403,   402,   405,   404,   407,   406,   409,   408,
     411,   410,   412,   412,   413,   291,   414,   414,   415,   415,
     416,   416,   416,   416,   417,   419,   418,   421,   420,   422,
     422,   423,   423,   423,   423,   423,   423,   423,   425,   424,
     426,   426,   426,   427,   429,   428,   430,   431,   432,   434,
     433,   435,   435,   436,   436,   438,   437,   440,   439,   442,
     441,   444,   443,   445,   445,   446,   446,   446,   446,   446,
     446,   446,   446,   446,   446,   446,   446,   446,   446,   446,
     447,   449,   448,   451,   450,   452,   454,   453,   455,   456,
     458,   457,   459,   459,   461,   460,   462,   463,   464,   466,
     465,   467,   467,   467,   467,   467,   469,   468,   471,   470,
     473,   472,   475,   474,   477,   476,   478,   478,   479,   481,
     480,   482,   482,   484,   483,   485,   485,   486,   486,   486,
     486,   486,   487,   489,   488,   491,   490,   492,   492,   494,
     493,   495,   495,   496,   496,   496,   496,   498,   497,   499,
     500,   501
  };

  const unsigned char
  Dhcp6Parser::yyr2_[] =
  {
       0,     2,     0,     3,     0,     3,     0,     3,     0,     3,
       0,     3,     0,     3,     0,     3,     0,     3,     0,     3,
       0,     3,     0,     3,     0,     3,     0,     3,     1,     1,
       1,     1,     1,     1,     1,     1,     0,     4,     1,     0,
       1,     3,     5,     0,     4,     0,     1,     1,     3,     0,
       4,     0,     1,     1,     3,     2,     0,     4,     1,     3,
       1,     1,     1,     1,     1,     1,     0,     6,     0,     4,
       1,     3,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     3,     3,     3,     3,     3,
       0,     6,     0,     4,     1,     3,     1,     1,     0,     4,
       3,     0,     6,     0,     6,     1,     3,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       0,     4,     1,     1,     1,     1,     1,     0,     4,     0,
       4,     0,     4,     3,     0,     4,     3,     3,     3,     3,
       0,     4,     0,     4,     0,     6,     1,     3,     1,     1,
       1,     1,     0,     6,     1,     3,     1,     1,     1,     1,
       1,     0,     6,     0,     6,     0,     1,     1,     3,     0,
       4,     0,     4,     1,     3,     1,     1,     1,     0,     4,
       0,     4,     0,     6,     1,     3,     1,     1,     1,     1,
       1,     1,     3,     3,     3,     3,     3,     3,     0,     6,
       0,     1,     1,     3,     0,     4,     0,     4,     1,     3,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     0,     4,
       0,     4,     0,     4,     0,     4,     0,     4,     1,     1,
       1,     3,     3,     0,     6,     0,     1,     1,     3,     0,
       4,     1,     3,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     0,     6,     0,
       4,     0,     1,     1,     3,     0,     4,     0,     4,     0,
       1,     1,     3,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     3,     1,     0,     4,     0,     4,     0,     4,
       1,     0,     4,     3,     0,     6,     0,     1,     1,     3,
       0,     4,     0,     4,     0,     1,     1,     3,     1,     1,
       1,     1,     1,     1,     1,     1,     0,     4,     1,     1,
       3,     3,     0,     6,     0,     1,     1,     3,     0,     4,
       0,     4,     1,     3,     1,     1,     1,     1,     0,     4,
       0,     4,     0,     6,     0,     1,     1,     3,     0,     4,
       0,     4,     1,     3,     1,     1,     1,     1,     1,     1,
       1,     1,     0,     4,     3,     0,     4,     3,     3,     0,
       6,     0,     1,     1,     3,     0,     4,     0,     4,     0,
       1,     1,     3,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     0,     4,     0,     4,     0,     4,     0,     4,
       0,     4,     0,     4,     0,     4,     0,     6,     0,     4,
       0,     6,     1,     3,     0,     4,     0,     1,     1,     3,
       1,     1,     1,     1,     1,     0,     4,     0,     6,     1,
       3,     1,     1,     1,     1,     1,     1,     1,     0,     4,
       1,     1,     1,     3,     0,     4,     3,     3,     3,     0,
       6,     1,     3,     1,     1,     0,     4,     0,     4,     0,
       6,     0,     4,     1,     3,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       3,     0,     4,     0,     4,     3,     0,     4,     3,     3,
       0,     4,     1,     1,     0,     4,     3,     3,     3,     0,
       4,     1,     1,     1,     1,     1,     0,     4,     0,     4,
       0,     4,     0,     4,     0,     6,     1,     3,     1,     0,
       6,     1,     3,     0,     4,     1,     3,     1,     1,     1,
       1,     1,     3,     0,     4,     0,     6,     1,     3,     0,
       4,     1,     3,     1,     1,     1,     1,     0,     4,     3,
       3,     3
  };



  // YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
  // First, the terminals, then, starting at \a yyntokens_, nonterminals.
  const char*
  const Dhcp6Parser::yytname_[] =
  {
  "\"end of file\"", "error", "$undefined", "\",\"", "\":\"", "\"[\"",
  "\"]\"", "\"{\"", "\"}\"", "\"null\"", "\"Dhcp6\"",
  "\"interfaces-config\"", "\"interfaces\"", "\"re-detect\"",
  "\"lease-database\"", "\"hosts-database\"", "\"type\"", "\"memfile\"",
  "\"mysql\"", "\"postgresql\"", "\"cql\"", "\"radius\"", "\"user\"",
  "\"password\"", "\"host\"", "\"port\"", "\"persist\"",
  "\"lfc-interval\"", "\"readonly\"", "\"connect-timeout\"",
  "\"contact-points\"", "\"keyspace\"", "\"preferred-lifetime\"",
  "\"valid-lifetime\"", "\"renew-timer\"", "\"rebind-timer\"",
  "\"decline-probation-period\"", "\"subnet6\"", "\"option-def\"",
  "\"option-data\"", "\"name\"", "\"data\"", "\"code\"", "\"space\"",
  "\"csv-format\"", "\"always-send\"", "\"record-types\"",
  "\"encapsulate\"", "\"array\"", "\"pools\"", "\"pool\"", "\"pd-pools\"",
  "\"prefix\"", "\"prefix-len\"", "\"excluded-prefix\"",
  "\"excluded-prefix-len\"", "\"delegated-len\"", "\"user-context\"",
  "\"subnet\"", "\"interface\"", "\"interface-id\"", "\"id\"",
  "\"rapid-commit\"", "\"reservation-mode\"", "\"disabled\"",
  "\"out-of-pool\"", "\"all\"", "\"shared-networks\"", "\"mac-sources\"",
  "\"relay-supplied-options\"", "\"host-reservation-identifiers\"",
  "\"client-classes\"", "\"test\"", "\"client-class\"", "\"reservations\"",
  "\"ip-addresses\"", "\"prefixes\"", "\"duid\"", "\"hw-address\"",
  "\"hostname\"", "\"flex-id\"", "\"relay\"", "\"ip-address\"",
  "\"hooks-libraries\"", "\"library\"", "\"parameters\"",
  "\"expired-leases-processing\"", "\"reclaim-timer-wait-time\"",
  "\"flush-reclaimed-timer-wait-time\"", "\"hold-reclaimed-time\"",
  "\"max-reclaim-leases\"", "\"max-reclaim-time\"",
  "\"unwarned-reclaim-cycles\"", "\"server-id\"", "\"LLT\"", "\"EN\"",
  "\"LL\"", "\"identifier\"", "\"htype\"", "\"time\"", "\"enterprise-id\"",
  "\"dhcp4o6-port\"", "\"control-socket\"", "\"socket-type\"",
  "\"socket-name\"", "\"dhcp-ddns\"", "\"enable-updates\"",
  "\"qualifying-suffix\"", "\"server-ip\"", "\"server-port\"",
  "\"sender-ip\"", "\"sender-port\"", "\"max-queue-size\"",
  "\"ncr-protocol\"", "\"ncr-format\"", "\"always-include-fqdn\"",
  "\"override-no-update\"", "\"override-client-update\"",
  "\"replace-client-name\"", "\"generated-prefix\"", "\"UDP\"", "\"TCP\"",
  "\"JSON\"", "\"when-present\"", "\"never\"", "\"always\"",
  "\"when-not-present\"", "\"Logging\"", "\"loggers\"",
  "\"output_options\"", "\"output\"", "\"debuglevel\"", "\"severity\"",
  "\"flush\"", "\"maxsize\"", "\"maxver\"", "\"Dhcp4\"", "\"DhcpDdns\"",
  "\"Control-agent\"", "TOPLEVEL_JSON", "TOPLEVEL_DHCP6", "SUB_DHCP6",
  "SUB_INTERFACES6", "SUB_SUBNET6", "SUB_POOL6", "SUB_PD_POOL",
  "SUB_RESERVATION", "SUB_OPTION_DEFS", "SUB_OPTION_DEF",
  "SUB_OPTION_DATA", "SUB_HOOKS_LIBRARY", "SUB_DHCP_DDNS",
  "\"constant string\"", "\"integer\"", "\"floating point\"",
  "\"boolean\"", "$accept", "start", "$@1", "$@2", "$@3", "$@4", "$@5",
  "$@6", "$@7", "$@8", "$@9", "$@10", "$@11", "$@12", "$@13", "value",
  "sub_json", "map2", "$@14", "map_value", "map_content", "not_empty_map",
  "list_generic", "$@15", "list_content", "not_empty_list", "list_strings",
  "$@16", "list_strings_content", "not_empty_list_strings",
  "unknown_map_entry", "syntax_map", "$@17", "global_objects",
  "global_object", "dhcp6_object", "$@18", "sub_dhcp6", "$@19",
  "global_params", "global_param", "preferred_lifetime", "valid_lifetime",
  "renew_timer", "rebind_timer", "decline_probation_period",
  "interfaces_config", "$@20", "sub_interfaces6", "$@21",
  "interfaces_config_params", "interfaces_config_param", "interfaces_list",
  "$@22", "re_detect", "lease_database", "$@23", "hosts_database", "$@24",
  "database_map_params", "database_map_param", "database_type", "$@25",
  "db_type", "user", "$@26", "password", "$@27", "host", "$@28", "port",
  "name", "$@29", "persist", "lfc_interval", "readonly", "connect_timeout",
  "contact_points", "$@30", "keyspace", "$@31", "mac_sources", "$@32",
  "mac_sources_list", "mac_sources_value", "duid_id", "string_id",
  "host_reservation_identifiers", "$@33",
  "host_reservation_identifiers_list", "host_reservation_identifier",
  "hw_address_id", "flex_id", "relay_supplied_options", "$@34",
  "hooks_libraries", "$@35", "hooks_libraries_list",
  "not_empty_hooks_libraries_list", "hooks_library", "$@36",
  "sub_hooks_library", "$@37", "hooks_params", "hooks_param", "library",
  "$@38", "parameters", "$@39", "expired_leases_processing", "$@40",
  "expired_leases_params", "expired_leases_param",
  "reclaim_timer_wait_time", "flush_reclaimed_timer_wait_time",
  "hold_reclaimed_time", "max_reclaim_leases", "max_reclaim_time",
  "unwarned_reclaim_cycles", "subnet6_list", "$@41",
  "subnet6_list_content", "not_empty_subnet6_list", "subnet6", "$@42",
  "sub_subnet6", "$@43", "subnet6_params", "subnet6_param", "subnet",
  "$@44", "interface", "$@45", "interface_id", "$@46", "client_class",
  "$@47", "reservation_mode", "$@48", "hr_mode", "id", "rapid_commit",
  "shared_networks", "$@49", "shared_networks_content",
  "shared_networks_list", "shared_network", "$@50",
  "shared_network_params", "shared_network_param", "option_def_list",
  "$@51", "sub_option_def_list", "$@52", "option_def_list_content",
  "not_empty_option_def_list", "option_def_entry", "$@53",
  "sub_option_def", "$@54", "option_def_params",
  "not_empty_option_def_params", "option_def_param", "option_def_name",
  "code", "option_def_code", "option_def_type", "$@55",
  "option_def_record_types", "$@56", "space", "$@57", "option_def_space",
  "option_def_encapsulate", "$@58", "option_def_array", "option_data_list",
  "$@59", "option_data_list_content", "not_empty_option_data_list",
  "option_data_entry", "$@60", "sub_option_data", "$@61",
  "option_data_params", "not_empty_option_data_params",
  "option_data_param", "option_data_name", "option_data_data", "$@62",
  "option_data_code", "option_data_space", "option_data_csv_format",
  "option_data_always_send", "pools_list", "$@63", "pools_list_content",
  "not_empty_pools_list", "pool_list_entry", "$@64", "sub_pool6", "$@65",
  "pool_params", "pool_param", "pool_entry", "$@66", "user_context",
  "$@67", "pd_pools_list", "$@68", "pd_pools_list_content",
  "not_empty_pd_pools_list", "pd_pool_entry", "$@69", "sub_pd_pool",
  "$@70", "pd_pool_params", "pd_pool_param", "pd_prefix", "$@71",
  "pd_prefix_len", "excluded_prefix", "$@72", "excluded_prefix_len",
  "pd_delegated_len", "reservations", "$@73", "reservations_list",
  "not_empty_reservations_list", "reservation", "$@74", "sub_reservation",
  "$@75", "reservation_params", "not_empty_reservation_params",
  "reservation_param", "ip_addresses", "$@76", "prefixes", "$@77", "duid",
  "$@78", "hw_address", "$@79", "hostname", "$@80", "flex_id_value",
  "$@81", "reservation_client_classes", "$@82", "relay", "$@83",
  "relay_map", "$@84", "client_classes", "$@85", "client_classes_list",
  "$@86", "client_class_params", "not_empty_client_class_params",
  "client_class_param", "client_class_name", "client_class_test", "$@87",
  "server_id", "$@88", "server_id_params", "server_id_param",
  "server_id_type", "$@89", "duid_type", "htype", "identifier", "$@90",
  "time", "enterprise_id", "dhcp4o6_port", "control_socket", "$@91",
  "control_socket_params", "control_socket_param", "socket_type", "$@92",
  "socket_name", "$@93", "dhcp_ddns", "$@94", "sub_dhcp_ddns", "$@95",
  "dhcp_ddns_params", "dhcp_ddns_param", "enable_updates",
  "qualifying_suffix", "$@96", "server_ip", "$@97", "server_port",
  "sender_ip", "$@98", "sender_port", "max_queue_size", "ncr_protocol",
  "$@99", "ncr_protocol_value", "ncr_format", "$@100",
  "always_include_fqdn", "override_no_update", "override_client_update",
  "replace_client_name", "$@101", "replace_client_name_value",
  "generated_prefix", "$@102", "dhcp4_json_object", "$@103",
  "dhcpddns_json_object", "$@104", "control_agent_json_object", "$@105",
  "logging_object", "$@106", "logging_params", "logging_param", "loggers",
  "$@107", "loggers_entries", "logger_entry", "$@108", "logger_params",
  "logger_param", "debuglevel", "severity", "$@109", "output_options_list",
  "$@110", "output_options_list_content", "output_entry", "$@111",
  "output_params_list", "output_params", "output", "$@112", "flush",
  "maxsize", "maxver", YY_NULLPTR
  };

#if PARSER6_DEBUG
  const unsigned short int
  Dhcp6Parser::yyrline_[] =
  {
       0,   237,   237,   237,   238,   238,   239,   239,   240,   240,
     241,   241,   242,   242,   243,   243,   244,   244,   245,   245,
     246,   246,   247,   247,   248,   248,   249,   249,   257,   258,
     259,   260,   261,   262,   263,   266,   271,   271,   282,   285,
     286,   289,   293,   300,   300,   307,   308,   311,   315,   322,
     322,   329,   330,   333,   337,   348,   358,   358,   373,   374,
     378,   379,   380,   381,   382,   383,   386,   386,   401,   401,
     410,   411,   416,   417,   418,   419,   420,   421,   422,   423,
     424,   425,   426,   427,   428,   429,   430,   431,   432,   433,
     434,   435,   436,   437,   438,   441,   446,   451,   456,   461,
     466,   466,   477,   477,   486,   487,   490,   491,   494,   494,
     504,   510,   510,   522,   522,   534,   535,   538,   539,   540,
     541,   542,   543,   544,   545,   546,   547,   548,   549,   550,
     553,   553,   560,   561,   562,   563,   564,   567,   567,   575,
     575,   583,   583,   591,   596,   596,   604,   609,   614,   619,
     624,   624,   632,   632,   641,   641,   651,   652,   655,   656,
     659,   664,   669,   669,   679,   680,   683,   684,   685,   688,
     693,   700,   700,   710,   710,   720,   721,   724,   725,   728,
     728,   738,   738,   748,   749,   750,   753,   754,   757,   757,
     765,   765,   773,   773,   784,   785,   788,   789,   790,   791,
     792,   793,   796,   801,   806,   811,   816,   821,   829,   829,
     842,   843,   846,   847,   854,   854,   880,   880,   891,   892,
     896,   897,   898,   899,   900,   901,   902,   903,   904,   905,
     906,   907,   908,   909,   910,   911,   912,   913,   916,   916,
     924,   924,   932,   932,   940,   940,   948,   948,   955,   956,
     957,   960,   965,   973,   973,   984,   985,   989,   990,   993,
     993,  1001,  1002,  1005,  1006,  1007,  1008,  1009,  1010,  1011,
    1012,  1013,  1014,  1015,  1016,  1017,  1018,  1025,  1025,  1038,
    1038,  1047,  1048,  1051,  1052,  1057,  1057,  1072,  1072,  1086,
    1087,  1090,  1091,  1094,  1095,  1096,  1097,  1098,  1099,  1100,
    1101,  1104,  1106,  1111,  1113,  1113,  1121,  1121,  1129,  1129,
    1137,  1139,  1139,  1147,  1156,  1156,  1168,  1169,  1174,  1175,
    1180,  1180,  1192,  1192,  1204,  1205,  1210,  1211,  1216,  1217,
    1218,  1219,  1220,  1221,  1222,  1225,  1227,  1227,  1235,  1237,
    1239,  1244,  1252,  1252,  1264,  1265,  1268,  1269,  1272,  1272,
    1282,  1282,  1291,  1292,  1295,  1296,  1297,  1298,  1301,  1301,
    1309,  1309,  1319,  1319,  1331,  1332,  1335,  1336,  1339,  1339,
    1351,  1351,  1363,  1364,  1367,  1368,  1369,  1370,  1371,  1372,
    1373,  1374,  1377,  1377,  1385,  1390,  1390,  1398,  1403,  1411,
    1411,  1421,  1422,  1425,  1426,  1429,  1429,  1438,  1438,  1447,
    1448,  1451,  1452,  1456,  1457,  1458,  1459,  1460,  1461,  1462,
    1463,  1464,  1467,  1467,  1477,  1477,  1487,  1487,  1495,  1495,
    1503,  1503,  1511,  1511,  1519,  1519,  1532,  1532,  1542,  1542,
    1553,  1553,  1563,  1564,  1567,  1567,  1577,  1578,  1581,  1582,
    1585,  1586,  1587,  1588,  1591,  1593,  1593,  1604,  1604,  1616,
    1617,  1620,  1621,  1622,  1623,  1624,  1625,  1626,  1629,  1629,
    1636,  1637,  1638,  1641,  1646,  1646,  1654,  1659,  1666,  1673,
    1673,  1683,  1684,  1687,  1688,  1691,  1691,  1699,  1699,  1709,
    1709,  1721,  1721,  1731,  1732,  1735,  1736,  1737,  1738,  1739,
    1740,  1741,  1742,  1743,  1744,  1745,  1746,  1747,  1748,  1749,
    1752,  1757,  1757,  1765,  1765,  1773,  1778,  1778,  1786,  1791,
    1796,  1796,  1804,  1805,  1808,  1808,  1816,  1821,  1826,  1831,
    1831,  1839,  1842,  1845,  1848,  1851,  1857,  1857,  1867,  1867,
    1874,  1874,  1881,  1881,  1894,  1894,  1907,  1908,  1912,  1916,
    1916,  1928,  1929,  1933,  1933,  1941,  1942,  1945,  1946,  1947,
    1948,  1949,  1952,  1957,  1957,  1965,  1965,  1975,  1976,  1979,
    1979,  1987,  1988,  1991,  1992,  1993,  1994,  1997,  1997,  2005,
    2010,  2015
  };

  // Print the state stack on the debug stream.
  void
  Dhcp6Parser::yystack_print_ ()
  {
    *yycdebug_ << "Stack now";
    for (stack_type::const_iterator
           i = yystack_.begin (),
           i_end = yystack_.end ();
         i != i_end; ++i)
      *yycdebug_ << ' ' << i->state;
    *yycdebug_ << std::endl;
  }

  // Report on the debug stream that the rule \a yyrule is going to be reduced.
  void
  Dhcp6Parser::yy_reduce_print_ (int yyrule)
  {
    unsigned int yylno = yyrline_[yyrule];
    int yynrhs = yyr2_[yyrule];
    // Print the symbols being reduced, and their result.
    *yycdebug_ << "Reducing stack by rule " << yyrule - 1
               << " (line " << yylno << "):" << std::endl;
    // The symbols being reduced.
    for (int yyi = 0; yyi < yynrhs; yyi++)
      YY_SYMBOL_PRINT ("   $" << yyi + 1 << " =",
                       yystack_[(yynrhs) - (yyi + 1)]);
  }
#endif // PARSER6_DEBUG


#line 14 "dhcp6_parser.yy" // lalr1.cc:1167
} } // isc::dhcp
#line 4474 "dhcp6_parser.cc" // lalr1.cc:1167
#line 2020 "dhcp6_parser.yy" // lalr1.cc:1168


void
isc::dhcp::Dhcp6Parser::error(const location_type& loc,
                              const std::string& what)
{
    ctx.error(loc, what);
}
