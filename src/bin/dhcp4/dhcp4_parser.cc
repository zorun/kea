// A Bison parser, made by GNU Bison 3.0.4.

// Skeleton implementation for Bison LALR(1) parsers in C++

// Copyright (C) 2002-2015 Free Software Foundation, Inc.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// As a special exception, you may create a larger work that contains
// part or all of the Bison parser skeleton and distribute that work
// under terms of your choice, so long as that work isn't itself a
// parser generator using the skeleton or a modified version thereof
// as a parser skeleton.  Alternatively, if you modify or redistribute
// the parser skeleton itself, you may (at your option) remove this
// special exception, which will cause the skeleton and the resulting
// Bison output files to be licensed under the GNU General Public
// License without this special exception.

// This special exception was added by the Free Software Foundation in
// version 2.2 of Bison.

// Take the name prefix into account.
#define yylex   parser4_lex

// First part of user declarations.

#line 39 "dhcp4_parser.cc" // lalr1.cc:404

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

#include "dhcp4_parser.h"

// User implementation prologue.

#line 53 "dhcp4_parser.cc" // lalr1.cc:412
// Unqualified %code blocks.
#line 34 "dhcp4_parser.yy" // lalr1.cc:413

#include <dhcp4/parser_context.h>

#line 59 "dhcp4_parser.cc" // lalr1.cc:413


#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> // FIXME: INFRINGES ON USER NAME SPACE.
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K].location)
/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

# ifndef YYLLOC_DEFAULT
#  define YYLLOC_DEFAULT(Current, Rhs, N)                               \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).begin  = YYRHSLOC (Rhs, 1).begin;                   \
          (Current).end    = YYRHSLOC (Rhs, N).end;                     \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).begin = (Current).end = YYRHSLOC (Rhs, 0).end;      \
        }                                                               \
    while (/*CONSTCOND*/ false)
# endif


// Suppress unused-variable warnings by "using" E.
#define YYUSE(E) ((void) (E))

// Enable debugging if requested.
#if PARSER4_DEBUG

// A pseudo ostream that takes yydebug_ into account.
# define YYCDEBUG if (yydebug_) (*yycdebug_)

# define YY_SYMBOL_PRINT(Title, Symbol)         \
  do {                                          \
    if (yydebug_)                               \
    {                                           \
      *yycdebug_ << Title << ' ';               \
      yy_print_ (*yycdebug_, Symbol);           \
      *yycdebug_ << std::endl;                  \
    }                                           \
  } while (false)

# define YY_REDUCE_PRINT(Rule)          \
  do {                                  \
    if (yydebug_)                       \
      yy_reduce_print_ (Rule);          \
  } while (false)

# define YY_STACK_PRINT()               \
  do {                                  \
    if (yydebug_)                       \
      yystack_print_ ();                \
  } while (false)

#else // !PARSER4_DEBUG

# define YYCDEBUG if (false) std::cerr
# define YY_SYMBOL_PRINT(Title, Symbol)  YYUSE(Symbol)
# define YY_REDUCE_PRINT(Rule)           static_cast<void>(0)
# define YY_STACK_PRINT()                static_cast<void>(0)

#endif // !PARSER4_DEBUG

#define yyerrok         (yyerrstatus_ = 0)
#define yyclearin       (yyla.clear ())

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYRECOVERING()  (!!yyerrstatus_)

#line 14 "dhcp4_parser.yy" // lalr1.cc:479
namespace isc { namespace dhcp {
#line 145 "dhcp4_parser.cc" // lalr1.cc:479

  /* Return YYSTR after stripping away unnecessary quotes and
     backslashes, so that it's suitable for yyerror.  The heuristic is
     that double-quoting is unnecessary unless the string contains an
     apostrophe, a comma, or backslash (other than backslash-backslash).
     YYSTR is taken from yytname.  */
  std::string
  Dhcp4Parser::yytnamerr_ (const char *yystr)
  {
    if (*yystr == '"')
      {
        std::string yyr = "";
        char const *yyp = yystr;

        for (;;)
          switch (*++yyp)
            {
            case '\'':
            case ',':
              goto do_not_strip_quotes;

            case '\\':
              if (*++yyp != '\\')
                goto do_not_strip_quotes;
              // Fall through.
            default:
              yyr += *yyp;
              break;

            case '"':
              return yyr;
            }
      do_not_strip_quotes: ;
      }

    return yystr;
  }


  /// Build a parser object.
  Dhcp4Parser::Dhcp4Parser (isc::dhcp::Parser4Context& ctx_yyarg)
    :
#if PARSER4_DEBUG
      yydebug_ (false),
      yycdebug_ (&std::cerr),
#endif
      ctx (ctx_yyarg)
  {}

  Dhcp4Parser::~Dhcp4Parser ()
  {}


  /*---------------.
  | Symbol types.  |
  `---------------*/



  // by_state.
  inline
  Dhcp4Parser::by_state::by_state ()
    : state (empty_state)
  {}

  inline
  Dhcp4Parser::by_state::by_state (const by_state& other)
    : state (other.state)
  {}

  inline
  void
  Dhcp4Parser::by_state::clear ()
  {
    state = empty_state;
  }

  inline
  void
  Dhcp4Parser::by_state::move (by_state& that)
  {
    state = that.state;
    that.clear ();
  }

  inline
  Dhcp4Parser::by_state::by_state (state_type s)
    : state (s)
  {}

  inline
  Dhcp4Parser::symbol_number_type
  Dhcp4Parser::by_state::type_get () const
  {
    if (state == empty_state)
      return empty_symbol;
    else
      return yystos_[state];
  }

  inline
  Dhcp4Parser::stack_symbol_type::stack_symbol_type ()
  {}


  inline
  Dhcp4Parser::stack_symbol_type::stack_symbol_type (state_type s, symbol_type& that)
    : super_type (s, that.location)
  {
      switch (that.type_get ())
    {
      case 165: // value
      case 169: // map_value
      case 207: // socket_type
      case 210: // outbound_interface_value
      case 220: // db_type
      case 297: // hr_mode
      case 440: // ncr_protocol_value
      case 448: // replace_client_name_value
        value.move< ElementPtr > (that.value);
        break;

      case 150: // "boolean"
        value.move< bool > (that.value);
        break;

      case 149: // "floating point"
        value.move< double > (that.value);
        break;

      case 148: // "integer"
        value.move< int64_t > (that.value);
        break;

      case 147: // "constant string"
        value.move< std::string > (that.value);
        break;

      default:
        break;
    }

    // that is emptied.
    that.type = empty_symbol;
  }

  inline
  Dhcp4Parser::stack_symbol_type&
  Dhcp4Parser::stack_symbol_type::operator= (const stack_symbol_type& that)
  {
    state = that.state;
      switch (that.type_get ())
    {
      case 165: // value
      case 169: // map_value
      case 207: // socket_type
      case 210: // outbound_interface_value
      case 220: // db_type
      case 297: // hr_mode
      case 440: // ncr_protocol_value
      case 448: // replace_client_name_value
        value.copy< ElementPtr > (that.value);
        break;

      case 150: // "boolean"
        value.copy< bool > (that.value);
        break;

      case 149: // "floating point"
        value.copy< double > (that.value);
        break;

      case 148: // "integer"
        value.copy< int64_t > (that.value);
        break;

      case 147: // "constant string"
        value.copy< std::string > (that.value);
        break;

      default:
        break;
    }

    location = that.location;
    return *this;
  }


  template <typename Base>
  inline
  void
  Dhcp4Parser::yy_destroy_ (const char* yymsg, basic_symbol<Base>& yysym) const
  {
    if (yymsg)
      YY_SYMBOL_PRINT (yymsg, yysym);
  }

#if PARSER4_DEBUG
  template <typename Base>
  void
  Dhcp4Parser::yy_print_ (std::ostream& yyo,
                                     const basic_symbol<Base>& yysym) const
  {
    std::ostream& yyoutput = yyo;
    YYUSE (yyoutput);
    symbol_number_type yytype = yysym.type_get ();
    // Avoid a (spurious) G++ 4.8 warning about "array subscript is
    // below array bounds".
    if (yysym.empty ())
      std::abort ();
    yyo << (yytype < yyntokens_ ? "token" : "nterm")
        << ' ' << yytname_[yytype] << " ("
        << yysym.location << ": ";
    switch (yytype)
    {
            case 147: // "constant string"

#line 225 "dhcp4_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< std::string > (); }
#line 366 "dhcp4_parser.cc" // lalr1.cc:636
        break;

      case 148: // "integer"

#line 225 "dhcp4_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< int64_t > (); }
#line 373 "dhcp4_parser.cc" // lalr1.cc:636
        break;

      case 149: // "floating point"

#line 225 "dhcp4_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< double > (); }
#line 380 "dhcp4_parser.cc" // lalr1.cc:636
        break;

      case 150: // "boolean"

#line 225 "dhcp4_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< bool > (); }
#line 387 "dhcp4_parser.cc" // lalr1.cc:636
        break;

      case 165: // value

#line 225 "dhcp4_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< ElementPtr > (); }
#line 394 "dhcp4_parser.cc" // lalr1.cc:636
        break;

      case 169: // map_value

#line 225 "dhcp4_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< ElementPtr > (); }
#line 401 "dhcp4_parser.cc" // lalr1.cc:636
        break;

      case 207: // socket_type

#line 225 "dhcp4_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< ElementPtr > (); }
#line 408 "dhcp4_parser.cc" // lalr1.cc:636
        break;

      case 210: // outbound_interface_value

#line 225 "dhcp4_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< ElementPtr > (); }
#line 415 "dhcp4_parser.cc" // lalr1.cc:636
        break;

      case 220: // db_type

#line 225 "dhcp4_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< ElementPtr > (); }
#line 422 "dhcp4_parser.cc" // lalr1.cc:636
        break;

      case 297: // hr_mode

#line 225 "dhcp4_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< ElementPtr > (); }
#line 429 "dhcp4_parser.cc" // lalr1.cc:636
        break;

      case 440: // ncr_protocol_value

#line 225 "dhcp4_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< ElementPtr > (); }
#line 436 "dhcp4_parser.cc" // lalr1.cc:636
        break;

      case 448: // replace_client_name_value

#line 225 "dhcp4_parser.yy" // lalr1.cc:636
        { yyoutput << yysym.value.template as< ElementPtr > (); }
#line 443 "dhcp4_parser.cc" // lalr1.cc:636
        break;


      default:
        break;
    }
    yyo << ')';
  }
#endif

  inline
  void
  Dhcp4Parser::yypush_ (const char* m, state_type s, symbol_type& sym)
  {
    stack_symbol_type t (s, sym);
    yypush_ (m, t);
  }

  inline
  void
  Dhcp4Parser::yypush_ (const char* m, stack_symbol_type& s)
  {
    if (m)
      YY_SYMBOL_PRINT (m, s);
    yystack_.push (s);
  }

  inline
  void
  Dhcp4Parser::yypop_ (unsigned int n)
  {
    yystack_.pop (n);
  }

#if PARSER4_DEBUG
  std::ostream&
  Dhcp4Parser::debug_stream () const
  {
    return *yycdebug_;
  }

  void
  Dhcp4Parser::set_debug_stream (std::ostream& o)
  {
    yycdebug_ = &o;
  }


  Dhcp4Parser::debug_level_type
  Dhcp4Parser::debug_level () const
  {
    return yydebug_;
  }

  void
  Dhcp4Parser::set_debug_level (debug_level_type l)
  {
    yydebug_ = l;
  }
#endif // PARSER4_DEBUG

  inline Dhcp4Parser::state_type
  Dhcp4Parser::yy_lr_goto_state_ (state_type yystate, int yysym)
  {
    int yyr = yypgoto_[yysym - yyntokens_] + yystate;
    if (0 <= yyr && yyr <= yylast_ && yycheck_[yyr] == yystate)
      return yytable_[yyr];
    else
      return yydefgoto_[yysym - yyntokens_];
  }

  inline bool
  Dhcp4Parser::yy_pact_value_is_default_ (int yyvalue)
  {
    return yyvalue == yypact_ninf_;
  }

  inline bool
  Dhcp4Parser::yy_table_value_is_error_ (int yyvalue)
  {
    return yyvalue == yytable_ninf_;
  }

  int
  Dhcp4Parser::parse ()
  {
    // State.
    int yyn;
    /// Length of the RHS of the rule being reduced.
    int yylen = 0;

    // Error handling.
    int yynerrs_ = 0;
    int yyerrstatus_ = 0;

    /// The lookahead symbol.
    symbol_type yyla;

    /// The locations where the error started and ended.
    stack_symbol_type yyerror_range[3];

    /// The return value of parse ().
    int yyresult;

    // FIXME: This shoud be completely indented.  It is not yet to
    // avoid gratuitous conflicts when merging into the master branch.
    try
      {
    YYCDEBUG << "Starting parse" << std::endl;


    /* Initialize the stack.  The initial state will be set in
       yynewstate, since the latter expects the semantical and the
       location values to have been already stored, initialize these
       stacks with a primary value.  */
    yystack_.clear ();
    yypush_ (YY_NULLPTR, 0, yyla);

    // A new symbol was pushed on the stack.
  yynewstate:
    YYCDEBUG << "Entering state " << yystack_[0].state << std::endl;

    // Accept?
    if (yystack_[0].state == yyfinal_)
      goto yyacceptlab;

    goto yybackup;

    // Backup.
  yybackup:

    // Try to take a decision without lookahead.
    yyn = yypact_[yystack_[0].state];
    if (yy_pact_value_is_default_ (yyn))
      goto yydefault;

    // Read a lookahead token.
    if (yyla.empty ())
      {
        YYCDEBUG << "Reading a token: ";
        try
          {
            symbol_type yylookahead (yylex (ctx));
            yyla.move (yylookahead);
          }
        catch (const syntax_error& yyexc)
          {
            error (yyexc);
            goto yyerrlab1;
          }
      }
    YY_SYMBOL_PRINT ("Next token is", yyla);

    /* If the proper action on seeing token YYLA.TYPE is to reduce or
       to detect an error, take that action.  */
    yyn += yyla.type_get ();
    if (yyn < 0 || yylast_ < yyn || yycheck_[yyn] != yyla.type_get ())
      goto yydefault;

    // Reduce or error.
    yyn = yytable_[yyn];
    if (yyn <= 0)
      {
        if (yy_table_value_is_error_ (yyn))
          goto yyerrlab;
        yyn = -yyn;
        goto yyreduce;
      }

    // Count tokens shifted since error; after three, turn off error status.
    if (yyerrstatus_)
      --yyerrstatus_;

    // Shift the lookahead token.
    yypush_ ("Shifting", yyn, yyla);
    goto yynewstate;

  /*-----------------------------------------------------------.
  | yydefault -- do the default action for the current state.  |
  `-----------------------------------------------------------*/
  yydefault:
    yyn = yydefact_[yystack_[0].state];
    if (yyn == 0)
      goto yyerrlab;
    goto yyreduce;

  /*-----------------------------.
  | yyreduce -- Do a reduction.  |
  `-----------------------------*/
  yyreduce:
    yylen = yyr2_[yyn];
    {
      stack_symbol_type yylhs;
      yylhs.state = yy_lr_goto_state_(yystack_[yylen].state, yyr1_[yyn]);
      /* Variants are always initialized to an empty instance of the
         correct type. The default '$$ = $1' action is NOT applied
         when using variants.  */
        switch (yyr1_[yyn])
    {
      case 165: // value
      case 169: // map_value
      case 207: // socket_type
      case 210: // outbound_interface_value
      case 220: // db_type
      case 297: // hr_mode
      case 440: // ncr_protocol_value
      case 448: // replace_client_name_value
        yylhs.value.build< ElementPtr > ();
        break;

      case 150: // "boolean"
        yylhs.value.build< bool > ();
        break;

      case 149: // "floating point"
        yylhs.value.build< double > ();
        break;

      case 148: // "integer"
        yylhs.value.build< int64_t > ();
        break;

      case 147: // "constant string"
        yylhs.value.build< std::string > ();
        break;

      default:
        break;
    }


      // Compute the default @$.
      {
        slice<stack_symbol_type, stack_type> slice (yystack_, yylen);
        YYLLOC_DEFAULT (yylhs.location, slice, yylen);
      }

      // Perform the reduction.
      YY_REDUCE_PRINT (yyn);
      try
        {
          switch (yyn)
            {
  case 2:
#line 234 "dhcp4_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.NO_KEYWORD; }
#line 690 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 4:
#line 235 "dhcp4_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.CONFIG; }
#line 696 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 6:
#line 236 "dhcp4_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.DHCP4; }
#line 702 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 8:
#line 237 "dhcp4_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.INTERFACES_CONFIG; }
#line 708 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 10:
#line 238 "dhcp4_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.SUBNET4; }
#line 714 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 12:
#line 239 "dhcp4_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.POOLS; }
#line 720 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 14:
#line 240 "dhcp4_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.RESERVATIONS; }
#line 726 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 16:
#line 241 "dhcp4_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.DHCP4; }
#line 732 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 18:
#line 242 "dhcp4_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.OPTION_DEF; }
#line 738 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 20:
#line 243 "dhcp4_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.OPTION_DATA; }
#line 744 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 22:
#line 244 "dhcp4_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.HOOKS_LIBRARIES; }
#line 750 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 24:
#line 245 "dhcp4_parser.yy" // lalr1.cc:859
    { ctx.ctx_ = ctx.DHCP_DDNS; }
#line 756 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 26:
#line 253 "dhcp4_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location))); }
#line 762 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 27:
#line 254 "dhcp4_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new DoubleElement(yystack_[0].value.as< double > (), ctx.loc2pos(yystack_[0].location))); }
#line 768 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 28:
#line 255 "dhcp4_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location))); }
#line 774 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 29:
#line 256 "dhcp4_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location))); }
#line 780 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 30:
#line 257 "dhcp4_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new NullElement(ctx.loc2pos(yystack_[0].location))); }
#line 786 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 31:
#line 258 "dhcp4_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ctx.stack_.back(); ctx.stack_.pop_back(); }
#line 792 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 32:
#line 259 "dhcp4_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ctx.stack_.back(); ctx.stack_.pop_back(); }
#line 798 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 33:
#line 262 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // Push back the JSON value on the stack
    ctx.stack_.push_back(yystack_[0].value.as< ElementPtr > ());
}
#line 807 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 34:
#line 267 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // This code is executed when we're about to start parsing
    // the content of the map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 818 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 35:
#line 272 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // map parsing completed. If we ever want to do any wrap up
    // (maybe some sanity checking), this would be the best place
    // for it.
}
#line 828 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 36:
#line 278 "dhcp4_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ctx.stack_.back(); ctx.stack_.pop_back(); }
#line 834 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 39:
#line 285 "dhcp4_parser.yy" // lalr1.cc:859
    {
                  // map containing a single entry
                  ctx.stack_.back()->set(yystack_[2].value.as< std::string > (), yystack_[0].value.as< ElementPtr > ());
                  }
#line 843 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 40:
#line 289 "dhcp4_parser.yy" // lalr1.cc:859
    {
                  // map consisting of a shorter map followed by
                  // comma and string:value
                  ctx.stack_.back()->set(yystack_[2].value.as< std::string > (), yystack_[0].value.as< ElementPtr > ());
                  }
#line 853 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 41:
#line 296 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(l);
}
#line 862 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 42:
#line 299 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // list parsing complete. Put any sanity checking here
}
#line 870 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 45:
#line 307 "dhcp4_parser.yy" // lalr1.cc:859
    {
                  // List consisting of a single element.
                  ctx.stack_.back()->add(yystack_[0].value.as< ElementPtr > ());
                  }
#line 879 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 46:
#line 311 "dhcp4_parser.yy" // lalr1.cc:859
    {
                  // List ending with , and a value.
                  ctx.stack_.back()->add(yystack_[0].value.as< ElementPtr > ());
                  }
#line 888 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 47:
#line 318 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // List parsing about to start
}
#line 896 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 48:
#line 320 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // list parsing complete. Put any sanity checking here
    //ctx.stack_.pop_back();
}
#line 905 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 51:
#line 329 "dhcp4_parser.yy" // lalr1.cc:859
    {
                          ElementPtr s(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
                          ctx.stack_.back()->add(s);
                          }
#line 914 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 52:
#line 333 "dhcp4_parser.yy" // lalr1.cc:859
    {
                          ElementPtr s(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
                          ctx.stack_.back()->add(s);
                          }
#line 923 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 53:
#line 344 "dhcp4_parser.yy" // lalr1.cc:859
    {
    const std::string& where = ctx.contextName();
    const std::string& keyword = yystack_[1].value.as< std::string > ();
    error(yystack_[1].location,
          "got unexpected keyword \"" + keyword + "\" in " + where + " map.");
}
#line 934 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 54:
#line 354 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // This code is executed when we're about to start parsing
    // the content of the map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 945 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 55:
#line 359 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // map parsing completed. If we ever want to do any wrap up
    // (maybe some sanity checking), this would be the best place
    // for it.

    // Dhcp4 is required
    ctx.require("Dhcp4", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
}
#line 958 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 64:
#line 383 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // This code is executed when we're about to start parsing
    // the content of the map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("Dhcp4", m);
    ctx.stack_.push_back(m);
    ctx.enter(ctx.DHCP4);
}
#line 971 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 65:
#line 390 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // No global parameter is required
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 981 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 66:
#line 398 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // Parse the Dhcp4 map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 991 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 67:
#line 402 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // No global parameter is required
    // parsing completed
}
#line 1000 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 94:
#line 439 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr prf(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("valid-lifetime", prf);
}
#line 1009 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 95:
#line 444 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr prf(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("renew-timer", prf);
}
#line 1018 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 96:
#line 449 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr prf(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("rebind-timer", prf);
}
#line 1027 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 97:
#line 454 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr dpp(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("decline-probation-period", dpp);
}
#line 1036 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 98:
#line 459 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr echo(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("echo-client-id", echo);
}
#line 1045 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 99:
#line 464 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr match(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("match-client-id", match);
}
#line 1054 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 100:
#line 470 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr i(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("interfaces-config", i);
    ctx.stack_.push_back(i);
    ctx.enter(ctx.INTERFACES_CONFIG);
}
#line 1065 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 101:
#line 475 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // No interfaces config param is required
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 1075 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 108:
#line 491 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // Parse the interfaces-config map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 1085 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 109:
#line 495 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // No interfaces config param is required
    // parsing completed
}
#line 1094 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 110:
#line 500 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("interfaces", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1105 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 111:
#line 505 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 1114 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 112:
#line 510 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.DHCP_SOCKET_TYPE);
}
#line 1122 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 113:
#line 512 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.back()->set("dhcp-socket-type", yystack_[0].value.as< ElementPtr > ());
    ctx.leave();
}
#line 1131 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 114:
#line 517 "dhcp4_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("raw", ctx.loc2pos(yystack_[0].location))); }
#line 1137 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 115:
#line 518 "dhcp4_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("udp", ctx.loc2pos(yystack_[0].location))); }
#line 1143 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 116:
#line 521 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.OUTBOUND_INTERFACE);
}
#line 1151 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 117:
#line 523 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.back()->set("outbound-interface", yystack_[0].value.as< ElementPtr > ());
    ctx.leave();
}
#line 1160 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 118:
#line 528 "dhcp4_parser.yy" // lalr1.cc:859
    {
    yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("same-as-inbound", ctx.loc2pos(yystack_[0].location)));
}
#line 1168 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 119:
#line 530 "dhcp4_parser.yy" // lalr1.cc:859
    {
    yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("use-routing", ctx.loc2pos(yystack_[0].location)));
    }
#line 1176 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 120:
#line 534 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr b(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("re-detect", b);
}
#line 1185 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 121:
#line 540 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr i(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("lease-database", i);
    ctx.stack_.push_back(i);
    ctx.enter(ctx.LEASE_DATABASE);
}
#line 1196 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 122:
#line 545 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // The type parameter is required
    ctx.require("type", ctx.loc2pos(yystack_[2].location), ctx.loc2pos(yystack_[0].location));
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 1207 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 123:
#line 552 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr i(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("hosts-database", i);
    ctx.stack_.push_back(i);
    ctx.enter(ctx.HOSTS_DATABASE);
}
#line 1218 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 124:
#line 557 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // The type parameter is required
    ctx.require("type", ctx.loc2pos(yystack_[2].location), ctx.loc2pos(yystack_[0].location));
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 1229 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 140:
#line 583 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.DATABASE_TYPE);
}
#line 1237 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 141:
#line 585 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.back()->set("type", yystack_[0].value.as< ElementPtr > ());
    ctx.leave();
}
#line 1246 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 142:
#line 590 "dhcp4_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("memfile", ctx.loc2pos(yystack_[0].location))); }
#line 1252 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 143:
#line 591 "dhcp4_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("mysql", ctx.loc2pos(yystack_[0].location))); }
#line 1258 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 144:
#line 592 "dhcp4_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("postgresql", ctx.loc2pos(yystack_[0].location))); }
#line 1264 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 145:
#line 593 "dhcp4_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("cql", ctx.loc2pos(yystack_[0].location))); }
#line 1270 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 146:
#line 594 "dhcp4_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("radius", ctx.loc2pos(yystack_[0].location))); }
#line 1276 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 147:
#line 597 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1284 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 148:
#line 599 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr user(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("user", user);
    ctx.leave();
}
#line 1294 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 149:
#line 605 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1302 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 150:
#line 607 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr pwd(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("password", pwd);
    ctx.leave();
}
#line 1312 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 151:
#line 613 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1320 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 152:
#line 615 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr h(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("host", h);
    ctx.leave();
}
#line 1330 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 153:
#line 621 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr p(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("port", p);
}
#line 1339 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 154:
#line 626 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1347 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 155:
#line 628 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr name(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("name", name);
    ctx.leave();
}
#line 1357 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 156:
#line 634 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr n(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("persist", n);
}
#line 1366 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 157:
#line 639 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr n(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("lfc-interval", n);
}
#line 1375 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 158:
#line 644 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr n(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("readonly", n);
}
#line 1384 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 159:
#line 649 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr n(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("connect-timeout", n);
}
#line 1393 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 160:
#line 654 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1401 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 161:
#line 656 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr cp(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("contact-points", cp);
    ctx.leave();
}
#line 1411 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 162:
#line 662 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1419 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 163:
#line 664 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr ks(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("keyspace", ks);
    ctx.leave();
}
#line 1429 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 164:
#line 671 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("host-reservation-identifiers", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.HOST_RESERVATION_IDENTIFIERS);
}
#line 1440 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 165:
#line 676 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 1449 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 173:
#line 692 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr duid(new StringElement("duid", ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(duid);
}
#line 1458 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 174:
#line 697 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr hwaddr(new StringElement("hw-address", ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(hwaddr);
}
#line 1467 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 175:
#line 702 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr circuit(new StringElement("circuit-id", ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(circuit);
}
#line 1476 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 176:
#line 707 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr client(new StringElement("client-id", ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(client);
}
#line 1485 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 177:
#line 712 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr flex_id(new StringElement("flex-id", ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(flex_id);
}
#line 1494 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 178:
#line 717 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("hooks-libraries", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.HOOKS_LIBRARIES);
}
#line 1505 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 179:
#line 722 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 1514 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 184:
#line 735 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(m);
    ctx.stack_.push_back(m);
}
#line 1524 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 185:
#line 739 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // The library hooks parameter is required
    ctx.require("library", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.stack_.pop_back();
}
#line 1534 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 186:
#line 745 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // Parse the hooks-libraries list entry map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 1544 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 187:
#line 749 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // The library hooks parameter is required
    ctx.require("library", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    // parsing completed
}
#line 1554 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 193:
#line 764 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1562 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 194:
#line 766 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr lib(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("library", lib);
    ctx.leave();
}
#line 1572 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 195:
#line 772 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1580 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 196:
#line 774 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.back()->set("parameters", yystack_[0].value.as< ElementPtr > ());
    ctx.leave();
}
#line 1589 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 197:
#line 780 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("expired-leases-processing", m);
    ctx.stack_.push_back(m);
    ctx.enter(ctx.EXPIRED_LEASES_PROCESSING);
}
#line 1600 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 198:
#line 785 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // No expired lease parameter is required
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 1610 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 207:
#line 803 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr value(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("reclaim-timer-wait-time", value);
}
#line 1619 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 208:
#line 808 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr value(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("flush-reclaimed-timer-wait-time", value);
}
#line 1628 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 209:
#line 813 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr value(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("hold-reclaimed-time", value);
}
#line 1637 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 210:
#line 818 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr value(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("max-reclaim-leases", value);
}
#line 1646 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 211:
#line 823 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr value(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("max-reclaim-time", value);
}
#line 1655 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 212:
#line 828 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr value(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("unwarned-reclaim-cycles", value);
}
#line 1664 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 213:
#line 836 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("subnet4", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.SUBNET4);
}
#line 1675 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 214:
#line 841 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 1684 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 219:
#line 861 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(m);
    ctx.stack_.push_back(m);
}
#line 1694 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 220:
#line 865 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // Once we reached this place, the subnet parsing is now complete.
    // If we want to, we can implement default values here.
    // In particular we can do things like this:
    // if (!ctx.stack_.back()->get("interface")) {
    //     ctx.stack_.back()->set("interface", StringElement("loopback"));
    // }
    //
    // We can also stack up one level (Dhcp4) and copy over whatever
    // global parameters we want to:
    // if (!ctx.stack_.back()->get("renew-timer")) {
    //     ElementPtr renew = ctx_stack_[...].get("renew-timer");
    //     if (renew) {
    //         ctx.stack_.back()->set("renew-timer", renew);
    //     }
    // }

    // The subnet subnet4 parameter is required
    ctx.require("subnet", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.stack_.pop_back();
}
#line 1720 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 221:
#line 887 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // Parse the subnet4 list entry map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 1730 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 222:
#line 891 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // The subnet subnet4 parameter is required
    ctx.require("subnet", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    // parsing completed
}
#line 1740 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 248:
#line 928 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1748 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 249:
#line 930 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr subnet(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("subnet", subnet);
    ctx.leave();
}
#line 1758 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 250:
#line 936 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1766 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 251:
#line 938 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr iface(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("4o6-interface", iface);
    ctx.leave();
}
#line 1776 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 252:
#line 944 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1784 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 253:
#line 946 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr iface(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("4o6-interface-id", iface);
    ctx.leave();
}
#line 1794 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 254:
#line 952 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1802 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 255:
#line 954 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr iface(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("4o6-subnet", iface);
    ctx.leave();
}
#line 1812 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 256:
#line 960 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1820 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 257:
#line 962 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr iface(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("interface", iface);
    ctx.leave();
}
#line 1830 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 258:
#line 968 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 1838 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 259:
#line 970 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr iface(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("interface-id", iface);
    ctx.leave();
}
#line 1848 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 260:
#line 976 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.CLIENT_CLASS);
}
#line 1856 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 261:
#line 978 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr cls(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("client-class", cls);
    ctx.leave();
}
#line 1866 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 262:
#line 984 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.RESERVATION_MODE);
}
#line 1874 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 263:
#line 986 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.back()->set("reservation-mode", yystack_[0].value.as< ElementPtr > ());
    ctx.leave();
}
#line 1883 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 264:
#line 991 "dhcp4_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("disabled", ctx.loc2pos(yystack_[0].location))); }
#line 1889 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 265:
#line 992 "dhcp4_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("out-of-pool", ctx.loc2pos(yystack_[0].location))); }
#line 1895 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 266:
#line 993 "dhcp4_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("all", ctx.loc2pos(yystack_[0].location))); }
#line 1901 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 267:
#line 996 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr id(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("id", id);
}
#line 1910 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 268:
#line 1001 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr rc(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("rapid-commit", rc);
}
#line 1919 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 269:
#line 1008 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("shared-networks", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.SHARED_NETWORK);
}
#line 1930 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 270:
#line 1013 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 1939 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 275:
#line 1028 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(m);
    ctx.stack_.push_back(m);
}
#line 1949 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 276:
#line 1032 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
}
#line 1957 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 294:
#line 1061 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("option-def", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.OPTION_DEF);
}
#line 1968 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 295:
#line 1066 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 1977 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 296:
#line 1074 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 1986 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 297:
#line 1077 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // parsing completed
}
#line 1994 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 302:
#line 1093 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(m);
    ctx.stack_.push_back(m);
}
#line 2004 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 303:
#line 1097 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // The name, code and type option def parameters are required.
    ctx.require("name", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.require("code", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.require("type", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.stack_.pop_back();
}
#line 2016 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 304:
#line 1108 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // Parse the option-def list entry map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 2026 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 305:
#line 1112 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // The name, code and type option def parameters are required.
    ctx.require("name", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.require("code", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.require("type", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    // parsing completed
}
#line 2038 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 319:
#line 1142 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr code(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("code", code);
}
#line 2047 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 321:
#line 1149 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2055 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 322:
#line 1151 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr prf(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("type", prf);
    ctx.leave();
}
#line 2065 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 323:
#line 1157 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2073 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 324:
#line 1159 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr rtypes(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("record-types", rtypes);
    ctx.leave();
}
#line 2083 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 325:
#line 1165 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2091 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 326:
#line 1167 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr space(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("space", space);
    ctx.leave();
}
#line 2101 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 328:
#line 1175 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2109 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 329:
#line 1177 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr encap(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("encapsulate", encap);
    ctx.leave();
}
#line 2119 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 330:
#line 1183 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr array(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("array", array);
}
#line 2128 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 331:
#line 1192 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("option-data", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.OPTION_DATA);
}
#line 2139 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 332:
#line 1197 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 2148 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 337:
#line 1216 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(m);
    ctx.stack_.push_back(m);
}
#line 2158 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 338:
#line 1220 "dhcp4_parser.yy" // lalr1.cc:859
    {
    /// @todo: the code or name parameters are required.
    ctx.stack_.pop_back();
}
#line 2167 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 339:
#line 1228 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // Parse the option-data list entry map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 2177 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 340:
#line 1232 "dhcp4_parser.yy" // lalr1.cc:859
    {
    /// @todo: the code or name parameters are required.
    // parsing completed
}
#line 2186 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 353:
#line 1263 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2194 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 354:
#line 1265 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr data(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("data", data);
    ctx.leave();
}
#line 2204 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 357:
#line 1275 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr space(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("csv-format", space);
}
#line 2213 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 358:
#line 1280 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr persist(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("always-send", persist);
}
#line 2222 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 359:
#line 1288 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("pools", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.POOLS);
}
#line 2233 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 360:
#line 1293 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 2242 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 365:
#line 1308 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(m);
    ctx.stack_.push_back(m);
}
#line 2252 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 366:
#line 1312 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // The pool parameter is required.
    ctx.require("pool", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.stack_.pop_back();
}
#line 2262 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 367:
#line 1318 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // Parse the pool list entry map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 2272 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 368:
#line 1322 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // The pool parameter is required.
    ctx.require("pool", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    // parsing completed
}
#line 2282 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 375:
#line 1338 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2290 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 376:
#line 1340 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr pool(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("pool", pool);
    ctx.leave();
}
#line 2300 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 377:
#line 1346 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2308 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 378:
#line 1348 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.back()->set("user-context", yystack_[0].value.as< ElementPtr > ());
    ctx.leave();
}
#line 2317 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 379:
#line 1356 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("reservations", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.RESERVATIONS);
}
#line 2328 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 380:
#line 1361 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 2337 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 385:
#line 1374 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(m);
    ctx.stack_.push_back(m);
}
#line 2347 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 386:
#line 1378 "dhcp4_parser.yy" // lalr1.cc:859
    {
    /// @todo: an identifier parameter is required.
    ctx.stack_.pop_back();
}
#line 2356 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 387:
#line 1383 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // Parse the reservations list entry map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 2366 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 388:
#line 1387 "dhcp4_parser.yy" // lalr1.cc:859
    {
    /// @todo: an identifier parameter is required.
    // parsing completed
}
#line 2375 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 406:
#line 1416 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2383 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 407:
#line 1418 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr next_server(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("next-server", next_server);
    ctx.leave();
}
#line 2393 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 408:
#line 1424 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2401 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 409:
#line 1426 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr srv(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("server-hostname", srv);
    ctx.leave();
}
#line 2411 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 410:
#line 1432 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2419 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 411:
#line 1434 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr bootfile(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("boot-file-name", bootfile);
    ctx.leave();
}
#line 2429 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 412:
#line 1440 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2437 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 413:
#line 1442 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr addr(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("ip-address", addr);
    ctx.leave();
}
#line 2447 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 414:
#line 1448 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2455 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 415:
#line 1450 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr d(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("duid", d);
    ctx.leave();
}
#line 2465 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 416:
#line 1456 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2473 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 417:
#line 1458 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr hw(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("hw-address", hw);
    ctx.leave();
}
#line 2483 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 418:
#line 1464 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2491 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 419:
#line 1466 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr hw(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("client-id", hw);
    ctx.leave();
}
#line 2501 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 420:
#line 1472 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2509 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 421:
#line 1474 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr hw(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("circuit-id", hw);
    ctx.leave();
}
#line 2519 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 422:
#line 1480 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2527 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 423:
#line 1482 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr hw(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("flex-id", hw);
    ctx.leave();
}
#line 2537 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 424:
#line 1488 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2545 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 425:
#line 1490 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr host(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("hostname", host);
    ctx.leave();
}
#line 2555 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 426:
#line 1496 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr c(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("client-classes", c);
    ctx.stack_.push_back(c);
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2566 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 427:
#line 1501 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 2575 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 428:
#line 1509 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("relay", m);
    ctx.stack_.push_back(m);
    ctx.enter(ctx.RELAY);
}
#line 2586 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 429:
#line 1514 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 2595 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 430:
#line 1519 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2603 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 431:
#line 1521 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr ip(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("ip-address", ip);
    ctx.leave();
}
#line 2613 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 432:
#line 1530 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("client-classes", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.CLIENT_CLASSES);
}
#line 2624 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 433:
#line 1535 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 2633 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 436:
#line 1544 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(m);
    ctx.stack_.push_back(m);
}
#line 2643 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 437:
#line 1548 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // The name client class parameter is required.
    ctx.require("name", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    ctx.stack_.pop_back();
}
#line 2653 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 451:
#line 1574 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2661 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 452:
#line 1576 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr test(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("test", test);
    ctx.leave();
}
#line 2671 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 453:
#line 1586 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr time(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("dhcp4o6-port", time);
}
#line 2680 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 454:
#line 1593 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("control-socket", m);
    ctx.stack_.push_back(m);
    ctx.enter(ctx.CONTROL_SOCKET);
}
#line 2691 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 455:
#line 1598 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 2700 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 460:
#line 1611 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2708 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 461:
#line 1613 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr stype(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("socket-type", stype);
    ctx.leave();
}
#line 2718 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 462:
#line 1619 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2726 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 463:
#line 1621 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr name(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("socket-name", name);
    ctx.leave();
}
#line 2736 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 464:
#line 1629 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("dhcp-ddns", m);
    ctx.stack_.push_back(m);
    ctx.enter(ctx.DHCP_DDNS);
}
#line 2747 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 465:
#line 1634 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // The enable updates DHCP DDNS parameter is required.
    ctx.require("enable-updates", ctx.loc2pos(yystack_[2].location), ctx.loc2pos(yystack_[0].location));
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 2758 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 466:
#line 1641 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // Parse the dhcp-ddns map
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.push_back(m);
}
#line 2768 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 467:
#line 1645 "dhcp4_parser.yy" // lalr1.cc:859
    {
    // The enable updates DHCP DDNS parameter is required.
    ctx.require("enable-updates", ctx.loc2pos(yystack_[3].location), ctx.loc2pos(yystack_[0].location));
    // parsing completed
}
#line 2778 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 485:
#line 1672 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr b(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("enable-updates", b);
}
#line 2787 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 486:
#line 1677 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2795 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 487:
#line 1679 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr s(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("qualifying-suffix", s);
    ctx.leave();
}
#line 2805 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 488:
#line 1685 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2813 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 489:
#line 1687 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr s(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("server-ip", s);
    ctx.leave();
}
#line 2823 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 490:
#line 1693 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr i(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("server-port", i);
}
#line 2832 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 491:
#line 1698 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 2840 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 492:
#line 1700 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr s(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("sender-ip", s);
    ctx.leave();
}
#line 2850 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 493:
#line 1706 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr i(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("sender-port", i);
}
#line 2859 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 494:
#line 1711 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr i(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("max-queue-size", i);
}
#line 2868 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 495:
#line 1716 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NCR_PROTOCOL);
}
#line 2876 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 496:
#line 1718 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.back()->set("ncr-protocol", yystack_[0].value.as< ElementPtr > ());
    ctx.leave();
}
#line 2885 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 497:
#line 1724 "dhcp4_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("UDP", ctx.loc2pos(yystack_[0].location))); }
#line 2891 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 498:
#line 1725 "dhcp4_parser.yy" // lalr1.cc:859
    { yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("TCP", ctx.loc2pos(yystack_[0].location))); }
#line 2897 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 499:
#line 1728 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NCR_FORMAT);
}
#line 2905 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 500:
#line 1730 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr json(new StringElement("JSON", ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("ncr-format", json);
    ctx.leave();
}
#line 2915 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 501:
#line 1736 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr b(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("always-include-fqdn", b);
}
#line 2924 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 502:
#line 1741 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr b(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("override-no-update", b);
}
#line 2933 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 503:
#line 1746 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr b(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("override-client-update", b);
}
#line 2942 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 504:
#line 1751 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.REPLACE_CLIENT_NAME);
}
#line 2950 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 505:
#line 1753 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.back()->set("replace-client-name", yystack_[0].value.as< ElementPtr > ());
    ctx.leave();
}
#line 2959 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 506:
#line 1759 "dhcp4_parser.yy" // lalr1.cc:859
    {
      yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("when-present", ctx.loc2pos(yystack_[0].location))); 
      }
#line 2967 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 507:
#line 1762 "dhcp4_parser.yy" // lalr1.cc:859
    {
      yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("never", ctx.loc2pos(yystack_[0].location)));
      }
#line 2975 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 508:
#line 1765 "dhcp4_parser.yy" // lalr1.cc:859
    {
      yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("always", ctx.loc2pos(yystack_[0].location)));
      }
#line 2983 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 509:
#line 1768 "dhcp4_parser.yy" // lalr1.cc:859
    {
      yylhs.value.as< ElementPtr > () = ElementPtr(new StringElement("when-not-present", ctx.loc2pos(yystack_[0].location)));
      }
#line 2991 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 510:
#line 1771 "dhcp4_parser.yy" // lalr1.cc:859
    {
      error(yystack_[0].location, "boolean values for the replace-client-name are "
                "no longer supported");
      }
#line 3000 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 511:
#line 1777 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 3008 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 512:
#line 1779 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr s(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("generated-prefix", s);
    ctx.leave();
}
#line 3018 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 513:
#line 1787 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 3026 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 514:
#line 1789 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.back()->set("Dhcp6", yystack_[0].value.as< ElementPtr > ());
    ctx.leave();
}
#line 3035 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 515:
#line 1794 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 3043 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 516:
#line 1796 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.back()->set("DhcpDdns", yystack_[0].value.as< ElementPtr > ());
    ctx.leave();
}
#line 3052 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 517:
#line 1801 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 3060 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 518:
#line 1803 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.back()->set("Control-agent", yystack_[0].value.as< ElementPtr > ());
    ctx.leave();
}
#line 3069 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 519:
#line 1813 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("Logging", m);
    ctx.stack_.push_back(m);
    ctx.enter(ctx.LOGGING);
}
#line 3080 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 520:
#line 1818 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 3089 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 524:
#line 1835 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("loggers", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.LOGGERS);
}
#line 3100 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 525:
#line 1840 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 3109 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 528:
#line 1852 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(l);
    ctx.stack_.push_back(l);
}
#line 3119 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 529:
#line 1856 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
}
#line 3127 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 537:
#line 1871 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr dl(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("debuglevel", dl);
}
#line 3136 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 538:
#line 1876 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 3144 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 539:
#line 1878 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr sev(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("severity", sev);
    ctx.leave();
}
#line 3154 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 540:
#line 1884 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr l(new ListElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("output_options", l);
    ctx.stack_.push_back(l);
    ctx.enter(ctx.OUTPUT_OPTIONS);
}
#line 3165 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 541:
#line 1889 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
    ctx.leave();
}
#line 3174 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 544:
#line 1898 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr m(new MapElement(ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->add(m);
    ctx.stack_.push_back(m);
}
#line 3184 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 545:
#line 1902 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.stack_.pop_back();
}
#line 3192 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 552:
#line 1916 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ctx.enter(ctx.NO_KEYWORD);
}
#line 3200 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 553:
#line 1918 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr sev(new StringElement(yystack_[0].value.as< std::string > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("output", sev);
    ctx.leave();
}
#line 3210 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 554:
#line 1924 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr flush(new BoolElement(yystack_[0].value.as< bool > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("flush", flush);
}
#line 3219 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 555:
#line 1929 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr maxsize(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("maxsize", maxsize);
}
#line 3228 "dhcp4_parser.cc" // lalr1.cc:859
    break;

  case 556:
#line 1934 "dhcp4_parser.yy" // lalr1.cc:859
    {
    ElementPtr maxver(new IntElement(yystack_[0].value.as< int64_t > (), ctx.loc2pos(yystack_[0].location)));
    ctx.stack_.back()->set("maxver", maxver);
}
#line 3237 "dhcp4_parser.cc" // lalr1.cc:859
    break;


#line 3241 "dhcp4_parser.cc" // lalr1.cc:859
            default:
              break;
            }
        }
      catch (const syntax_error& yyexc)
        {
          error (yyexc);
          YYERROR;
        }
      YY_SYMBOL_PRINT ("-> $$ =", yylhs);
      yypop_ (yylen);
      yylen = 0;
      YY_STACK_PRINT ();

      // Shift the result of the reduction.
      yypush_ (YY_NULLPTR, yylhs);
    }
    goto yynewstate;

  /*--------------------------------------.
  | yyerrlab -- here on detecting error.  |
  `--------------------------------------*/
  yyerrlab:
    // If not already recovering from an error, report this error.
    if (!yyerrstatus_)
      {
        ++yynerrs_;
        error (yyla.location, yysyntax_error_ (yystack_[0].state, yyla));
      }


    yyerror_range[1].location = yyla.location;
    if (yyerrstatus_ == 3)
      {
        /* If just tried and failed to reuse lookahead token after an
           error, discard it.  */

        // Return failure if at end of input.
        if (yyla.type_get () == yyeof_)
          YYABORT;
        else if (!yyla.empty ())
          {
            yy_destroy_ ("Error: discarding", yyla);
            yyla.clear ();
          }
      }

    // Else will try to reuse lookahead token after shifting the error token.
    goto yyerrlab1;


  /*---------------------------------------------------.
  | yyerrorlab -- error raised explicitly by YYERROR.  |
  `---------------------------------------------------*/
  yyerrorlab:

    /* Pacify compilers like GCC when the user code never invokes
       YYERROR and the label yyerrorlab therefore never appears in user
       code.  */
    if (false)
      goto yyerrorlab;
    yyerror_range[1].location = yystack_[yylen - 1].location;
    /* Do not reclaim the symbols of the rule whose action triggered
       this YYERROR.  */
    yypop_ (yylen);
    yylen = 0;
    goto yyerrlab1;

  /*-------------------------------------------------------------.
  | yyerrlab1 -- common code for both syntax error and YYERROR.  |
  `-------------------------------------------------------------*/
  yyerrlab1:
    yyerrstatus_ = 3;   // Each real token shifted decrements this.
    {
      stack_symbol_type error_token;
      for (;;)
        {
          yyn = yypact_[yystack_[0].state];
          if (!yy_pact_value_is_default_ (yyn))
            {
              yyn += yyterror_;
              if (0 <= yyn && yyn <= yylast_ && yycheck_[yyn] == yyterror_)
                {
                  yyn = yytable_[yyn];
                  if (0 < yyn)
                    break;
                }
            }

          // Pop the current state because it cannot handle the error token.
          if (yystack_.size () == 1)
            YYABORT;

          yyerror_range[1].location = yystack_[0].location;
          yy_destroy_ ("Error: popping", yystack_[0]);
          yypop_ ();
          YY_STACK_PRINT ();
        }

      yyerror_range[2].location = yyla.location;
      YYLLOC_DEFAULT (error_token.location, yyerror_range, 2);

      // Shift the error token.
      error_token.state = yyn;
      yypush_ ("Shifting", error_token);
    }
    goto yynewstate;

    // Accept.
  yyacceptlab:
    yyresult = 0;
    goto yyreturn;

    // Abort.
  yyabortlab:
    yyresult = 1;
    goto yyreturn;

  yyreturn:
    if (!yyla.empty ())
      yy_destroy_ ("Cleanup: discarding lookahead", yyla);

    /* Do not reclaim the symbols of the rule whose action triggered
       this YYABORT or YYACCEPT.  */
    yypop_ (yylen);
    while (1 < yystack_.size ())
      {
        yy_destroy_ ("Cleanup: popping", yystack_[0]);
        yypop_ ();
      }

    return yyresult;
  }
    catch (...)
      {
        YYCDEBUG << "Exception caught: cleaning lookahead and stack"
                 << std::endl;
        // Do not try to display the values of the reclaimed symbols,
        // as their printer might throw an exception.
        if (!yyla.empty ())
          yy_destroy_ (YY_NULLPTR, yyla);

        while (1 < yystack_.size ())
          {
            yy_destroy_ (YY_NULLPTR, yystack_[0]);
            yypop_ ();
          }
        throw;
      }
  }

  void
  Dhcp4Parser::error (const syntax_error& yyexc)
  {
    error (yyexc.location, yyexc.what());
  }

  // Generate an error message.
  std::string
  Dhcp4Parser::yysyntax_error_ (state_type yystate, const symbol_type& yyla) const
  {
    // Number of reported tokens (one for the "unexpected", one per
    // "expected").
    size_t yycount = 0;
    // Its maximum.
    enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
    // Arguments of yyformat.
    char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];

    /* There are many possibilities here to consider:
       - If this state is a consistent state with a default action, then
         the only way this function was invoked is if the default action
         is an error action.  In that case, don't check for expected
         tokens because there are none.
       - The only way there can be no lookahead present (in yyla) is
         if this state is a consistent state with a default action.
         Thus, detecting the absence of a lookahead is sufficient to
         determine that there is no unexpected or expected token to
         report.  In that case, just report a simple "syntax error".
       - Don't assume there isn't a lookahead just because this state is
         a consistent state with a default action.  There might have
         been a previous inconsistent state, consistent state with a
         non-default action, or user semantic action that manipulated
         yyla.  (However, yyla is currently not documented for users.)
       - Of course, the expected token list depends on states to have
         correct lookahead information, and it depends on the parser not
         to perform extra reductions after fetching a lookahead from the
         scanner and before detecting a syntax error.  Thus, state
         merging (from LALR or IELR) and default reductions corrupt the
         expected token list.  However, the list is correct for
         canonical LR with one exception: it will still contain any
         token that will not be accepted due to an error action in a
         later state.
    */
    if (!yyla.empty ())
      {
        int yytoken = yyla.type_get ();
        yyarg[yycount++] = yytname_[yytoken];
        int yyn = yypact_[yystate];
        if (!yy_pact_value_is_default_ (yyn))
          {
            /* Start YYX at -YYN if negative to avoid negative indexes in
               YYCHECK.  In other words, skip the first -YYN actions for
               this state because they are default actions.  */
            int yyxbegin = yyn < 0 ? -yyn : 0;
            // Stay within bounds of both yycheck and yytname.
            int yychecklim = yylast_ - yyn + 1;
            int yyxend = yychecklim < yyntokens_ ? yychecklim : yyntokens_;
            for (int yyx = yyxbegin; yyx < yyxend; ++yyx)
              if (yycheck_[yyx + yyn] == yyx && yyx != yyterror_
                  && !yy_table_value_is_error_ (yytable_[yyx + yyn]))
                {
                  if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                    {
                      yycount = 1;
                      break;
                    }
                  else
                    yyarg[yycount++] = yytname_[yyx];
                }
          }
      }

    char const* yyformat = YY_NULLPTR;
    switch (yycount)
      {
#define YYCASE_(N, S)                         \
        case N:                               \
          yyformat = S;                       \
        break
        YYCASE_(0, YY_("syntax error"));
        YYCASE_(1, YY_("syntax error, unexpected %s"));
        YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
        YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
        YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
        YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
#undef YYCASE_
      }

    std::string yyres;
    // Argument number.
    size_t yyi = 0;
    for (char const* yyp = yyformat; *yyp; ++yyp)
      if (yyp[0] == '%' && yyp[1] == 's' && yyi < yycount)
        {
          yyres += yytnamerr_ (yyarg[yyi++]);
          ++yyp;
        }
      else
        yyres += *yyp;
    return yyres;
  }


  const short int Dhcp4Parser::yypact_ninf_ = -712;

  const signed char Dhcp4Parser::yytable_ninf_ = -1;

  const short int
  Dhcp4Parser::yypact_[] =
  {
     248,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,    34,    32,    33,    35,    44,    46,    50,
      54,    92,    98,   100,   122,   128,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
      32,   -72,     9,   141,   178,    22,    -2,   185,    86,    -1,
      57,    -8,   266,  -712,   196,   186,   208,   252,   261,  -712,
    -712,  -712,  -712,  -712,   267,  -712,    61,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,   269,   271,  -712,  -712,  -712,  -712,
    -712,   272,   274,   275,   276,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,   283,  -712,  -712,  -712,    65,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,   285,    76,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,   286,   287,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,    95,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,   116,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,   284,
     291,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,   289,  -712,  -712,   296,  -712,  -712,  -712,
     298,  -712,  -712,   295,   292,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,   300,   301,  -712,  -712,
    -712,  -712,   304,   306,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,   130,  -712,  -712,  -712,   313,  -712,
    -712,   322,  -712,   327,   329,  -712,  -712,   330,   332,   333,
    -712,  -712,  -712,   145,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
      32,    32,  -712,   172,   334,   335,   336,   339,   341,  -712,
       9,  -712,   342,   197,   209,   354,   356,   357,   358,   359,
     219,   247,   249,   253,   392,   394,   395,   396,   398,   399,
     400,   401,   258,   403,   404,   141,  -712,   405,   406,   407,
     262,   178,  -712,    25,   410,   411,   412,   413,   414,   415,
     416,   417,   277,   273,   418,   420,   422,   423,    22,  -712,
     424,    -2,  -712,   425,   426,   427,   428,   429,   430,   431,
     432,  -712,   185,  -712,   433,   434,   293,   435,   436,   438,
     294,  -712,    -1,   439,   297,   302,  -712,    57,   441,   442,
      68,  -712,   303,   444,   446,   307,   447,   310,   311,   450,
     456,   317,   318,   319,   457,   461,   266,  -712,  -712,  -712,
     466,   464,   465,    32,    32,    32,  -712,   468,  -712,  -712,
     326,   337,   340,   469,   470,  -712,  -712,  -712,  -712,   474,
     475,   477,   478,   481,   483,   484,   485,  -712,   486,   487,
    -712,   490,   107,   245,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,   482,   488,  -712,  -712,  -712,   349,
     351,   352,   495,   494,   360,   361,   362,  -712,  -712,   183,
     364,   498,   497,  -712,   365,  -712,   490,   366,   367,   368,
     369,   370,   371,   372,  -712,   373,   374,  -712,   375,   376,
     377,  -712,  -712,   378,  -712,  -712,  -712,   379,    32,  -712,
    -712,   380,   381,  -712,   382,  -712,  -712,    17,   388,  -712,
    -712,  -712,    55,   383,  -712,    32,   141,   408,  -712,  -712,
    -712,   178,  -712,  -712,  -712,   315,   315,   524,   526,   527,
     528,   153,    26,   529,   129,    70,   266,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,   533,  -712,    25,  -712,  -712,
    -712,   531,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,   532,   453,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,   146,  -712,   152,  -712,
    -712,   165,  -712,  -712,  -712,  -712,   537,   538,   539,   540,
     541,  -712,  -712,  -712,   175,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,   193,  -712,
     542,   543,  -712,  -712,   544,   546,  -712,  -712,   545,   549,
    -712,  -712,   547,   551,  -712,  -712,  -712,  -712,  -712,  -712,
      80,  -712,  -712,  -712,  -712,  -712,  -712,  -712,   103,  -712,
     550,   552,  -712,   553,   554,   555,   556,   557,   558,   207,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,   210,
    -712,  -712,  -712,   211,   419,   421,  -712,  -712,   559,   560,
    -712,  -712,   561,   566,  -712,  -712,   562,  -712,   567,   408,
    -712,  -712,   568,   569,   570,   571,   437,   397,   440,   443,
     448,   572,   573,   315,  -712,  -712,    22,  -712,   524,    -1,
    -712,   526,    57,  -712,   527,    73,  -712,   528,   153,  -712,
      26,  -712,    -8,  -712,   529,   449,   451,   452,   454,   455,
     458,   129,  -712,   574,   575,    70,  -712,  -712,  -712,   576,
     577,  -712,    -2,  -712,   531,   185,  -712,   532,   579,  -712,
     581,  -712,   254,   445,   460,   462,  -712,  -712,  -712,  -712,
    -712,   463,   467,  -712,   238,  -712,   582,  -712,   583,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
     239,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
     241,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,   471,
     472,  -712,  -712,   473,   242,  -712,   586,  -712,   476,   580,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,    73,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,   224,  -712,  -712,     3,   580,  -712,
    -712,   585,  -712,  -712,  -712,   250,  -712,  -712,  -712,  -712,
    -712,   591,   479,   594,     3,  -712,   596,  -712,   489,  -712,
     597,  -712,  -712,   225,  -712,    69,   597,  -712,  -712,   601,
     604,   607,   251,  -712,  -712,  -712,  -712,  -712,  -712,   608,
     480,   491,   492,    69,  -712,   496,  -712,  -712,  -712,  -712,
    -712
  };

  const unsigned short int
  Dhcp4Parser::yydefact_[] =
  {
       0,     2,     4,     6,     8,    10,    12,    14,    16,    18,
      20,    22,    24,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     1,    41,    34,    30,
      29,    26,    27,    28,    33,     3,    31,    32,    54,     5,
      66,     7,   108,     9,   221,    11,   367,    13,   387,    15,
     296,    17,   304,    19,   339,    21,   186,    23,   466,    25,
      43,    37,     0,     0,     0,     0,     0,   389,     0,   306,
     341,     0,     0,    45,     0,    44,     0,     0,    38,    64,
     519,   513,   515,   517,     0,    63,     0,    56,    58,    60,
      61,    62,    59,   100,     0,     0,   406,   408,   410,   121,
     123,     0,     0,     0,     0,   213,   294,   331,   269,   164,
     432,   178,   197,     0,   454,   464,    93,     0,    68,    70,
      71,    72,    73,    88,    89,    76,    77,    78,    79,    83,
      84,    74,    75,    81,    82,    90,    91,    92,    80,    85,
      86,    87,   110,   112,   116,     0,     0,   102,   104,   105,
     106,   107,   436,   250,   252,   254,   359,   377,   248,   256,
     258,     0,     0,   262,   260,   379,   428,   247,   225,   226,
     227,   239,     0,   223,   230,   243,   244,   245,   231,   232,
     235,   237,   233,   234,   228,   229,   246,   236,   240,   241,
     242,   238,   375,   374,   372,     0,   369,   371,   373,   426,
     414,   416,   420,   418,   424,   422,   412,   405,   401,     0,
     390,   391,   402,   403,   404,   398,   393,   399,   395,   396,
     397,   400,   394,     0,   321,   154,     0,   325,   323,   328,
       0,   317,   318,     0,   307,   308,   310,   320,   311,   312,
     313,   327,   314,   315,   316,   353,     0,     0,   351,   352,
     355,   356,     0,   342,   343,   345,   346,   347,   348,   349,
     350,   193,   195,   190,     0,   188,   191,   192,     0,   486,
     488,     0,   491,     0,     0,   495,   499,     0,     0,     0,
     504,   511,   484,     0,   468,   470,   471,   472,   473,   474,
     475,   476,   477,   478,   479,   480,   481,   482,   483,    42,
       0,     0,    35,     0,     0,     0,     0,     0,     0,    53,
       0,    55,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    67,     0,     0,     0,
       0,     0,   109,   438,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   222,
       0,     0,   368,     0,     0,     0,     0,     0,     0,     0,
       0,   388,     0,   297,     0,     0,     0,     0,     0,     0,
       0,   305,     0,     0,     0,     0,   340,     0,     0,     0,
       0,   187,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   467,    46,    39,
       0,     0,     0,     0,     0,     0,    57,     0,    98,    99,
       0,     0,     0,     0,     0,    94,    95,    96,    97,     0,
       0,     0,     0,     0,     0,     0,     0,   453,     0,     0,
      69,     0,     0,     0,   120,   103,   451,   449,   450,   444,
     445,   446,   447,   448,     0,   439,   440,   442,   443,     0,
       0,     0,     0,     0,     0,     0,     0,   267,   268,     0,
       0,     0,     0,   224,     0,   370,     0,     0,     0,     0,
       0,     0,     0,     0,   392,     0,     0,   319,     0,     0,
       0,   330,   309,     0,   357,   358,   344,     0,     0,   189,
     485,     0,     0,   490,     0,   493,   494,     0,     0,   501,
     502,   503,     0,     0,   469,     0,     0,     0,   514,   516,
     518,     0,   407,   409,   411,     0,     0,   215,   298,   333,
     271,     0,     0,   180,     0,     0,     0,    47,   111,   114,
     115,   113,   118,   119,   117,     0,   437,     0,   251,   253,
     255,   361,    36,   378,   249,   257,   259,   264,   265,   266,
     263,   261,   381,     0,   376,   427,   415,   417,   421,   419,
     425,   423,   413,   322,   155,   326,   324,   329,   354,   194,
     196,   487,   489,   492,   497,   498,   496,   500,   506,   507,
     508,   509,   510,   505,   512,    40,     0,   524,     0,   521,
     523,     0,   140,   147,   149,   151,     0,     0,     0,     0,
       0,   160,   162,   139,     0,   125,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,   137,   138,     0,   219,
       0,   216,   217,   302,     0,   299,   300,   337,     0,   334,
     335,   275,     0,   272,   273,   173,   174,   175,   176,   177,
       0,   166,   168,   169,   170,   171,   172,   434,     0,   184,
       0,   181,   182,     0,     0,     0,     0,     0,     0,     0,
     199,   201,   202,   203,   204,   205,   206,   460,   462,     0,
     456,   458,   459,     0,    49,     0,   441,   365,     0,   362,
     363,   385,     0,   382,   383,   430,     0,    65,     0,     0,
     520,   101,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   122,   124,     0,   214,     0,   306,
     295,     0,   341,   332,     0,     0,   270,     0,     0,   165,
       0,   433,     0,   179,     0,     0,     0,     0,     0,     0,
       0,     0,   198,     0,     0,     0,   455,   465,    51,     0,
      50,   452,     0,   360,     0,   389,   380,     0,     0,   429,
       0,   522,     0,     0,     0,     0,   153,   156,   157,   158,
     159,     0,     0,   126,     0,   218,     0,   301,     0,   336,
     293,   292,   282,   283,   285,   279,   280,   281,   291,   290,
       0,   277,   284,   286,   287,   288,   289,   274,   167,   435,
       0,   183,   207,   208,   209,   210,   211,   212,   200,     0,
       0,   457,    48,     0,     0,   364,     0,   384,     0,     0,
     142,   143,   144,   145,   146,   141,   148,   150,   152,   161,
     163,   220,   303,   338,     0,   276,   185,   461,   463,    52,
     366,   386,   431,   528,     0,   526,   278,     0,     0,   525,
     540,     0,   538,   536,   532,     0,   530,   534,   535,   533,
     527,     0,     0,     0,     0,   529,     0,   537,     0,   531,
       0,   539,   544,     0,   542,     0,     0,   541,   552,     0,
       0,     0,     0,   546,   548,   549,   550,   551,   543,     0,
       0,     0,     0,     0,   545,     0,   554,   555,   556,   547,
     553
  };

  const short int
  Dhcp4Parser::yypgoto_[] =
  {
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,   -49,  -712,   101,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,   105,  -712,  -712,  -712,   -62,
    -712,  -712,  -712,   305,  -712,  -712,  -712,  -712,    97,   281,
     -44,   -34,   -30,  -712,  -712,   -29,  -712,  -712,    63,   280,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,    91,   -91,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,   -69,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
    -104,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
    -109,  -712,  -712,  -712,  -106,   243,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -113,  -712,  -712,  -712,  -712,  -712,  -712,
    -711,  -712,  -712,  -712,   -89,  -712,  -712,  -712,   -85,   279,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -707,  -712,
    -712,  -712,  -519,  -712,  -698,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,   -95,  -712,  -712,  -200,   -66,  -712,  -712,
    -712,  -712,  -712,   -86,  -712,  -712,  -712,   -81,  -712,   259,
    -712,   -64,  -712,  -712,  -712,  -712,  -712,   -46,  -712,  -712,
    -712,  -712,  -712,   -50,  -712,  -712,  -712,   -82,  -712,  -712,
    -712,   -77,  -712,   260,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,  -108,  -712,  -712,  -712,  -103,
     308,  -712,  -712,   -54,  -712,  -712,  -712,  -712,  -712,  -107,
    -712,  -712,  -712,  -100,  -712,   290,   -45,  -712,   -42,  -712,
     -37,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -687,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,   112,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,   -97,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,   125,   246,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,
    -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,  -712,   -35,
    -712,  -712,  -712,  -183,  -712,  -712,  -197,  -712,  -712,  -712,
    -712,  -712,  -712,  -208,  -712,  -712,  -220,  -712,  -712,  -712,
    -712,  -712
  };

  const short int
  Dhcp4Parser::yydefgoto_[] =
  {
      -1,    13,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    34,    35,    36,    61,   553,    77,
      78,    37,    60,    74,    75,   538,   684,   749,   750,   116,
      39,    62,    86,    87,    88,   304,    41,    63,   117,   118,
     119,   120,   121,   122,   123,   124,   125,   312,   146,   147,
      43,    64,   148,   337,   149,   338,   541,   150,   339,   544,
     151,   126,   318,   127,   319,   614,   615,   616,   702,   825,
     617,   703,   618,   704,   619,   705,   620,   232,   375,   622,
     623,   624,   625,   626,   711,   627,   712,   128,   328,   650,
     651,   652,   653,   654,   655,   656,   129,   330,   660,   661,
     662,   732,    57,    71,   264,   265,   266,   388,   267,   389,
     130,   331,   669,   670,   671,   672,   673,   674,   675,   676,
     131,   324,   630,   631,   632,   716,    45,    65,   172,   173,
     174,   349,   175,   344,   176,   345,   177,   346,   178,   350,
     179,   351,   180,   355,   181,   354,   560,   182,   183,   132,
     327,   642,   643,   644,   725,   790,   791,   133,   325,    51,
      68,   634,   635,   636,   719,    53,    69,   233,   234,   235,
     236,   237,   238,   239,   374,   240,   378,   241,   377,   242,
     243,   379,   244,   134,   326,   638,   639,   640,   722,    55,
      70,   252,   253,   254,   255,   256,   383,   257,   258,   259,
     260,   185,   347,   688,   689,   690,   752,    47,    66,   195,
     196,   197,   360,   186,   348,   187,   356,   692,   693,   694,
     755,    49,    67,   209,   210,   211,   135,   315,   136,   316,
     137,   317,   215,   370,   216,   364,   217,   365,   218,   367,
     219,   366,   220,   369,   221,   368,   222,   363,   191,   357,
     696,   758,   138,   329,   658,   343,   454,   455,   456,   457,
     458,   545,   139,   140,   333,   679,   680,   681,   743,   682,
     744,   141,   334,    59,    72,   283,   284,   285,   286,   393,
     287,   394,   288,   289,   396,   290,   291,   292,   399,   586,
     293,   400,   294,   295,   296,   297,   404,   593,   298,   405,
      89,   306,    90,   307,    91,   308,    92,   305,   598,   599,
     600,   698,   844,   845,   847,   855,   856,   857,   858,   863,
     859,   861,   873,   874,   875,   882,   883,   884,   889,   885,
     886,   887
  };

  const unsigned short int
  Dhcp4Parser::yytable_[] =
  {
      85,   249,   223,   167,   193,   207,   250,   231,   248,   263,
     282,    73,   198,   657,   786,   184,   194,   208,   787,    79,
     188,   168,   212,   189,   251,   213,   224,   789,   190,   152,
     214,   169,   584,   152,    26,   170,   171,    27,   796,    28,
      38,    29,    40,    95,    96,    97,    98,    96,    97,    98,
     107,    42,   225,    44,   226,   227,   225,    46,   228,   229,
     230,    48,   192,   157,   310,   101,   102,   103,   335,   311,
     153,   154,   155,   336,   107,    76,   106,   107,   225,   341,
     152,   261,   262,   728,   342,   156,   729,   157,   158,   159,
     160,   161,   162,   163,    95,    96,    97,    98,   358,    50,
     164,   165,   446,   359,   164,    52,   730,    54,   166,   731,
     225,   245,   226,   227,   246,   247,   101,   102,   103,   361,
     105,   539,   540,   786,   362,   107,   225,   787,   850,    56,
     851,   852,    80,   390,   585,    58,   789,   106,   391,    84,
     159,    81,    82,    83,   163,    84,    84,   796,   406,   335,
      84,   164,    93,   407,   697,   699,    84,   261,   262,   166,
     700,    94,    95,    96,    97,    98,    99,   100,   341,    84,
     677,   678,    84,   701,   588,   589,   590,   591,   713,    30,
      31,    32,    33,   714,   101,   102,   103,   104,   105,   300,
     142,   143,   106,   107,   144,   878,   713,   145,   879,   880,
     881,   715,   299,   108,    84,   592,   788,    96,    97,    98,
     741,   799,   301,   745,   406,   742,   109,   110,   746,   747,
      84,   663,   664,   665,   666,   667,   668,   848,   876,   111,
     849,   877,   112,   645,   646,   647,   648,   107,   649,   113,
     114,   358,   834,   115,   390,   361,   831,   835,    85,   836,
     840,   408,   409,   864,   893,   557,   558,   559,   865,   894,
     302,   199,   542,   543,   303,   200,   201,   202,   203,   204,
     205,   309,   206,   313,   448,   314,   320,   449,   321,   322,
     323,   447,   820,   821,   822,   823,   824,   332,    84,   340,
     352,   353,   371,   450,   372,   382,   167,   373,   451,   193,
     376,   452,   380,   381,   384,   385,   453,   198,   184,   387,
     207,   194,   386,   188,   168,   788,   189,   392,   249,   410,
     231,   190,   208,   250,   169,   248,   395,   212,   170,   171,
     213,   397,    84,   398,   401,   214,   402,   403,   411,   412,
     413,   251,   602,   414,   282,   415,   417,   418,   603,   604,
     605,   606,   607,   608,   609,   610,   611,   612,   420,   419,
     421,   422,   423,   424,   518,   519,   520,   425,   225,   268,
     269,   270,   271,   272,   273,   274,   275,   276,   277,   278,
     279,   280,   281,     1,     2,     3,     4,     5,     6,     7,
       8,     9,    10,    11,    12,   426,   429,   427,   430,   431,
     432,   428,   433,   434,   435,   436,   437,   438,   439,   441,
     442,   443,   444,    84,   459,   460,   461,   462,   463,   464,
     465,   466,   469,   468,   470,   467,   471,   472,   474,   476,
     477,   478,   479,   480,   481,   482,   483,   485,   486,   488,
     489,   487,   490,   493,   491,   497,   498,   494,   501,   580,
     502,   504,   495,   500,   507,   503,   621,   621,   505,   506,
     508,   512,    84,   613,   613,   513,   595,   509,   510,   511,
     515,   516,   517,   522,   282,   521,   525,   526,   448,   527,
     528,   449,   529,   530,   523,   447,   531,   524,   532,   533,
     546,   547,   534,   535,   536,   537,   548,   450,   549,   550,
     551,    28,   451,   562,   563,   452,   587,   554,   555,   556,
     453,   561,   564,   566,   567,   568,   569,   570,   571,   572,
     573,   574,   575,   576,   577,   578,   579,   581,   582,   583,
     594,   629,   597,   633,   637,   641,   659,   685,   687,   691,
     695,   706,   707,   708,   709,   710,   718,   767,   717,   721,
     720,   723,   724,   726,   727,   734,   733,   735,   736,   737,
     738,   739,   740,   754,   552,   753,   748,   756,   751,   757,
     759,   760,   762,   763,   764,   765,   771,   772,   809,   810,
     813,   565,   812,   818,   601,   766,   819,   843,   768,   862,
     832,   833,   826,   769,   841,   866,   770,   802,   868,   803,
     804,   870,   805,   806,   872,   890,   807,   827,   891,   828,
     829,   892,   895,   596,   830,   416,   440,   628,   837,   838,
     839,   445,   773,   842,   798,   801,   800,   867,   808,   775,
     896,   774,   797,   499,   846,   777,   871,   473,   776,   897,
     898,   492,   779,   900,   621,   778,   815,   496,   811,   814,
     817,   613,   514,   249,   167,   816,   785,   231,   250,   686,
     248,   683,   484,   780,   761,   860,   184,   869,   888,   475,
     263,   188,   168,   899,   189,   792,   251,     0,     0,   190,
     793,   781,   169,   794,     0,     0,   170,   171,   795,     0,
     193,   782,     0,   207,     0,   783,   784,     0,   198,     0,
       0,     0,   194,     0,     0,   208,     0,     0,     0,     0,
     212,     0,     0,   213,     0,     0,     0,     0,   214,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   785,     0,     0,     0,     0,
       0,     0,   780,     0,     0,     0,     0,     0,   854,     0,
       0,     0,     0,     0,   792,   853,     0,     0,     0,   793,
     781,     0,   794,     0,     0,   854,     0,   795,     0,     0,
     782,     0,   853,     0,   783,   784
  };

  const short int
  Dhcp4Parser::yycheck_[] =
  {
      62,    70,    68,    65,    66,    67,    70,    69,    70,    71,
      72,    60,    66,   532,   725,    65,    66,    67,   725,    10,
      65,    65,    67,    65,    70,    67,    27,   725,    65,     7,
      67,    65,    15,     7,     0,    65,    65,     5,   725,     7,
       7,     9,     7,    21,    22,    23,    24,    22,    23,    24,
      52,     7,    53,     7,    55,    56,    53,     7,    59,    60,
      61,     7,    64,    65,     3,    43,    44,    45,     3,     8,
      48,    49,    50,     8,    52,   147,    51,    52,    53,     3,
       7,    89,    90,     3,     8,    63,     6,    65,    66,    67,
      68,    69,    70,    71,    21,    22,    23,    24,     3,     7,
      78,    79,    77,     8,    78,     7,     3,     7,    86,     6,
      53,    54,    55,    56,    57,    58,    43,    44,    45,     3,
      47,    14,    15,   834,     8,    52,    53,   834,   125,     7,
     127,   128,   123,     3,   117,     7,   834,    51,     8,   147,
      67,   132,   133,   134,    71,   147,   147,   834,     3,     3,
     147,    78,    11,     8,     8,     3,   147,    89,    90,    86,
       8,    20,    21,    22,    23,    24,    25,    26,     3,   147,
     100,   101,   147,     8,   119,   120,   121,   122,     3,   147,
     148,   149,   150,     8,    43,    44,    45,    46,    47,     3,
      12,    13,    51,    52,    16,   126,     3,    19,   129,   130,
     131,     8,     6,    62,   147,   150,   725,    22,    23,    24,
       3,   730,     4,     3,     3,     8,    75,    76,     8,     8,
     147,    92,    93,    94,    95,    96,    97,     3,     3,    88,
       6,     6,    91,    80,    81,    82,    83,    52,    85,    98,
      99,     3,     3,   102,     3,     3,     8,     8,   310,     8,
       8,   300,   301,     3,     3,    72,    73,    74,     8,     8,
       8,    76,    17,    18,     3,    80,    81,    82,    83,    84,
      85,     4,    87,     4,   343,     4,     4,   343,     4,     4,
       4,   343,    28,    29,    30,    31,    32,     4,   147,     4,
       4,     4,     8,   343,     3,     3,   358,     8,   343,   361,
       4,   343,     4,     8,     4,     4,   343,   361,   358,     3,
     372,   361,     8,   358,   358,   834,   358,     4,   387,   147,
     382,   358,   372,   387,   358,   387,     4,   372,   358,   358,
     372,     4,   147,     4,     4,   372,     4,     4,     4,     4,
       4,   387,    27,     4,   406,     4,     4,   150,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,     4,   150,
       4,     4,     4,     4,   413,   414,   415,   148,    53,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   148,     4,   148,     4,     4,
       4,   148,     4,     4,     4,     4,   148,     4,     4,     4,
       4,     4,   150,   147,     4,     4,     4,     4,     4,     4,
       4,     4,     4,   150,     4,   148,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     4,     4,
       4,   148,     4,     4,   150,     4,     4,   150,     4,   498,
       4,     4,   150,   150,     4,   148,   525,   526,   148,   148,
       4,     4,   147,   525,   526,     4,   515,   150,   150,   150,
       4,     7,     7,   147,   536,     7,     7,     7,   547,     5,
       5,   547,     5,     5,   147,   547,     5,   147,     5,     5,
       8,     3,     7,     7,     7,     5,   147,   547,   147,   147,
       5,     7,   547,     5,     7,   547,   118,   147,   147,   147,
     547,   147,   147,   147,   147,   147,   147,   147,   147,   147,
     147,   147,   147,   147,   147,   147,   147,   147,   147,   147,
     147,     7,   124,     7,     7,     7,     7,     4,     7,     7,
      87,     4,     4,     4,     4,     4,     3,   150,     6,     3,
       6,     6,     3,     6,     3,     3,     6,     4,     4,     4,
       4,     4,     4,     3,   463,     6,   147,     6,   147,     3,
       8,     4,     4,     4,     4,     4,     4,     4,     4,     4,
       3,   476,     6,     4,   521,   148,     5,     7,   148,     4,
       8,     8,   147,   150,     8,     4,   148,   148,     4,   148,
     148,     5,   148,   148,     7,     4,   148,   147,     4,   147,
     147,     4,     4,   516,   147,   310,   335,   526,   147,   147,
     147,   341,   713,   147,   728,   734,   732,   148,   741,   718,
     150,   716,   727,   390,   834,   721,   147,   358,   719,   148,
     148,   382,   724,   147,   713,   722,   754,   387,   745,   752,
     757,   713,   406,   722,   716,   755,   725,   719,   722,   547,
     722,   536,   372,   725,   699,   848,   716,   864,   876,   361,
     732,   716,   716,   893,   716,   725,   722,    -1,    -1,   716,
     725,   725,   716,   725,    -1,    -1,   716,   716,   725,    -1,
     752,   725,    -1,   755,    -1,   725,   725,    -1,   752,    -1,
      -1,    -1,   752,    -1,    -1,   755,    -1,    -1,    -1,    -1,
     755,    -1,    -1,   755,    -1,    -1,    -1,    -1,   755,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   834,    -1,    -1,    -1,    -1,
      -1,    -1,   834,    -1,    -1,    -1,    -1,    -1,   847,    -1,
      -1,    -1,    -1,    -1,   834,   847,    -1,    -1,    -1,   834,
     834,    -1,   834,    -1,    -1,   864,    -1,   834,    -1,    -1,
     834,    -1,   864,    -1,   834,   834
  };

  const unsigned short int
  Dhcp4Parser::yystos_[] =
  {
       0,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,   162,   163,   164,     0,     5,     7,     9,
     147,   148,   149,   150,   165,   166,   167,   172,     7,   181,
       7,   187,     7,   201,     7,   277,     7,   358,     7,   372,
       7,   310,     7,   316,     7,   340,     7,   253,     7,   424,
     173,   168,   182,   188,   202,   278,   359,   373,   311,   317,
     341,   254,   425,   165,   174,   175,   147,   170,   171,    10,
     123,   132,   133,   134,   147,   180,   183,   184,   185,   451,
     453,   455,   457,    11,    20,    21,    22,    23,    24,    25,
      26,    43,    44,    45,    46,    47,    51,    52,    62,    75,
      76,    88,    91,    98,    99,   102,   180,   189,   190,   191,
     192,   193,   194,   195,   196,   197,   212,   214,   238,   247,
     261,   271,   300,   308,   334,   377,   379,   381,   403,   413,
     414,   422,    12,    13,    16,    19,   199,   200,   203,   205,
     208,   211,     7,    48,    49,    50,    63,    65,    66,    67,
      68,    69,    70,    71,    78,    79,    86,   180,   191,   192,
     193,   196,   279,   280,   281,   283,   285,   287,   289,   291,
     293,   295,   298,   299,   334,   352,   364,   366,   377,   379,
     381,   399,    64,   180,   334,   360,   361,   362,   364,    76,
      80,    81,    82,    83,    84,    85,    87,   180,   334,   374,
     375,   376,   377,   379,   381,   383,   385,   387,   389,   391,
     393,   395,   397,   308,    27,    53,    55,    56,    59,    60,
      61,   180,   228,   318,   319,   320,   321,   322,   323,   324,
     326,   328,   330,   331,   333,    54,    57,    58,   180,   228,
     322,   328,   342,   343,   344,   345,   346,   348,   349,   350,
     351,    89,    90,   180,   255,   256,   257,   259,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   180,   426,   427,   428,   429,   431,   433,   434,
     436,   437,   438,   441,   443,   444,   445,   446,   449,     6,
       3,     4,     8,     3,   186,   458,   452,   454,   456,     4,
       3,     8,   198,     4,     4,   378,   380,   382,   213,   215,
       4,     4,     4,     4,   272,   309,   335,   301,   239,   404,
     248,   262,     4,   415,   423,     3,     8,   204,   206,   209,
       4,     3,     8,   406,   284,   286,   288,   353,   365,   282,
     290,   292,     4,     4,   296,   294,   367,   400,     3,     8,
     363,     3,     8,   398,   386,   388,   392,   390,   396,   394,
     384,     8,     3,     8,   325,   229,     4,   329,   327,   332,
       4,     8,     3,   347,     4,     4,     8,     3,   258,   260,
       3,     8,     4,   430,   432,     4,   435,     4,     4,   439,
     442,     4,     4,     4,   447,   450,     3,     8,   165,   165,
     147,     4,     4,     4,     4,     4,   184,     4,   150,   150,
       4,     4,     4,     4,     4,   148,   148,   148,   148,     4,
       4,     4,     4,     4,     4,     4,     4,   148,     4,     4,
     190,     4,     4,     4,   150,   200,    77,   180,   228,   308,
     334,   377,   379,   381,   407,   408,   409,   410,   411,     4,
       4,     4,     4,     4,     4,     4,     4,   148,   150,     4,
       4,     4,     4,   280,     4,   361,     4,     4,     4,     4,
       4,     4,     4,     4,   376,     4,     4,   148,     4,     4,
       4,   150,   320,     4,   150,   150,   344,     4,     4,   256,
     150,     4,     4,   148,     4,   148,   148,     4,     4,   150,
     150,   150,     4,     4,   427,     4,     7,     7,   165,   165,
     165,     7,   147,   147,   147,     7,     7,     5,     5,     5,
       5,     5,     5,     5,     7,     7,     7,     5,   176,    14,
      15,   207,    17,    18,   210,   412,     8,     3,   147,   147,
     147,     5,   167,   169,   147,   147,   147,    72,    73,    74,
     297,   147,     5,     7,   147,   176,   147,   147,   147,   147,
     147,   147,   147,   147,   147,   147,   147,   147,   147,   147,
     165,   147,   147,   147,    15,   117,   440,   118,   119,   120,
     121,   122,   150,   448,   147,   165,   189,   124,   459,   460,
     461,   199,    27,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,   180,   216,   217,   218,   221,   223,   225,
     227,   228,   230,   231,   232,   233,   234,   236,   216,     7,
     273,   274,   275,     7,   312,   313,   314,     7,   336,   337,
     338,     7,   302,   303,   304,    80,    81,    82,    83,    85,
     240,   241,   242,   243,   244,   245,   246,   293,   405,     7,
     249,   250,   251,    92,    93,    94,    95,    96,    97,   263,
     264,   265,   266,   267,   268,   269,   270,   100,   101,   416,
     417,   418,   420,   426,   177,     4,   409,     7,   354,   355,
     356,     7,   368,   369,   370,    87,   401,     8,   462,     3,
       8,     8,   219,   222,   224,   226,     4,     4,     4,     4,
       4,   235,   237,     3,     8,     8,   276,     6,     3,   315,
       6,     3,   339,     6,     3,   305,     6,     3,     3,     6,
       3,     6,   252,     6,     3,     4,     4,     4,     4,     4,
       4,     3,     8,   419,   421,     3,     8,     8,   147,   178,
     179,   147,   357,     6,     3,   371,     6,     3,   402,     8,
       4,   460,     4,     4,     4,     4,   148,   150,   148,   150,
     148,     4,     4,   217,   279,   275,   318,   314,   342,   338,
     180,   191,   192,   193,   196,   228,   271,   289,   293,   295,
     306,   307,   334,   377,   379,   381,   399,   304,   241,   293,
     255,   251,   148,   148,   148,   148,   148,   148,   264,     4,
       4,   417,     6,     3,   360,   356,   374,   370,     4,     5,
      28,    29,    30,    31,    32,   220,   147,   147,   147,   147,
     147,     8,     8,     8,     3,     8,     8,   147,   147,   147,
       8,     8,   147,     7,   463,   464,   307,   465,     3,     6,
     125,   127,   128,   180,   228,   466,   467,   468,   469,   471,
     464,   472,     4,   470,     3,     8,     4,   148,     4,   467,
       5,   147,     7,   473,   474,   475,     3,     6,   126,   129,
     130,   131,   476,   477,   478,   480,   481,   482,   474,   479,
       4,     4,     4,     3,     8,     4,   150,   148,   148,   477,
     147
  };

  const unsigned short int
  Dhcp4Parser::yyr1_[] =
  {
       0,   151,   153,   152,   154,   152,   155,   152,   156,   152,
     157,   152,   158,   152,   159,   152,   160,   152,   161,   152,
     162,   152,   163,   152,   164,   152,   165,   165,   165,   165,
     165,   165,   165,   166,   168,   167,   169,   170,   170,   171,
     171,   173,   172,   174,   174,   175,   175,   177,   176,   178,
     178,   179,   179,   180,   182,   181,   183,   183,   184,   184,
     184,   184,   184,   184,   186,   185,   188,   187,   189,   189,
     190,   190,   190,   190,   190,   190,   190,   190,   190,   190,
     190,   190,   190,   190,   190,   190,   190,   190,   190,   190,
     190,   190,   190,   190,   191,   192,   193,   194,   195,   196,
     198,   197,   199,   199,   200,   200,   200,   200,   202,   201,
     204,   203,   206,   205,   207,   207,   209,   208,   210,   210,
     211,   213,   212,   215,   214,   216,   216,   217,   217,   217,
     217,   217,   217,   217,   217,   217,   217,   217,   217,   217,
     219,   218,   220,   220,   220,   220,   220,   222,   221,   224,
     223,   226,   225,   227,   229,   228,   230,   231,   232,   233,
     235,   234,   237,   236,   239,   238,   240,   240,   241,   241,
     241,   241,   241,   242,   243,   244,   245,   246,   248,   247,
     249,   249,   250,   250,   252,   251,   254,   253,   255,   255,
     255,   256,   256,   258,   257,   260,   259,   262,   261,   263,
     263,   264,   264,   264,   264,   264,   264,   265,   266,   267,
     268,   269,   270,   272,   271,   273,   273,   274,   274,   276,
     275,   278,   277,   279,   279,   280,   280,   280,   280,   280,
     280,   280,   280,   280,   280,   280,   280,   280,   280,   280,
     280,   280,   280,   280,   280,   280,   280,   280,   282,   281,
     284,   283,   286,   285,   288,   287,   290,   289,   292,   291,
     294,   293,   296,   295,   297,   297,   297,   298,   299,   301,
     300,   302,   302,   303,   303,   305,   304,   306,   306,   307,
     307,   307,   307,   307,   307,   307,   307,   307,   307,   307,
     307,   307,   307,   307,   309,   308,   311,   310,   312,   312,
     313,   313,   315,   314,   317,   316,   318,   318,   319,   319,
     320,   320,   320,   320,   320,   320,   320,   320,   321,   322,
     323,   325,   324,   327,   326,   329,   328,   330,   332,   331,
     333,   335,   334,   336,   336,   337,   337,   339,   338,   341,
     340,   342,   342,   343,   343,   344,   344,   344,   344,   344,
     344,   344,   345,   347,   346,   348,   349,   350,   351,   353,
     352,   354,   354,   355,   355,   357,   356,   359,   358,   360,
     360,   361,   361,   361,   361,   363,   362,   365,   364,   367,
     366,   368,   368,   369,   369,   371,   370,   373,   372,   374,
     374,   375,   375,   376,   376,   376,   376,   376,   376,   376,
     376,   376,   376,   376,   376,   376,   378,   377,   380,   379,
     382,   381,   384,   383,   386,   385,   388,   387,   390,   389,
     392,   391,   394,   393,   396,   395,   398,   397,   400,   399,
     402,   401,   404,   403,   405,   405,   406,   293,   407,   407,
     408,   408,   409,   409,   409,   409,   409,   409,   409,   409,
     410,   412,   411,   413,   415,   414,   416,   416,   417,   417,
     419,   418,   421,   420,   423,   422,   425,   424,   426,   426,
     427,   427,   427,   427,   427,   427,   427,   427,   427,   427,
     427,   427,   427,   427,   427,   428,   430,   429,   432,   431,
     433,   435,   434,   436,   437,   439,   438,   440,   440,   442,
     441,   443,   444,   445,   447,   446,   448,   448,   448,   448,
     448,   450,   449,   452,   451,   454,   453,   456,   455,   458,
     457,   459,   459,   460,   462,   461,   463,   463,   465,   464,
     466,   466,   467,   467,   467,   467,   467,   468,   470,   469,
     472,   471,   473,   473,   475,   474,   476,   476,   477,   477,
     477,   477,   479,   478,   480,   481,   482
  };

  const unsigned char
  Dhcp4Parser::yyr2_[] =
  {
       0,     2,     0,     3,     0,     3,     0,     3,     0,     3,
       0,     3,     0,     3,     0,     3,     0,     3,     0,     3,
       0,     3,     0,     3,     0,     3,     1,     1,     1,     1,
       1,     1,     1,     1,     0,     4,     1,     0,     1,     3,
       5,     0,     4,     0,     1,     1,     3,     0,     4,     0,
       1,     1,     3,     2,     0,     4,     1,     3,     1,     1,
       1,     1,     1,     1,     0,     6,     0,     4,     1,     3,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     3,     3,     3,     3,     3,     3,
       0,     6,     1,     3,     1,     1,     1,     1,     0,     4,
       0,     4,     0,     4,     1,     1,     0,     4,     1,     1,
       3,     0,     6,     0,     6,     1,     3,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       0,     4,     1,     1,     1,     1,     1,     0,     4,     0,
       4,     0,     4,     3,     0,     4,     3,     3,     3,     3,
       0,     4,     0,     4,     0,     6,     1,     3,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     0,     6,
       0,     1,     1,     3,     0,     4,     0,     4,     1,     3,
       1,     1,     1,     0,     4,     0,     4,     0,     6,     1,
       3,     1,     1,     1,     1,     1,     1,     3,     3,     3,
       3,     3,     3,     0,     6,     0,     1,     1,     3,     0,
       4,     0,     4,     1,     3,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     0,     4,
       0,     4,     0,     4,     0,     4,     0,     4,     0,     4,
       0,     4,     0,     4,     1,     1,     1,     3,     3,     0,
       6,     0,     1,     1,     3,     0,     4,     1,     3,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     0,     6,     0,     4,     0,     1,
       1,     3,     0,     4,     0,     4,     0,     1,     1,     3,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     3,
       1,     0,     4,     0,     4,     0,     4,     1,     0,     4,
       3,     0,     6,     0,     1,     1,     3,     0,     4,     0,
       4,     0,     1,     1,     3,     1,     1,     1,     1,     1,
       1,     1,     1,     0,     4,     1,     1,     3,     3,     0,
       6,     0,     1,     1,     3,     0,     4,     0,     4,     1,
       3,     1,     1,     1,     1,     0,     4,     0,     4,     0,
       6,     0,     1,     1,     3,     0,     4,     0,     4,     0,
       1,     1,     3,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     0,     4,     0,     4,
       0,     4,     0,     4,     0,     4,     0,     4,     0,     4,
       0,     4,     0,     4,     0,     4,     0,     4,     0,     6,
       0,     4,     0,     6,     1,     3,     0,     4,     0,     1,
       1,     3,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     0,     4,     3,     0,     6,     1,     3,     1,     1,
       0,     4,     0,     4,     0,     6,     0,     4,     1,     3,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     3,     0,     4,     0,     4,
       3,     0,     4,     3,     3,     0,     4,     1,     1,     0,
       4,     3,     3,     3,     0,     4,     1,     1,     1,     1,
       1,     0,     4,     0,     4,     0,     4,     0,     4,     0,
       6,     1,     3,     1,     0,     6,     1,     3,     0,     4,
       1,     3,     1,     1,     1,     1,     1,     3,     0,     4,
       0,     6,     1,     3,     0,     4,     1,     3,     1,     1,
       1,     1,     0,     4,     3,     3,     3
  };



  // YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
  // First, the terminals, then, starting at \a yyntokens_, nonterminals.
  const char*
  const Dhcp4Parser::yytname_[] =
  {
  "\"end of file\"", "error", "$undefined", "\",\"", "\":\"", "\"[\"",
  "\"]\"", "\"{\"", "\"}\"", "\"null\"", "\"Dhcp4\"",
  "\"interfaces-config\"", "\"interfaces\"", "\"dhcp-socket-type\"",
  "\"raw\"", "\"udp\"", "\"outbound-interface\"", "\"same-as-inbound\"",
  "\"use-routing\"", "\"re-detect\"", "\"echo-client-id\"",
  "\"match-client-id\"", "\"next-server\"", "\"server-hostname\"",
  "\"boot-file-name\"", "\"lease-database\"", "\"hosts-database\"",
  "\"type\"", "\"memfile\"", "\"mysql\"", "\"postgresql\"", "\"cql\"",
  "\"radius\"", "\"user\"", "\"password\"", "\"host\"", "\"port\"",
  "\"persist\"", "\"lfc-interval\"", "\"readonly\"", "\"connect-timeout\"",
  "\"contact-points\"", "\"keyspace\"", "\"valid-lifetime\"",
  "\"renew-timer\"", "\"rebind-timer\"", "\"decline-probation-period\"",
  "\"subnet4\"", "\"4o6-interface\"", "\"4o6-interface-id\"",
  "\"4o6-subnet\"", "\"option-def\"", "\"option-data\"", "\"name\"",
  "\"data\"", "\"code\"", "\"space\"", "\"csv-format\"", "\"always-send\"",
  "\"record-types\"", "\"encapsulate\"", "\"array\"",
  "\"shared-networks\"", "\"pools\"", "\"pool\"", "\"user-context\"",
  "\"subnet\"", "\"interface\"", "\"interface-id\"", "\"id\"",
  "\"rapid-commit\"", "\"reservation-mode\"", "\"disabled\"",
  "\"out-of-pool\"", "\"all\"", "\"host-reservation-identifiers\"",
  "\"client-classes\"", "\"test\"", "\"client-class\"", "\"reservations\"",
  "\"duid\"", "\"hw-address\"", "\"circuit-id\"", "\"client-id\"",
  "\"hostname\"", "\"flex-id\"", "\"relay\"", "\"ip-address\"",
  "\"hooks-libraries\"", "\"library\"", "\"parameters\"",
  "\"expired-leases-processing\"", "\"reclaim-timer-wait-time\"",
  "\"flush-reclaimed-timer-wait-time\"", "\"hold-reclaimed-time\"",
  "\"max-reclaim-leases\"", "\"max-reclaim-time\"",
  "\"unwarned-reclaim-cycles\"", "\"dhcp4o6-port\"", "\"control-socket\"",
  "\"socket-type\"", "\"socket-name\"", "\"dhcp-ddns\"",
  "\"enable-updates\"", "\"qualifying-suffix\"", "\"server-ip\"",
  "\"server-port\"", "\"sender-ip\"", "\"sender-port\"",
  "\"max-queue-size\"", "\"ncr-protocol\"", "\"ncr-format\"",
  "\"always-include-fqdn\"", "\"override-no-update\"",
  "\"override-client-update\"", "\"replace-client-name\"",
  "\"generated-prefix\"", "\"tcp\"", "\"JSON\"", "\"when-present\"",
  "\"never\"", "\"always\"", "\"when-not-present\"", "\"Logging\"",
  "\"loggers\"", "\"output_options\"", "\"output\"", "\"debuglevel\"",
  "\"severity\"", "\"flush\"", "\"maxsize\"", "\"maxver\"", "\"Dhcp6\"",
  "\"DhcpDdns\"", "\"Control-agent\"", "TOPLEVEL_JSON", "TOPLEVEL_DHCP4",
  "SUB_DHCP4", "SUB_INTERFACES4", "SUB_SUBNET4", "SUB_POOL4",
  "SUB_RESERVATION", "SUB_OPTION_DEFS", "SUB_OPTION_DEF",
  "SUB_OPTION_DATA", "SUB_HOOKS_LIBRARY", "SUB_DHCP_DDNS",
  "\"constant string\"", "\"integer\"", "\"floating point\"",
  "\"boolean\"", "$accept", "start", "$@1", "$@2", "$@3", "$@4", "$@5",
  "$@6", "$@7", "$@8", "$@9", "$@10", "$@11", "$@12", "value", "sub_json",
  "map2", "$@13", "map_value", "map_content", "not_empty_map",
  "list_generic", "$@14", "list_content", "not_empty_list", "list_strings",
  "$@15", "list_strings_content", "not_empty_list_strings",
  "unknown_map_entry", "syntax_map", "$@16", "global_objects",
  "global_object", "dhcp4_object", "$@17", "sub_dhcp4", "$@18",
  "global_params", "global_param", "valid_lifetime", "renew_timer",
  "rebind_timer", "decline_probation_period", "echo_client_id",
  "match_client_id", "interfaces_config", "$@19",
  "interfaces_config_params", "interfaces_config_param", "sub_interfaces4",
  "$@20", "interfaces_list", "$@21", "dhcp_socket_type", "$@22",
  "socket_type", "outbound_interface", "$@23", "outbound_interface_value",
  "re_detect", "lease_database", "$@24", "hosts_database", "$@25",
  "database_map_params", "database_map_param", "database_type", "$@26",
  "db_type", "user", "$@27", "password", "$@28", "host", "$@29", "port",
  "name", "$@30", "persist", "lfc_interval", "readonly", "connect_timeout",
  "contact_points", "$@31", "keyspace", "$@32",
  "host_reservation_identifiers", "$@33",
  "host_reservation_identifiers_list", "host_reservation_identifier",
  "duid_id", "hw_address_id", "circuit_id", "client_id", "flex_id",
  "hooks_libraries", "$@34", "hooks_libraries_list",
  "not_empty_hooks_libraries_list", "hooks_library", "$@35",
  "sub_hooks_library", "$@36", "hooks_params", "hooks_param", "library",
  "$@37", "parameters", "$@38", "expired_leases_processing", "$@39",
  "expired_leases_params", "expired_leases_param",
  "reclaim_timer_wait_time", "flush_reclaimed_timer_wait_time",
  "hold_reclaimed_time", "max_reclaim_leases", "max_reclaim_time",
  "unwarned_reclaim_cycles", "subnet4_list", "$@40",
  "subnet4_list_content", "not_empty_subnet4_list", "subnet4", "$@41",
  "sub_subnet4", "$@42", "subnet4_params", "subnet4_param", "subnet",
  "$@43", "subnet_4o6_interface", "$@44", "subnet_4o6_interface_id",
  "$@45", "subnet_4o6_subnet", "$@46", "interface", "$@47", "interface_id",
  "$@48", "client_class", "$@49", "reservation_mode", "$@50", "hr_mode",
  "id", "rapid_commit", "shared_networks", "$@51",
  "shared_networks_content", "shared_networks_list", "shared_network",
  "$@52", "shared_network_params", "shared_network_param",
  "option_def_list", "$@53", "sub_option_def_list", "$@54",
  "option_def_list_content", "not_empty_option_def_list",
  "option_def_entry", "$@55", "sub_option_def", "$@56",
  "option_def_params", "not_empty_option_def_params", "option_def_param",
  "option_def_name", "code", "option_def_code", "option_def_type", "$@57",
  "option_def_record_types", "$@58", "space", "$@59", "option_def_space",
  "option_def_encapsulate", "$@60", "option_def_array", "option_data_list",
  "$@61", "option_data_list_content", "not_empty_option_data_list",
  "option_data_entry", "$@62", "sub_option_data", "$@63",
  "option_data_params", "not_empty_option_data_params",
  "option_data_param", "option_data_name", "option_data_data", "$@64",
  "option_data_code", "option_data_space", "option_data_csv_format",
  "option_data_always_send", "pools_list", "$@65", "pools_list_content",
  "not_empty_pools_list", "pool_list_entry", "$@66", "sub_pool4", "$@67",
  "pool_params", "pool_param", "pool_entry", "$@68", "user_context",
  "$@69", "reservations", "$@70", "reservations_list",
  "not_empty_reservations_list", "reservation", "$@71", "sub_reservation",
  "$@72", "reservation_params", "not_empty_reservation_params",
  "reservation_param", "next_server", "$@73", "server_hostname", "$@74",
  "boot_file_name", "$@75", "ip_address", "$@76", "duid", "$@77",
  "hw_address", "$@78", "client_id_value", "$@79", "circuit_id_value",
  "$@80", "flex_id_value", "$@81", "hostname", "$@82",
  "reservation_client_classes", "$@83", "relay", "$@84", "relay_map",
  "$@85", "client_classes", "$@86", "client_classes_list", "$@87",
  "client_class_params", "not_empty_client_class_params",
  "client_class_param", "client_class_name", "client_class_test", "$@88",
  "dhcp4o6_port", "control_socket", "$@89", "control_socket_params",
  "control_socket_param", "control_socket_type", "$@90",
  "control_socket_name", "$@91", "dhcp_ddns", "$@92", "sub_dhcp_ddns",
  "$@93", "dhcp_ddns_params", "dhcp_ddns_param", "enable_updates",
  "qualifying_suffix", "$@94", "server_ip", "$@95", "server_port",
  "sender_ip", "$@96", "sender_port", "max_queue_size", "ncr_protocol",
  "$@97", "ncr_protocol_value", "ncr_format", "$@98",
  "always_include_fqdn", "override_no_update", "override_client_update",
  "replace_client_name", "$@99", "replace_client_name_value",
  "generated_prefix", "$@100", "dhcp6_json_object", "$@101",
  "dhcpddns_json_object", "$@102", "control_agent_json_object", "$@103",
  "logging_object", "$@104", "logging_params", "logging_param", "loggers",
  "$@105", "loggers_entries", "logger_entry", "$@106", "logger_params",
  "logger_param", "debuglevel", "severity", "$@107", "output_options_list",
  "$@108", "output_options_list_content", "output_entry", "$@109",
  "output_params_list", "output_params", "output", "$@110", "flush",
  "maxsize", "maxver", YY_NULLPTR
  };

#if PARSER4_DEBUG
  const unsigned short int
  Dhcp4Parser::yyrline_[] =
  {
       0,   234,   234,   234,   235,   235,   236,   236,   237,   237,
     238,   238,   239,   239,   240,   240,   241,   241,   242,   242,
     243,   243,   244,   244,   245,   245,   253,   254,   255,   256,
     257,   258,   259,   262,   267,   267,   278,   281,   282,   285,
     289,   296,   296,   303,   304,   307,   311,   318,   318,   325,
     326,   329,   333,   344,   354,   354,   370,   371,   375,   376,
     377,   378,   379,   380,   383,   383,   398,   398,   407,   408,
     413,   414,   415,   416,   417,   418,   419,   420,   421,   422,
     423,   424,   425,   426,   427,   428,   429,   430,   431,   432,
     433,   434,   435,   436,   439,   444,   449,   454,   459,   464,
     470,   470,   481,   482,   485,   486,   487,   488,   491,   491,
     500,   500,   510,   510,   517,   518,   521,   521,   528,   530,
     534,   540,   540,   552,   552,   564,   565,   568,   569,   570,
     571,   572,   573,   574,   575,   576,   577,   578,   579,   580,
     583,   583,   590,   591,   592,   593,   594,   597,   597,   605,
     605,   613,   613,   621,   626,   626,   634,   639,   644,   649,
     654,   654,   662,   662,   671,   671,   681,   682,   685,   686,
     687,   688,   689,   692,   697,   702,   707,   712,   717,   717,
     727,   728,   731,   732,   735,   735,   745,   745,   755,   756,
     757,   760,   761,   764,   764,   772,   772,   780,   780,   791,
     792,   795,   796,   797,   798,   799,   800,   803,   808,   813,
     818,   823,   828,   836,   836,   849,   850,   853,   854,   861,
     861,   887,   887,   898,   899,   903,   904,   905,   906,   907,
     908,   909,   910,   911,   912,   913,   914,   915,   916,   917,
     918,   919,   920,   921,   922,   923,   924,   925,   928,   928,
     936,   936,   944,   944,   952,   952,   960,   960,   968,   968,
     976,   976,   984,   984,   991,   992,   993,   996,  1001,  1008,
    1008,  1019,  1020,  1024,  1025,  1028,  1028,  1036,  1037,  1040,
    1041,  1042,  1043,  1044,  1045,  1046,  1047,  1048,  1049,  1050,
    1051,  1052,  1053,  1054,  1061,  1061,  1074,  1074,  1083,  1084,
    1087,  1088,  1093,  1093,  1108,  1108,  1122,  1123,  1126,  1127,
    1130,  1131,  1132,  1133,  1134,  1135,  1136,  1137,  1140,  1142,
    1147,  1149,  1149,  1157,  1157,  1165,  1165,  1173,  1175,  1175,
    1183,  1192,  1192,  1204,  1205,  1210,  1211,  1216,  1216,  1228,
    1228,  1240,  1241,  1246,  1247,  1252,  1253,  1254,  1255,  1256,
    1257,  1258,  1261,  1263,  1263,  1271,  1273,  1275,  1280,  1288,
    1288,  1300,  1301,  1304,  1305,  1308,  1308,  1318,  1318,  1328,
    1329,  1332,  1333,  1334,  1335,  1338,  1338,  1346,  1346,  1356,
    1356,  1366,  1367,  1370,  1371,  1374,  1374,  1383,  1383,  1392,
    1393,  1396,  1397,  1401,  1402,  1403,  1404,  1405,  1406,  1407,
    1408,  1409,  1410,  1411,  1412,  1413,  1416,  1416,  1424,  1424,
    1432,  1432,  1440,  1440,  1448,  1448,  1456,  1456,  1464,  1464,
    1472,  1472,  1480,  1480,  1488,  1488,  1496,  1496,  1509,  1509,
    1519,  1519,  1530,  1530,  1540,  1541,  1544,  1544,  1554,  1555,
    1558,  1559,  1562,  1563,  1564,  1565,  1566,  1567,  1568,  1569,
    1572,  1574,  1574,  1586,  1593,  1593,  1603,  1604,  1607,  1608,
    1611,  1611,  1619,  1619,  1629,  1629,  1641,  1641,  1651,  1652,
    1655,  1656,  1657,  1658,  1659,  1660,  1661,  1662,  1663,  1664,
    1665,  1666,  1667,  1668,  1669,  1672,  1677,  1677,  1685,  1685,
    1693,  1698,  1698,  1706,  1711,  1716,  1716,  1724,  1725,  1728,
    1728,  1736,  1741,  1746,  1751,  1751,  1759,  1762,  1765,  1768,
    1771,  1777,  1777,  1787,  1787,  1794,  1794,  1801,  1801,  1813,
    1813,  1826,  1827,  1831,  1835,  1835,  1847,  1848,  1852,  1852,
    1860,  1861,  1864,  1865,  1866,  1867,  1868,  1871,  1876,  1876,
    1884,  1884,  1894,  1895,  1898,  1898,  1906,  1907,  1910,  1911,
    1912,  1913,  1916,  1916,  1924,  1929,  1934
  };

  // Print the state stack on the debug stream.
  void
  Dhcp4Parser::yystack_print_ ()
  {
    *yycdebug_ << "Stack now";
    for (stack_type::const_iterator
           i = yystack_.begin (),
           i_end = yystack_.end ();
         i != i_end; ++i)
      *yycdebug_ << ' ' << i->state;
    *yycdebug_ << std::endl;
  }

  // Report on the debug stream that the rule \a yyrule is going to be reduced.
  void
  Dhcp4Parser::yy_reduce_print_ (int yyrule)
  {
    unsigned int yylno = yyrline_[yyrule];
    int yynrhs = yyr2_[yyrule];
    // Print the symbols being reduced, and their result.
    *yycdebug_ << "Reducing stack by rule " << yyrule - 1
               << " (line " << yylno << "):" << std::endl;
    // The symbols being reduced.
    for (int yyi = 0; yyi < yynrhs; yyi++)
      YY_SYMBOL_PRINT ("   $" << yyi + 1 << " =",
                       yystack_[(yynrhs) - (yyi + 1)]);
  }
#endif // PARSER4_DEBUG


#line 14 "dhcp4_parser.yy" // lalr1.cc:1167
} } // isc::dhcp
#line 4386 "dhcp4_parser.cc" // lalr1.cc:1167
#line 1939 "dhcp4_parser.yy" // lalr1.cc:1168


void
isc::dhcp::Dhcp4Parser::error(const location_type& loc,
                              const std::string& what)
{
    ctx.error(loc, what);
}
