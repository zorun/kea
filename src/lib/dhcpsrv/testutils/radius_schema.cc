// Copyright (C) 2015-2016 Internet Systems Consortium, Inc. ("ISC")
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <config.h>
#include <string>
#include <dhcpsrv/testutils/radius_schema.h>
#include <gtest/gtest.h>

#include <fstream>
#include <sstream>
#include <stdlib.h>

using namespace std;

namespace isc {
namespace dhcp {
namespace test {

const char* RADIUS_AUTHORIZE_FILE = "/tmp/freeradius-authorize";
const char* RADIUS_VALID_TYPE = "type=radius";
const char* VALID_RADIUS_REALM = "name=kea.isc.org";
const char* RADIUS_REALM = "kea.isc.org";

string
validRadiusConnectionString() {
    return (connectionString(RADIUS_VALID_TYPE, VALID_RADIUS_REALM, VALID_HOST,
                             VALID_USER, VALID_PASSWORD));
}

void freeradiusReload() {
    std::string line;
    ifstream myfile(RADIUS_AUTHORIZE_FILE);
    while(std::getline(myfile, line)) {
        std::cerr << line << "\n";
    }
    std::ostringstream cmd;
    cmd << "sudo service freeradius stop && sudo service freeradius start";
    int retval = ::system(cmd.str().c_str());

    ASSERT_EQ(0, retval) << "failed:" << cmd.str();
    myfile.close();
}

void destroyRadiusSchema(bool show_err) {
}

void createRadiusSchema(bool show_err) {
    std::ostringstream cmd;
    cmd << "sudo sh -c 'echo >" << RADIUS_AUTHORIZE_FILE << "'";
    int retval = ::system(cmd.str().c_str());
    ASSERT_EQ(0, retval) << "failed:" << cmd.str();

    freeradiusReload();
}

void freeradiusAdd(const HostPtr& host) {
    ofstream myfile;
    myfile.open(RADIUS_AUTHORIZE_FILE, std::ios_base::app);
    myfile << "# " << host->getIdentifierAsText() << "\n";

    if( host->getHWAddress()) {
        myfile << host->getHWAddress()->toText(false) ;
        if( RADIUS_REALM ) {
            myfile << "@" << RADIUS_REALM;
        }
    } else if( host->getDuid()) {
        myfile << host->getDuid()->toText() ;
        if( RADIUS_REALM ) {
            myfile << "@" << RADIUS_REALM;
        }
    } else {
        std::stringstream tmp;
        tmp << std::hex;
        bool delim = false;
        for (std::vector<uint8_t>::const_iterator it = host->getIdentifier().begin();
             it != host->getIdentifier().end(); ++it) {
            if (delim) {
                tmp << ":";
            }
            tmp << std::setw(2) << std::setfill('0') << static_cast<unsigned int>(*it);
            delim = true;
        }
        myfile << tmp.str();
        if( RADIUS_REALM ) {
            myfile << "@" << RADIUS_REALM;
        }
    }

    myfile << " Auth-Type := Accept\n";
    myfile << "        Service-Type = Framed-User";

    if(! host->getIPv4Reservation().isV4Zero()) {
        myfile << ",\n        Framed-IP-Address = " << host->getIPv4Reservation();
    }

    IPv6ResrvRange resrv = host->getIPv6Reservations();
    if (std::distance(resrv.first, resrv.second) > 0) {
        int na = 0;
        int pd = 0;
        for (IPv6ResrvIterator resv = resrv.first; resv != resrv.second;
             ++resv) {

            if(resv->first == IPv6Resrv::TYPE_NA) {
                myfile << ",\n        Framed-IPv6-Address:" << na++ << " = " << resv->second.getPrefix();
            } else {
                myfile << ",\n        Delegated-IPv6-Prefix:" << pd++ << " = " << resv->second.toText();
            }
        }
    }
    myfile << "\n";
    myfile.close();
    freeradiusReload();
}

bool freeradiusDel(const SubnetID& subnet_id, const asiolink::IOAddress& addr) {
}

bool freeradiusDel4(const SubnetID& subnet_id,
                  const Host::IdentifierType& identifier_type,
                  const uint8_t* identifier_begin, const size_t identifier_len) {
}

bool freeradiusDel6(const SubnetID& subnet_id,
                  const Host::IdentifierType& identifier_type,
                  const uint8_t* identifier_begin, const size_t identifier_len) {
}


};
};
};
