// Copyright (C) 2015-2016 Internet Systems Consortium, Inc. ("ISC")
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef TEST_RADIUS_SCHEMA_H
#define TEST_RADIUS_SCHEMA_H

#include <config.h>
#include <dhcpsrv/host.h>
#include <dhcpsrv/testutils/schema.h>
#include <string>

namespace isc {
namespace dhcp {
namespace test {

extern const char* RADIUS_VALID_TYPE;

/// Return valid connection string
///
/// @return valid Radius connection string.
std::string validRadiusConnectionString();

/// @brief Clear everything from the database
///
/// Submits the current schema drop script:
///
///  <TEST_ADMIN_SCRIPTS_DIR>/mysql/dhcpdb_drop.mysql
///
/// to the unit test Radius database. If the script fails, the invoking test
/// will fail. The output of stderr is suppressed unless the parameter,
/// show_err is true.
///
/// @param show_err flag which governs whether or not stderr is suppressed.
void destroyRadiusSchema(bool show_err = false);

/// @brief Create the Radius Schema
///
/// Submits the current schema creation script:
///
///  <TEST_ADMIN_SCRIPTS_DIR>/mysql/dhcpdb_create.mysql
///
/// to the unit test Radius database. If the script fails, the invoking test
/// will fail. The output of stderr is suppressed unless the parameter,
/// show_err is true.
///
/// @param show_err flag which governs whether or not stderr is suppressed.
void createRadiusSchema(bool show_err = false);

void freeradiusAdd(const HostPtr& host);

bool freeradiusDel(const SubnetID& subnet_id, const asiolink::IOAddress& addr);

bool freeradiusDel4(const SubnetID& subnet_id,
          const Host::IdentifierType& identifier_type,
          const uint8_t* identifier_begin, const size_t identifier_len);

bool freeradiusDel6(const SubnetID& subnet_id,
                  const Host::IdentifierType& identifier_type,
                  const uint8_t* identifier_begin, const size_t identifier_len);


};
};
};

#endif
