// Copyright (C) 2015-2017 Internet Systems Consortium, Inc. ("ISC")
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <config.h>

#include <asiolink/io_address.h>
#include <dhcpsrv/tests/test_utils.h>
#include <exceptions/exceptions.h>
#include <dhcpsrv/host.h>
#include <dhcpsrv/radius_host_data_source.h>
#include <dhcpsrv/tests/generic_host_data_source_unittest.h>
#include <dhcpsrv/testutils/radius_schema.h>
#include <dhcpsrv/host_data_source_factory.h>

#include <gtest/gtest.h>

#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <utility>

using namespace isc;
using namespace isc::asiolink;
using namespace isc::dhcp;
using namespace isc::dhcp::test;
using namespace std;

namespace {

class RadiusHostDataSourceTest : public GenericHostDataSourceTest {
public:
    /// @brief Constructor
    ///
    /// Deletes everything from the database and opens it.
    RadiusHostDataSourceTest() {

        destroyRadiusSchema();
        createRadiusSchema();

        // Connect to the database
        try {
            HostDataSourceFactory::create(validRadiusConnectionString());
        } catch (...) {
            std::cerr << "*** ERROR: unable to open database. The test\n"
                         "*** environment is broken and must be fixed before\n"
                         "*** the radius tests will run correctly.\n"
                         "*** The reason for the problem is described in the\n"
                         "*** accompanying exception output.\n";
            throw;
        }

        hdsptr_ = HostDataSourceFactory::getHostDataSourcePtr();
    }

    /// @brief Destructor
    ///
    /// Rolls back all pending transactions.  The deletion of myhdsptr_ will close
    /// the database.  Then reopen it and delete everything created by the test.
    virtual ~RadiusHostDataSourceTest() {
        try {
            hdsptr_->rollback();
        } catch (...) {
            // Rollback may fail if backend is in read only mode. That's ok.
        }
        HostDataSourceFactory::destroy();
        hdsptr_.reset();
        destroyRadiusSchema();
    }

    /// @brief Reopen the database
    ///
    /// Closes the database and re-open it.  Anything committed should be
    /// visible.
    ///
    /// Parameter is ignored for Radius backend as the v4 and v6 leases share
    /// the same database.
    void reopen(Universe) {
        HostDataSourceFactory::destroy();
        HostDataSourceFactory::create(validRadiusConnectionString());
        hdsptr_ = HostDataSourceFactory::getHostDataSourcePtr();
    }

    /// fake adding host
    void add(const HostPtr& host) const {
        freeradiusAdd(host);
    }

    bool del(const SubnetID& subnet_id, const asiolink::IOAddress& addr) {
        return freeradiusDel(subnet_id, addr);
    }

    bool del4(const SubnetID& subnet_id,
                      const Host::IdentifierType& identifier_type,
                      const uint8_t* identifier_begin, const size_t identifier_len) {
        return freeradiusDel4(subnet_id, identifier_type, identifier_begin, identifier_len);
    }

    bool del6(const SubnetID& subnet_id,
                      const Host::IdentifierType& identifier_type,
                      const uint8_t* identifier_begin, const size_t identifier_len) {
        return freeradiusDel6(subnet_id, identifier_type, identifier_begin, identifier_len);
    }


    HostPtr initializeHost4(const std::string& address, const Host::IdentifierType& id) {
        std::vector<uint8_t> ident;
        if (id == Host::IDENT_HWADDR) {
            ident = generateHWAddr();

        } else {
            ident = generateIdentifier();
        }

        // SubnetID are ignored with radius backend
        static SubnetID subnet4 = 0;
        static SubnetID subnet6 = 0;

        IOAddress addr(address);
        HostPtr host(new Host(&ident[0], ident.size(), id, subnet4, subnet6, addr));

        return (host);
    }

    HostPtr initializeHost6(std::string address,
                            Host::IdentifierType identifier,
                            bool prefix,
                            bool new_identifier) {
        std::vector<uint8_t> ident;
        switch (identifier) {
        case Host::IDENT_HWADDR:
            ident = generateHWAddr(new_identifier);
            break;
        case Host::IDENT_DUID:
            ident = generateIdentifier(new_identifier);
            break;
        default:
            ADD_FAILURE() << "Unknown IdType: " << identifier;
            return HostPtr();
        }

        // SubnetID are ignored with radius backend
        static SubnetID subnet4 = 0;
        static SubnetID subnet6 = 0;

        HostPtr host(new Host(&ident[0], ident.size(), identifier, subnet4,
                              subnet6, IOAddress("0.0.0.0")));

        if (!prefix) {
            // Create IPv6 reservation (for an address)
            IPv6Resrv resv(IPv6Resrv::TYPE_NA, IOAddress(address), 128);
            host->addReservation(resv);
        } else {
            // Create IPv6 reservation for a /64 prefix
            IPv6Resrv resv(IPv6Resrv::TYPE_PD, IOAddress(address), 64);
            host->addReservation(resv);
        }
        return (host);
    }




    void compareHosts(const ConstHostPtr& host1, const ConstHostPtr& host2) {

        // Let's compare HW addresses and expect match.
        compareHwaddrs(host1, host2, true);

        // Now compare DUIDs
        compareDuids(host1, host2, true);

        // Now check that the identifiers returned as vectors are the same
        EXPECT_EQ(host1->getIdentifierType(), host2->getIdentifierType());
        EXPECT_TRUE(host1->getIdentifier() == host2->getIdentifier());

        // Check host parameters
        //EXPECT_EQ(host1->getIPv4SubnetID(), host2->getIPv4SubnetID());
        //EXPECT_EQ(host1->getIPv6SubnetID(), host2->getIPv6SubnetID());
        EXPECT_EQ(host1->getIPv4Reservation(), host2->getIPv4Reservation());
        EXPECT_EQ(host1->getHostname(), host2->getHostname());
        EXPECT_EQ(host1->getNextServer(), host2->getNextServer());
        EXPECT_EQ(host1->getServerHostname(), host2->getServerHostname());
        EXPECT_EQ(host1->getBootFileName(), host2->getBootFileName());

        // Compare IPv6 reservations
        compareReservations6(host1->getIPv6Reservations(),
                             host2->getIPv6Reservations());

        // Compare client classification details
        compareClientClasses(host1->getClientClasses4(),
                             host2->getClientClasses4());

        compareClientClasses(host1->getClientClasses6(),
                             host2->getClientClasses6());

        // Compare DHCPv4 and DHCPv6 options.
        compareOptions(host1->getCfgOption4(), host2->getCfgOption4());
        compareOptions(host1->getCfgOption6(), host2->getCfgOption6());
    }




};

/// @brief Check that database can be opened
///
/// This test checks if the RadiusHostDataSource can be instantiated.  This happens
/// only if the database can be opened.  Note that this is not part of the
/// RadiusLeaseMgr test fixure set.  This test checks that the database can be
/// opened: the fixtures assume that and check basic operations.

TEST(RadiusHostDataSource, OpenDatabase) {

    // Schema needs to be created for the test to work.
    destroyRadiusSchema();
    createRadiusSchema();

    // Check that lease manager open the database opens correctly and tidy up.
    //  If it fails, print the error message.
    try {
        HostDataSourceFactory::create(validRadiusConnectionString());
        EXPECT_NO_THROW((void) HostDataSourceFactory::getHostDataSourcePtr());
        HostDataSourceFactory::destroy();
    } catch (const isc::Exception& ex) {
        FAIL() << "*** ERROR: unable to open database, reason:\n"
               << "    " << ex.what() << "\n"
               << "*** The test environment is broken and must be fixed\n"
               << "*** before the Radius tests will run correctly.\n";
    }

    // Check that lease manager open the database opens correctly with a longer
    // timeout.  If it fails, print the error message.
    try {
        string connection_string = validRadiusConnectionString() + string(" ") +
                                   string(VALID_TIMEOUT);
        HostDataSourceFactory::create(connection_string);
        EXPECT_NO_THROW((void) HostDataSourceFactory::getHostDataSourcePtr());
        HostDataSourceFactory::destroy();
    } catch (const isc::Exception& ex) {
        FAIL() << "*** ERROR: unable to open database, reason:\n"
               << "    " << ex.what() << "\n"
               << "*** The test environment is broken and must be fixed\n"
               << "*** before the Radius tests will run correctly.\n";
    }

    // Check that attempting to get an instance of the lease manager when
    // none is set throws an exception.
    EXPECT_FALSE(HostDataSourceFactory::getHostDataSourcePtr());

    // Check that wrong specification of backend throws an exception.
    // (This is really a check on LeaseMgrFactory, but is convenient to
    // perform here.)
    EXPECT_THROW(HostDataSourceFactory::create(connectionString(
        NULL, VALID_NAME, VALID_HOST, INVALID_USER, VALID_PASSWORD)),
        InvalidParameter);
    EXPECT_THROW(HostDataSourceFactory::create(connectionString(
        INVALID_TYPE, VALID_NAME, VALID_HOST, VALID_USER, VALID_PASSWORD)),
        InvalidType);

    // Check that invalid login data causes an exception.
    /* Not relevant for freeradius
    EXPECT_THROW(HostDataSourceFactory::create(connectionString(
        RADIUS_VALID_TYPE, INVALID_NAME, VALID_HOST, VALID_USER, VALID_PASSWORD)),
        DbOpenError);
    EXPECT_THROW(HostDataSourceFactory::create(connectionString(
        RADIUS_VALID_TYPE, VALID_NAME, INVALID_HOST, VALID_USER, VALID_PASSWORD)),
        DbOpenError);
    EXPECT_THROW(HostDataSourceFactory::create(connectionString(
        RADIUS_VALID_TYPE, VALID_NAME, VALID_HOST, INVALID_USER, VALID_PASSWORD)),
        DbOpenError);
    EXPECT_THROW(HostDataSourceFactory::create(connectionString(
        RADIUS_VALID_TYPE, VALID_NAME, VALID_HOST, VALID_USER, INVALID_PASSWORD)),
        DbOpenError);
    EXPECT_THROW(HostDataSourceFactory::create(connectionString(
        RADIUS_VALID_TYPE, VALID_NAME, VALID_HOST, VALID_USER, VALID_PASSWORD, INVALID_TIMEOUT_1)),
        DbInvalidTimeout);
    EXPECT_THROW(HostDataSourceFactory::create(connectionString(
        RADIUS_VALID_TYPE, VALID_NAME, VALID_HOST, VALID_USER, VALID_PASSWORD, INVALID_TIMEOUT_2)),
        DbInvalidTimeout);
    EXPECT_THROW(HostDataSourceFactory::create(connectionString(
        RADIUS_VALID_TYPE, VALID_NAME, VALID_HOST, VALID_USER, VALID_PASSWORD,
        VALID_TIMEOUT, INVALID_READONLY_DB)), DbInvalidReadOnly);
    // Check for missing parameters
    EXPECT_THROW(HostDataSourceFactory::create(connectionString(
        RADIUS_VALID_TYPE, NULL, VALID_HOST, INVALID_USER, VALID_PASSWORD)),
        NoDatabaseName);
    */

    // Tidy up after the test
    destroyRadiusSchema();
}

// Test verifies if a host reservation can be added and later retrieved by IPv4
// address. Host uses hw address as identifier.
/*
TEST_F(RadiusHostDataSourceTest, basic4HWAddr) {
    testBasic4(Host::IDENT_HWADDR);
}

// Test verifies if a host reservation can be added and later retrieved by IPv4
// address. Host uses client-id (DUID) as identifier.
TEST_F(RadiusHostDataSourceTest, basic4ClientId) {
    testBasic4(Host::IDENT_DUID);
}
// Test verifies that multiple hosts can be added and later retrieved by their
// reserved IPv4 address. This test uses HW addresses as identifiers.
TEST_F(RadiusHostDataSourceTest, getByIPv4HWaddr) {
    testGetByIPv4(Host::IDENT_HWADDR);
}

// Test verifies that multiple hosts can be added and later retrieved by their
// reserved IPv4 address. This test uses client-id (DUID) as identifiers.
TEST_F(RadiusHostDataSourceTest, getByIPv4ClientId) {
    testGetByIPv4(Host::IDENT_DUID);
}
*/

// Test verifies if a host reservation can be added and later retrieved by
// hardware address.
TEST_F(RadiusHostDataSourceTest, get4ByHWaddr) {
    testGet4ByIdentifier(Host::IDENT_HWADDR);
}

// Test verifies if a host reservation can be added and later retrieved by
// DUID.
TEST_F(RadiusHostDataSourceTest, get4ByDUID) {
    testGet4ByIdentifier(Host::IDENT_DUID);
}

// Test verifies if a host reservation can be added and later retrieved by
// circuit id.
TEST_F(RadiusHostDataSourceTest, get4ByCircuitId) {
    testGet4ByIdentifier(Host::IDENT_CIRCUIT_ID);
}

// Test verifies if a host reservation can be added and later retrieved by
// client-id.
TEST_F(RadiusHostDataSourceTest, get4ByClientId) {
    testGet4ByIdentifier(Host::IDENT_CLIENT_ID);
}

// Test verifies if hardware address and client identifier are not confused.
// XXX: Is it relevant for freeradius backend ?
// TEST_F(RadiusHostDataSourceTest, hwaddrNotClientId1) {
//    testHWAddrNotClientId();
//}

// Test verifies if hardware address and client identifier are not confused.
// XXX: Is it relevant for freeradius backend ?
// TEST_F(RadiusHostDataSourceTest, hwaddrNotClientId2) {
//     testClientIdNotHWAddr();
// }

// Test verifies if a host with FQDN hostname can be stored and later retrieved.
// XXX: Is it relevant for freeradius backend ?
// TEST_F(RadiusHostDataSourceTest, hostnameFQDN) {
//     testHostname("foo.example.org", 1);
// }

// Test verifies if 100 hosts with unique FQDN hostnames can be stored and later
// retrieved.
// XXX: Is it relevant for freeradius backend ?
// TEST_F(RadiusHostDataSourceTest, hostnameFQDN100) {
//    testHostname("foo.example.org", 100);
// }

// Test verifies if a host without any hostname specified can be stored and later
// retrieved.
// TEST_F(RadiusHostDataSourceTest, noHostname) {
//     testHostname("", 1);
// }

// // Test verifies if the hardware or client-id query can match hardware address.
// TEST_F(RadiusHostDataSourceTest, DISABLED_hwaddrOrClientId1) {
//     /// @todo: The logic behind ::get4(subnet_id, hwaddr, duid) call needs to
//     /// be discussed.
//     ///
//     /// @todo: Add host reservation with hardware address X, try to retrieve
//     /// host for hardware address X or client identifier Y, verify that the
//     /// reservation is returned.
// }
//
// // Test verifies if the hardware or client-id query can match client-id.
// TEST_F(RadiusHostDataSourceTest, DISABLED_hwaddrOrClientId2) {
//     /// @todo: The logic behind ::get4(subnet_id, hwaddr, duid) call needs to
//     /// be discussed.
//     ///
//     /// @todo: Add host reservation with client identifier Y, try to retrieve
//     /// host for hardware address X or client identifier Y, verify that the
//     /// reservation is returned.
// }

// Test verifies that host with IPv6 address and DUID can be added and
// later retrieved by IPv6 address.
// TEST_F(RadiusHostDataSourceTest, get6AddrWithDuid) {
//     testGetByIPv6(Host::IDENT_DUID, false);
// }

// Test verifies that host with IPv6 address and HWAddr can be added and
// later retrieved by IPv6 address.
// TEST_F(RadiusHostDataSourceTest, get6AddrWithHWAddr) {
//     testGetByIPv6(Host::IDENT_HWADDR, false);
// }

// Test verifies that host with IPv6 prefix and DUID can be added and
// later retrieved by IPv6 prefix.
// TEST_F(RadiusHostDataSourceTest, get6PrefixWithDuid) {
//     testGetByIPv6(Host::IDENT_DUID, true);
// }

// Test verifies that host with IPv6 prefix and HWAddr can be added and
// later retrieved by IPv6 prefix.
// TEST_F(RadiusHostDataSourceTest, get6PrefixWithHWaddr) {
//     testGetByIPv6(Host::IDENT_HWADDR, true);
// }

// Test verifies that host with IPv6 prefix reservation can be retrieved
// by subnet id and prefix value.
// TEST_F(RadiusHostDataSourceTest, get6SubnetPrefix) {
//     testGetBySubnetIPv6();
// }

// Test verifies if a host reservation can be added and later retrieved by
// hardware address.
TEST_F(RadiusHostDataSourceTest, get6ByHWaddr) {
    testGet6ByHWAddr();
}

// Test verifies if a host reservation can be added and later retrieved by
// client identifier.
TEST_F(RadiusHostDataSourceTest, get6ByClientId) {
    testGet6ByClientId();
}

// Test verifies if a host reservation can be stored with both IPv6 address and
// prefix.
TEST_F(RadiusHostDataSourceTest, addr6AndPrefix) {
    testAddr6AndPrefix();
}

}; // Of anonymous namespace
